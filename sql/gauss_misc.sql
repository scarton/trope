select l.wiki_id, l.tvt_id, w.title, t.title, rand() as r from wiki_tvt_work_link as l 
join wiki_work as w on w.wiki_id = l.wiki_id
join tvt_work as t on t.tvt_id = l.tvt_id
order by r limit 10;


select count(*) from tvtropes_work;

select * from tvtropes_work order by file_name;

SHOW COLUMNS FROM wiki_work LIKE 'wiki_id';

update  wiki_work set web_url = 'https://en.wikipedia.org/wiki/Bad_Eggs' where wiki_id = 2427210;

select count(*) from wiki_work;

select count(*) from tvt_work;

select count(*) from wiki_tvt_work_link; --5889, better

--TVT works that couldn't be matched to Wiki works

select t.* from tvt_work as t
left join wiki_tvt_work_link as l
on t.tvt_id = l.tvt_id
where l.tvt_id is null;

select count(distinct(t.tvt_id)) from tvt_work as t
left join wiki_tvt_work_link as l
on t.tvt_id = l.tvt_id
where l.tvt_id is null;


--Wiki works that couldn't be matched to TVT works

select w.*,l.tvt_id,l.wiki_id from wiki_work as w
left join wiki_tvt_work_link as l
on w.wiki_id = l.wiki_id
where l.tvt_id is null;

select count(distinct(w.wiki_id)) from wiki_work as w
left join wiki_tvt_work_link as l
on w.wiki_id = l.wiki_id
where l.tvt_id is null;

select * from wiki_work where title like 'crouching tiger%';


select w.web_url,rand() as r from wiki_work as w
join wiki_tvt_work_link as l
on w.wiki_id = l.wiki_id
order by r limit 10;



select * from wiki_work where title like '%iron man%' order by title;

select * from tvt_work where title like '%iron man%' order by title;

select count(*) from wiki_work;


truncate table wiki_work;

truncate table wiki_tvt_work_link;

alter table trope add column (matched_tvt_work_count int);

            select w.title as wiki_title,
            w.wiki_id as wiki_id, 
            w.web_url as wiki_url, 
            t.title as tvtropes_title, 
            t.tvt_id as tvtropes_id, 
            t.web_url as tvtropes_url, 
            tr.title as trope_title, 
            tr.tvt_id as trope_id, 
            tr.web_url as trope_url
            from 
                (select *, rand(120911) as r
                from (wiki_tvt_work_link)
                order by r limit 5)
            as l 
            join wiki_work as w on l.wiki_id = w.wiki_id 
            join tvt_work as t on l.tvt_id = t.tvt_id
            join tvt_work_trope_link as tl on tl.work_tvt_id = l.tvt_id
            join trope as tr on tl.trope_tvt_id = tr.tvt_id
            order by wiki_id, trope_id;
            
 update trope set title = trim('\n' from title);
 
 select title  from trope;
 
 select * from tvt_work where title like '%pumpkin%';
 
 truncate table wiki_work;
 
select count(*) from tvt_work;

truncate table wiki_tvt_work_link;

select count(*) from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id;

select * from trope where tvt_id = 9779;

show variables like 'char%';

select * from trope where title like 'Oh_ Crap';


--Count how many times each 1st level category occurs in a list of tropes

select c.title,c.web_url,l.category_tvt_id, count(*) as cnt from tvt_trope_category_link as l 
join tvt_category as c on l.category_tvt_id = c.tvt_id 
where l.trope_tvt_id in (11742,971,1611,3462,7280,1259,3372,3461,5249,846,14442,3566,2964,698,8797)
group by l.category_tvt_id
order by cnt desc;

--Get all the second-level category occurrences for a list of tropes

select t.title as t_title,t.tvt_id as t_tvt_id, c1.title as c1_title,l.category_tvt_id as c1_tvt_id, c2.title as c2_title, c2.tvt_id as c2_tvt_id
from tvt_trope_category_link as l
join trope as t on t.tvt_id = l.trope_tvt_id
join tvt_category as c1 on l.category_tvt_id = c1.tvt_id 
join tvt_category_category_link as ccl on ccl.child_tvt_id = l.category_tvt_id
join tvt_category as c2 on ccl.parent_tvt_id = c2.tvt_id
where l.trope_tvt_id in (11742,971,1611,3462,7280,1259,3372,3461,5249,846,14442,3566,2964,698,8797)
order by t.tvt_id, c2.tvt_id;

--Count  the second-level category occurrences for a list of tropes

select c2_title, c2_tvt_id, c2_web_url, count(*) as c2_count from (
	select t.title as t_title,t.tvt_id as t_tvt_id,c2.title as c2_title, c2.tvt_id as c2_tvt_id, c2.web_url as c2_web_url
	from tvt_trope_category_link as l
	join trope as t on t.tvt_id = l.trope_tvt_id
	join tvt_category as c1 on l.category_tvt_id = c1.tvt_id 
	join tvt_category_category_link as ccl on ccl.child_tvt_id = l.category_tvt_id
	join tvt_category as c2 on ccl.parent_tvt_id = c2.tvt_id
	where l.trope_tvt_id in (11742,971,1611,3462,7280,1259,3372,3461,5249,846,14442,3566,2964,698,8797)
	group by c2_tvt_id, t.tvt_id) as sub
group by c2_tvt_id
order by c2_count desc;

select count(*) from tvt_work; -- 

select w.tvt_id, w.trope_count, count(*) as trope_count
from tvt_work as w 
join tvt_work_trope_link as l on w.tvt_id = l.work_tvt_id
group by w.tvt_id
order by w.tvt_id;


--show list of tropes by how many works they occur in

select t.tvt_id, t.title, count(*) as cnt from trope as t
join tvt_work_trope_link as ttl on t.tvt_id = ttl.trope_tvt_id
join wiki_tvt_work_link as wtl on ttl.work_tvt_id = wtl.tvt_id
group by t.tvt_id
order by cnt desc limit 20;

select title from trope where tvt_id in (3372, 1611, 763, 14442, 2460, 20669, 478, 8797, 1259, 825, 7287, 9158, 4579, 1832, 21823, 7280, 292855, 13635, 2964, 7810, 601,
                 11020, 3697, 3991, 14068, 16770, 5864, 6241, 4061, 4078, 5961, 3290, 28024, 3060, 722, 4593, 2968, 730, 11742);

