        select w.title as wiki_title,
        w.wiki_id as wiki_id, 
        w.web_url as wiki_url, 
        t.title as tvtropes_title, 
        t.tvt_id as tvtropes_id, 
        t.web_url as tvtropes_url, 
        tr.title as trope_title, 
        tr.tvt_id as trope_id, 
        tr.web_url as trope_url, 
        "" as found, 
        "" as notes 
        from (select *,rand(120912) as r from trope
        	where matched_tvt_work_count >= 3
            order by r limit 5) as tr
        join tvt_work_trope_link as tl on tl.trope_tvt_id = tr.tvt_id
        join tvt_work as t on t.tvt_id = tl.work_tvt_id
        join wiki_tvt_work_link as l on l.tvt_id = tl.work_tvt_id
        join wiki_work as w on l.wiki_id = w.wiki_id 
        order by trope_id, wiki_id;
        
select *,rand(120912) as r from trope
            order by r limit 5;
            
select * from tvt_work_trope_link where trope_tvt_id = 3245;
select * from tvt_work where tvt_id = 14384;

alter table trope add column (matched_tvt_work_count int);

	select ttl.trope_tvt_id, count(*) as matched_count from wiki_tvt_work_link as wtl
	join tvt_work_trope_link as ttl
	on wtl.tvt_id= ttl.work_tvt_id
	group by ttl.trope_tvt_id

	
	
update trope set matched_tvt_work_count = 0;
update trope, (
	select ttl.trope_tvt_id, count(*) as matched_count from wiki_tvt_work_link as wtl
	join tvt_work_trope_link as ttl
	on wtl.tvt_id= ttl.work_tvt_id
	group by ttl.trope_tvt_id
) as matched_counts
set trope.matched_tvt_work_count = matched_counts.matched_count
where trope.tvt_id = matched_counts.trope_tvt_id;



create table if not exists tvt_category (
	tvt_id int primary key,
	title varchar(256),
	file_name varchar(256),
	web_url varchar(256)
);

create table if not exists tvt_trope_category_link (
	id int auto_increment primary key,
	trope_tvt_id int,
	category_tvt_id int,
	unique key (trope_tvt_id,category_tvt_id),
	key (category_tvt_id)
);

create table if not exists tvt_work_category_link (
	link_id int auto_increment primary key,
	work_tvt_id int,
	category_tvt_id int,
	key(category_tvt_id),
	unique key(work_tvt_id,category_tvt_id)
);

create table if not exists tvt_category_category_link (
	link_id int auto_increment primary key,
	child_tvt_id int,
	parent_tvt_id int,
	key(child_tvt_id),
	unique key(parent_tvt_id,child_tvt_id)
);

drop table tvt_work_category_link;

show variables like 'char%';

ALTER TABLE trope.trope MODIFY COLUMN title VARCHAR(256)
    CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE trope.tvt_category MODIFY COLUMN title VARCHAR(256)
    CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE trope.tvt_work MODIFY COLUMN title VARCHAR(256)
    CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;