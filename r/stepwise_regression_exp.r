# This is a script that reads in a series of CSV files produced by run_film_regression_experiment.py on "regression_dump" mode
# It does linear regression, training first on a non-trope-related features, and then adding each trope to the base model and
# seeing if it significantly improves results. The goal is to discover which tropes are useful in predicting the indicator variable
# (either film grosses or film critical ratings)


options(width = 1000) 
options(echo=TRUE)

discards = c('newid','complaint_no','dispatch_start_datetime')
target = 't_adverse'

info = Sys.info()
if (info['sysname'] == 'Windows' && info['nodename'] == 'GONDOLIN')
{
	default_data_dir = 'C:/Users/Sam/Desktop/DSSG 2015/KDD 2016 Paper/new_leave_one_out_experiment'
} else {
	if (info['sysname'] == 'Linux' && info['nodename'] == 'gauss')
	{
		default_data_dir = '/home/scarton/dssg/kdd_2016/leave_one_out_experiment'
	} else {
		stop('Not a recognized system. I do not know how to assign file names and such')
	}
}


train_file = 'cached_train.csv'
test_file = 'cached_test.csv'

data_dir = default_data_dir



cat('\n\n************************* Training data *************************\n')
train_Xy = read.csv(paste(data_dir,train_file,sep='/'))
print(t(summary(train_Xy)))

cat('\n\n************************* Test data *************************\n')
test_Xy = read.csv(paste(data_dir,test_file,sep='/'))
print(t(summary(test_Xy)))

cat('\n\n************************* Doing stepwise regression *************************\n')
train_Xy = train_Xy[,!(names(train_Xy) %in% discards)]
test_XY = test_Xy[,!(names(test_Xy) %in% discards)]




cat('\n\n')
cat('Performing stepwise regression using trope features on a base model of non-trope features')

target_col = colnames(base_Xy)[1]
cat('Target column: ',target_col)

null_model = lm(as.formula(paste(target_col,'~ 1')),data = full_Xy)

base_model  = lm(formula(base_Xy),data = full_Xy)
cat('Base model:')
print(summary(base_model))


full_model = lm(formula(full_Xy),data=full_Xy)
cat('Full model')
print(summary(full_model))

forward_model = step(base_model,scope =list(lower=base_model,upper=full_model),direction = "forward",data=full_Xy)
cat('Stepwise model')
print(summary(forward_model))

cat('Stepwise model ANOVA')
print(forward_model$anova)


write.csv(forward_model$anova,paste(data_dir,'/','stepwise_regression_anova_results.csv',sep=''))




base_test_Xy = cbind(test_target,base_test_X)
full_test_Xy = cbind(base_test_Xy,supp_test_X)

base_train_score = predict(base_model,full_Xy)
base_train_rmse = (mean((base_train_score-full_Xy[1])^2)^0.5)
cat('Base model training RMSE: ',base_train_rmse)

forward_train_score = predict(forward_model,full_Xy)
forward_train_rmse = (mean((forward_train_score-full_Xy[1])^2)^0.5)
cat('Forward model training RMSE: ',forward_train_rmse)

full_train_score = predict(full_model,full_Xy)
full_train_rmse = (mean((full_train_score-full_Xy[1])^2)^0.5)
cat('full model training RMSE: ',full_train_rmse)

base_test_score = predict(base_model, full_test_Xy)
base_test_rmse = (mean((base_test_score-full_test_Xy[1])^2)^0.5)
cat('Base model testing RMSE: ',base_test_rmse)

forward_test_score = predict(forward_model, full_test_Xy)
forward_test_rmse = (mean((forward_test_score-full_test_Xy[1])^2)^0.5)
cat('Forward model training RMSE: ',forward_test_rmse)

full_test_score = predict(full_model, full_test_Xy)
full_test_rmse = (mean((full_test_score-full_test_Xy[1])^2)^0.5)
cat('full model training RMSE: ',full_test_rmse)





