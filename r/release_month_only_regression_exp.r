# This is a script that reads in a series of CSV files produced by run_film_regression_experiment.py on "regression_dump" mode
# It does linear regression on only release-date features, in order to understand what's going on with these. 

options(width = 1000) 
options(echo=TRUE)

info = Sys.info()
if (info['sysname'] == 'Windows' && info['nodename'] == 'GONDOLIN')
{
	default_data_dir = './dev_data/film_regression_results/regression_dump_gross_adjusted_Gondolin_2015_12_09_12_16'
	col_str = 'genre' 
	} else {
		if (info['sysname'] == 'Linux' && info['nodename'] == 'gauss')
		{
			default_data_dir = '/storage6/users/scarton/tropes_2/data/film_regression_results/working_regression_dump_gross_adjusted_gauss_2015_12_10_19_30'
			col_str = 'release_month'
		} else {
			stop('Not a recognized system. I do not know how to assign file names and such')
		}
}

args <- commandArgs(trailingOnly = TRUE)
print(args)

if (length(args) >= 1)
{
	data_dir = args[1]
} else {
	data_dir = default_data_dir
}

base_train_X_file = 'non_trope_train_X.csv'
train_target_file = 'train_y.csv'

base_test_X_file = 'non_trope_test_X.csv'
test_target_file = 'test_y.csv'



cat('\n\n************************* Non-trope training X *************************\n')
base_train_X = read.csv(paste(data_dir,base_train_X_file,sep='/'))
print(t(summary(base_train_X)))

cat('\n\n************************* Non-trope testing X *************************\n')
base_test_X = read.csv(paste(data_dir,base_test_X_file,sep='/'))
print(t(summary(base_test_X)))


cat('\n\n************************* Training target column *************************\n')
train_target = read.csv(paste(data_dir,train_target_file,sep='/'))
print(summary(train_target))

cat('\n\n************************* Testing target column ************************\n*')
test_target = read.csv(paste(data_dir,test_target_file,sep='/'))
print(summary(test_target))

#Grab only the columns that have a name that contains 'release_month'
date_X = base_train_X[,grepl(col_str,names(base_train_X))]

cat('\nFitting base model\n')
base_fit  = lm(as.matrix(train_target) ~ as.matrix(date_X))

print(summary(base_fit))


