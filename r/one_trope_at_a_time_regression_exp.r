# This is a script that reads in a series of CSV files produced by run_film_regression_experiment.py on "regression_dump" mode
# It does linear regression, training first on a non-trope-related features, and then adding each trope to the base model and
# seeing if it significantly improves results. The goal is to discover which tropes are useful in predicting the indicator variable
# (either film grosses or film critical ratings)

options(width = 1000) 
options(echo=TRUE)

add_to_results_frame <- function(rf,new_model,base_model,new_column,name,i)
{
	ns = summary(new_model)
	rf[i,'name'] <- name
	rf[i,'model_r_squared'] <- ns$r.squared
	rf[i,'model_adj_r_squared'] <- ns$adj.r.squared
	fs = ns$fstatistic
	rf[i,'model_p_val'] <- pf(fs[1],fs[2],fs[3],lower.tail=FALSE)
	rf[i,'model_RSE'] <- ns$sigma
	
	if (is.na(base_model) || is.na(new_column))
	{
		rf[i,'new_col_coeff'] <- NA
		rf[i,'new_col_p_val'] <- NA
		rf[i,'anova_vs_base_p_val'] <- NA
	}
	else
	{
		co <- ns$coefficients
		row = co[paste('as.matrix(new_X)',new_column,sep=''),]
		
		rf[i,'new_col_coeff'] <- row['Estimate']
		rf[i,'new_col_p_val'] <- row['Pr(>|t|)']
		
		an <- anova(new_model,base_model)
		rf[i,'anova_vs_base_p_val'] <- an[2,'Pr(>F)']
	}
		
	
	return (rf)
}

#default_data_dir = './dev_data/film_regression_results/regression_dump_gross_adjusted_Gondolin_2015_12_09_12_16'
default_data_dir = '/storage6/users/scarton/tropes_2/data/film_regression_results/regression_dump_gross_adjusted_gauss_2015_12_10_19_30'




args <- commandArgs(trailingOnly = TRUE)
print(args)

if (length(args) >= 1)
{
	data_dir = args[1]
} else {
	data_dir = default_data_dir
}

cat('Data directory: ',data_dir)

base_train_X_file = 'non_trope_train_X.csv'
supp_train_X_file = 'trope_train_X.csv'
train_target_file = 'train_y.csv'

base_test_X_file = 'non_trope_test_X.csv'
supp_test_X_file = 'trope_test_X.csv'
test_target_file = 'test_y.csv'



cat('\n\n************************* Non-trope training X *************************\n')
base_train_X = read.csv(paste(data_dir,base_train_X_file,sep='/'))
print(t(summary(base_train_X)))

cat('\n\n************************* Non-trope testing X *************************\n')
base_test_X = read.csv(paste(data_dir,base_test_X_file,sep='/'))
print(t(summary(base_test_X)))


cat('\n\n************************* Trope training X *************************\n')
supp_train_X = read.csv(paste(data_dir,supp_train_X_file,sep='/'))
print(t(summary(supp_train_X)))

cat('\n\n************************* Trope testing X *************************\n')
supp_test_X = read.csv(paste(data_dir,supp_test_X_file,sep='/'))
print(t(summary(supp_test_X)))


cat('\n\n************************* Training target column *************************\n')
train_target = read.csv(paste(data_dir,train_target_file,sep='/'))
print(summary(train_target))

cat('\n\n************************* Testing target column ************************\n*')
test_target = read.csv(paste(data_dir,test_target_file,sep='/'))
print(summary(test_target))


base_X <- base_train_X
supp_X <- supp_train_X
target <- train_target

cat('\n\n')
cat('Building base non-trope model and then adding trope columns in one-by-one, accumulating significance findings as we go\n')

n <- 1+dim(supp_X)[2]
results = data.frame(name=character(n),
									 model_r_squared=double(n),
									 model_adj_r_squared=double(n), 
# 									 model_f_stat = double(n), 
# 									 model_df = double(n),
									 model_p_val=double(n),
									 model_RSE=double(n),
									 new_col_coeff=double(n),
									 new_col_p_val=double(n),
									 anova_vs_base=double(n),
									 stringsAsFactors=FALSE)

cat('\nFitting base model\n')
base_fit  = lm(as.matrix(target) ~ as.matrix(base_X))

print(summary(base_fit))

results <- add_to_results_frame(results,base_fit,NA,NA,'base',1)


cat('\nFitting ',(n-1),' one-shot models with one additional trope column\n')
i <- 2
for (supp_col in colnames(supp_X))
{
	try({
		cat(i-1,'... ')
		new_X <- cbind(base_X,supp_X[supp_col])
		new_fit <- lm(as.matrix(target) ~ as.matrix(new_X))
		results <- add_to_results_frame(results,new_fit,base_fit,supp_col,paste('base',supp_col,sep='+'),i)
		i = i + 1
	})
}
cat('\n')


cat('\nSingle trope models by significance: \n')
print(results[order(results[,'new_col_p_val']),])

# cat('\nSummary of model performances:\n')
# print(results)

write.csv(results,paste(data_dir,'/','one_at_a_time_regression_results.csv',sep=''))
print('DONE!')











