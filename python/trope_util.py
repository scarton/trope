__author__ = 'Sam'
import re
import urllib
import time
import datetime
import os
import csv
import inspect

def wiki_to_web_title(wiki_title):
        adjusted_title = re.sub(' ','_',wiki_title.strip())
        # adjusted_title = urllib.quote(adjusted_title.encode("UTF-8"),safe="\",():;!&/*")
        adjusted_title = urllib.quote(adjusted_title.encode("UTF-8"),safe="\",():;!&*")

        return adjusted_title

def tvt_file_name_to_web_url(file_name):
    return urllib.unquote(file_name)

def current_milli_time():
    return int(round(time.time() * 1000))

def current_date_string():
    return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M")

def is_main_page(url):
    return url.startswith('http://tvtropes.org/pmwiki/pmwiki.php/Main/')

def append_tvt_prefix(url):
    if not url.startswith("http://tvtropes.org"):
        return "http://tvtropes.org" + url
    else:
        return url

def write_result_to_csv(resultset, dir, filename, create_dirs = True):
    '''
    Write a SQLAlchemy resultset object to a CSV
    :param resultset:
    :param filepath:
    :return:
    '''
    if create_dirs and not os.path.exists(dir):
        os.makedirs(dir)

    with open(dir+'/'+filename,'w') as f:
        writer = csv.writer(f,quoting=csv.QUOTE_NONNUMERIC,lineterminator='\n')
        writer.writerow(resultset.keys())
        for row in resultset:
            writer.writerow(map(encode,row))

def encode(obj):
    if type(obj) == unicode:
        return obj.encode('utf-8')
    else:
        return obj

def short(obj,l=100):
    strobj = str(encode(obj))
    if len(strobj) > l:
        return strobj[0:l/2]+' ... ' + strobj[-l/2:]
    else:
        return strobj

def gen_var_string(globals = True,locals=False):
    '''
    A quick and dirty way of saving the hardcoded parameters of any experiment: save what global variables are
    defined as to a string, and then write that string to a file along with the other output of the experiment
    :param globals:
    :param locals:
    :return:
    '''

    caller = inspect.stack()[1]

    rstring ='Variables defined in'+caller[1]+' function ' + caller[3]+':\n'

    if locals:
        rstring = 'Local variables:\n'
        clocals = dict(caller[0].f_locals)
        for var in clocals:
            rstring += '\t'+var+': '+short(clocals[var])+'\n'

    if globals:
        cglobals = dict(caller[0].f_globals)
        rstring += 'Global variables:\n'
        for var in cglobals:
            rstring += '\t'+var+': '+short(cglobals[var])+'\n'

    return rstring

def title_to_filename(title):
    '''
    Converts an entity title to a string suitable for inclusion in a filename. Doesn't preserve anything, so
    don't try to convert back from this to the original title.
    Specifically, set it to lower case, replace all whitespace with underscores, and get rid of non-alphanumeric
    characters
    :param title: a string
    :return: filename-safe version of title
    '''
    newtitle = title.lower()
    newtitle = re.sub('\s+','_',newtitle)
    newtitle = re.sub('[^A-Za-z0-9_]','',newtitle)
    return newtitle