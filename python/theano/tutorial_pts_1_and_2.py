__author__ = 'Sam'

from theano import *
import theano.tensor as T

#Going through tutorial at http://deeplearning.net/software/theano/tutorial/index.html

#Baby Steps - Algebra
print '*** Baby Steps - Algebra ***'
'''
T.dscalar is the theano type for doubles. It's a 0-dimensional array

All variables in theano have to be typed.

T.dscalar isn't a class, so x and y are instances of type TensorVariable with a
type attribute indicating that they are scalars
'''
x = T.dscalar('x')
y = T.dscalar('y')

'''
This actually creates z as an operator, not as the value of x+y
'''
z = x+y

'''
Don't really understand why this step is necessary. I would have thought you could
just use z as a function after the previous line. But this creates a function f that
applies z to two arguments
'''
f = function([x,y],z)

print f(2,3)

#Could also have done:
print z.eval({x:2,y:3})

#Returns true if two arrays are elementwise-equal to within a tolerance
print numpy.allclose(f(16.3, 12.1), 28.4)

x = T.dmatrix('x')
y = T.dmatrix('y')
z = x + y
f = function([x, y], z)

print f([[1, 2], [3, 4]], [[10, 20], [30, 40]])

#More Examples
print '*** More examples ***'

#Create an elementwise logistic function and test it on a small array
x= T.dmatrix('x')
s = 1/(1+T.exp(-x))
logistic = function([x],s)
print logistic([[0, 1], [-1, -2]])

#Create a function that outputs various kinds of diffs between two arrays
a,b = T.dmatrices('a','b')
diff = a-b
abs_diff = abs(diff)
diff_squared = diff**2

f = theano.function([a,b],[diff,abs_diff,diff_squared])
print f([[1, 1], [1, 1]], [[0, 1], [2, 3]])

#Set a default value for an argument
x, y = T.dscalars('x', 'y')
z = x + y
f = function([x, In(y, value=1)], z)
print f(33)

#Using shared variables
print 'Using shared variables'
#Kind of weird

state = shared(0)
inc = T.iscalar('inc')
accumulator = function([inc], state, updates=[(state, state+inc)])
print(state.get_value())
accumulator(1)
print(state.get_value())
accumulator(300)
print(state.get_value())

#Reset the value
state.set_value(-1)
accumulator(3)
print(state.get_value())

#Create a decrementor function that operates on the same shared variable
decrementor = function([inc], state, updates=[(state, state-inc)])
decrementor(2)
print(state.get_value())

#Impressions so far: lots of reinventing the wheel. Kind of creating it's own weird
#symbolic language using Python as a base. I'm sure it will all turn out to be necessary
#for parallelization and construction of NN architectures

#Givens parameter
print 'Givens parameter'
fn_of_state = state ** 2 + inc
foo = T.scalar(dtype=state.dtype)
skip_shared = function([inc,foo],fn_of_state,  givens=[(state, foo)])
print skip_shared(1,3)
#I don't really get this. I guess we replaced state with foo, just for that invocation of
#the function
print state.get_value()


#using random numbers
print 'Using random numbers'
from theano.tensor.shared_randomstreams import RandomStreams
srng = RandomStreams(seed = 24)
rv_u = srng.uniform((2,2))
rv_n = srng.normal((2,2))
f = function([],rv_u)
g = function([],rv_n, no_default_updates=True)

#Random variables are only drawn once per function call, so this will return exactly zero (other than rounding error)
nearly_zeros = function([], rv_u + rv_u - 2 * rv_u)

print f()
print f()
print g()
print g()

print nearly_zeros()

#Seeding streams
print 'Seeding streams'

#Seed rv_u with a new seed (seems unnecessarily complicated, I guess because of the symbolic scheme)
rng_val = rv_u.rng.get_value(borrow = True)
rng_val.seed(89234)
rv_u.rng.set_value(rng_val, borrow=True)

#Seed all random variables created by srng
#Seeds rv_u and rv_n with different seed values, generated randomly from 902340
srng.seed(902340)

#sharing streams between functions
print('sharing streams between functions')

state_after_v_0 = rv_u.rng.get_value().get_state()
print nearly_zeros()

v1 = f()
rng = rv_u.rng.get_value(borrow=True)
rng.set_state(state_after_v_0)
rv_u.rng.set_value(rng,borrow=True)
v2 = f()
v3 = f()

print 'v1',v1
print 'v2',v2
print 'v3',v3

print 'Copying random states between theano graphs'
'''
If, for instance, you wanted to initialized the state of a model from the parameters
of a pickled model.

Though, I would have expected the state of a model to be not a random state.
'''

# from __future__ import print_function
import theano
import numpy
import theano.tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams
from theano.tensor.shared_randomstreams import RandomStreams

class Graph():
    def __init__(self,seed=123):
        self.rng = RandomStreams(seed)
        self.y = self.rng.uniform(size=(1,))

g1 = Graph(seed=123)
f1 = theano.function([],g1.y)

g2 = Graph(seed=987)
f2 = theano.function([],g2.y)

print f1()
print f2()

def copy_random_state(g1,g2):
    if isinstance(g1.rng, MRG_RandomStreams):
        g2.rng.rstate = g1.rng.rstate
    for (su1, su2) in zip(g1.rng.state_updates,g2.rng.state_updates):
        su2[0].set_value(su1[0].get_value())

copy_random_state(g1,g2)
print f1()
print f2()

print 'Logistic regression example'
import numpy
import theano
import theano.tensor as T
rng = numpy.random
N = 400
feats = 784

D = (rng.randn(N,feats), rng.randint(size=N,low=0,high=2))
training_steps = 10000

x = T.matrix('x')
y = T.vector('y')

#Weight vector
w = theano.shared(rng.randn(feats),name='w')

#Bias term
b = theano.shared(0.,name='b')

print 'Initial weight vector:'
print (w.get_value())
print 'Initial bias term:'
print (b.get_value())

#Construct "Theano expression graph", whatever that is
p_1 = 1 / (1+T.exp(-T.dot(x,w) - b)) #Probability that target is 1 for a given x
prediction = p_1 > 0.5
xent = -y * T.log(p_1) - (1-y) * T.log(1-p_1) #Cross-entropy loss function between prediction and actual truth
cost = xent.mean() + 0.01*(w**2).sum() #Overall cost to minimize. Looks like we are doing L2 regularization in addition to the cross-entropy loss
gw,gb = T.grad(cost, [w,b]) #Compute gradient of cost with respect to weight and bias. How does it do this? Numerically? Analytically?

# compile
train = theano.function(
    inputs= [x,y],
    outputs=[prediction,xent],
    updates=((w,w-0.1*gw),(b,b-0.1*gb))
)

predict = theano.function(inputs = [x],outputs = prediction)

print 'Training model:'
for i in range(training_steps):
    pred_err = train(D[0],D[1])

print("Final model:")
print(w.get_value())
print(b.get_value())
print("target values for D:")
print(D[1])
print("prediction on D:")
print(predict(D[0]))







pass
