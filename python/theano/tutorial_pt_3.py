__author__ = 'Sam'


print '*** Computing Gradients ***'

print 'Basic example'

import numpy
import theano
import theano.tensor as T
from theano import pp

x = T.dscalar('x')
y = x ** 2
gy = T.grad(y,x)
print pp(gy)
print gy

f = theano.function([x],gy)
print f(4)
# print f([4, 4]) #This doesn't work because it expects a single value as input

print numpy.allclose(f(94.2),188.4)

print 'Logistic function example'
x = T.dmatrix('x')
s = T.sum(1/(1+T.exp(-x))) #Logistic function
gs = T.grad(s,x)
dlogistic = theano.function([x],gs)
print dlogistic([[0, 1], [-1, -2]])


print 'Computing the Jacobian'
#This can be done manually with theano.gradient.jacobian. Below is how you would do it manually with scan()
#Scan is a way to write recurrent operations in a symbolic way. It's covered more later in the tutorial
import theano
import theano.tensor as T
x = T.dvector('x')
y = x ** 2
J, updates = theano.scan(lambda i, y, x : T.grad(y[i], x), sequences = T.arange(y.shape[0]), non_sequences = [y,x])
# I don't totally understand the difference between what we are doing here and what we did before
f = theano.function([x],J,updates=updates)
print f([4,4])
#So i guess this allows an input sequence rather than just one scalar
print f([4])

print "Computing the Hessian"
#This can be done automatically with theano.gradient.hessian()
#Below is how you would do it manually by taking the jacobian of the gradient

x = T.dvector('x')
y = x ** 2
cost = y.sum()
gy = T.grad(cost,x)
H, updates = theano.scan(lambda i, gy, x: T.grad(gy[i],x),sequences=T.arange(gy.shape[0]), non_sequences = [gy, x])
f = theano.function([x], H, updates = updates)
print f([4,4])

print "Jacobian times a vector"
'''
It's sometimes faster to generate the product of the Jacobian and a vector than 'to generate the Jacobian and multiply it by a vector.
So here's how you do that
'''

print 'R operator'

W = T.dmatrix('W')
V = T.dmatrix('V')
x = T.dvector('x')
y = T.dot(x, W)

#Generates the Jacobian of y with respect to W and multiplies it by V
JV = T.Rop(y, W, V)
f = theano.function([W, V, x], JV)
print f([[1, 1], [1, 1]], [[2, 2], [2, 2]], [0,1])

#L operator multiplies a row vector by the the Jacobian (since order matters in matrix multiplication)
print 'L operator'
v = T.dvector('v')
VJ = T.Lop(y, W, v)
f = theano.function([v,x], VJ)
print f([2, 2], [0, 1])


print 'Hession times a vector'
print 'Without R operator'
x = T.dvector('x')
v = T.dvector('v')
y = T.sum(x ** 2)
gy = T.grad(y, x)
vH = T.grad(T.sum(gy * v), x)
f = theano.function([x, v], vH)
print f([4, 4], [2, 2])

print 'With R operator (much faster, if I use a profiler)'
x = T.dvector('x')
v = T.dvector('v')
y = T.sum(x ** 2)
gy = T.grad(y, x)
Hv = T.Rop(gy, x, v)
f = theano.function([x, v], Hv)
print f([4, 4], [2, 2])




