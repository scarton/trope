__author__ = 'Sam'

print '*** Conditions ***'

'''
The ongoing theme in this tutorial is basically, lots of different types of computation you can lay out symbolically
so that it can be compiled into an efficient, parallelized (I guess) form.
'''

print 'IfElse vs Switch'
'''
Switch is elementwise and takes a tensor of booleans as input and does something different for each element
IfElse just takes one variable as input
'''
from theano import tensor as T
from theano.ifelse import ifelse
import theano, time, numpy

a,b = T.scalars('a','b')
x,y = T.matrices('x','y')

# I assume T.lt is less than
z_switch = T.switch(T.lt(a,b),T.mean(x),T.mean(y))
z_lazy = ifelse(T.lt(a,b),T.mean(x), T.mean(y))

f_switch = theano.function([a,b,x,y,], z_switch, mode = theano.Mode(linker = 'vm'))
f_lazyifelse = theano.function([a,b,x,y], z_lazy, mode = theano.Mode(linker = 'vm'))

val1 = 0.
val2 = 1.
big_mat1 = numpy.ones((10000,1000))
big_mat2 = numpy.ones((10000,1000))

n_times = 1000

tic = time.clock()
for i in range(n_times):
    f_switch(val1, val2, big_mat1, big_mat2)
print('time spent evaluating both values %f sec' % (time.clock() - tic))

tic = time.clock()
for i in range(n_times):
    f_lazyifelse(val1, val2, big_mat1, big_mat2)
print('time spent evaluating one value %f sec' % (time.clock() - tic))
