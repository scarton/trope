__author__ = 'Sam'
import platform
from sqlalchemy import create_engine
import json
import sys

sys.path.append('../..')
import python.trope_util as tu
import python.data_processing.extract_wiki_article_text as wex
import pandas as pd
from python.db_entity.entity import *

import nltk
import os
import traceback

#This script generates CSV featurset files for a baseline machine learning experiment fo detecting tropes in plot summaries
#For each trope in a predefined set, it creates a CSV consisting of unigram features for articles that contain that trope
#and articles that do not contain that trope, balanced to some reasonable ratio (like 10-90)

max_negs_per_pos = 9

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    server = 'gauss'
    wiki_file_dir = '../../../data/wiki_films/'
    out_super_dir = '../../../data/features/unigram/'
    stop_early = False
    
    #These are the IDs of the tropes found significant in the low support (10 for nontrope, 50 for trope) critical rating stepwise regression experiment
    trope_ids = [3372, 1611, 763, 14442, 2460, 20669, 478, 8797, 1259, 825, 7287, 9158, 4579, 1832, 21823, 7280, 292855, 13635, 2964, 7810, 601,
                 11020, 3697, 3991, 14068, 16770, 5864, 6241, 4061, 4078, 5961, 3290, 28024, 3060, 722, 4593, 2968, 730, 11742]
    vocab_size = 10000

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    server = 'local'
    wiki_file_dir = '../../dev_data/wiki_film_sample_files/'
    out_super_dir = '../../dev_data/features/unigram/'
    stop_early = False
    #The IDs of the most frequently occurring tropes in my small set of local test films
    trope_ids = [1161, 11742, 240401, 5961, 4445, 15584, 230, 13286]
    vocab_size = 200

else:
    raise Exception('Unknown system. I do not know how to assign file names')

wiki_cache = {} #Maps wiki_ids to tuples of (WikiWork,wikitext)

def main():

    vstring = tu.gen_var_string()


    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    texts = []
    wiki_works = {}

    stemmer = nltk.stem.PorterStemmer()
    stopwords = nltk.corpus.stopwords.words('english')

    #Construct feature matrix as a dataframe and write it to CSV
    out_dir = out_super_dir +'/'+ tu.current_date_string()+'/'
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    print 'Writing files to ',out_dir

    for trope_id in trope_ids:
        try:
            labels = {}
            token_map = {}


            #Get basic information about this trope
            t_query = 'select * from trope where tvt_id = '+str(trope_id)
            trope = Trope(engine.execute(t_query).fetchone())

            print '*********** '+str(trope_id)+' | '+trope['title']+' ***********'
            #Get Wiki works that have this trope
            pos_query = '''select w.*
                from wiki_tvt_work_link as wtl
                join tvt_work_trope_link as ttl on wtl.tvt_id = ttl.work_tvt_id
                join wiki_work as w on wtl.wiki_id = w.wiki_id
                where ttl.trope_tvt_id = ''' + str(trope_id)
            pos_wiki_id_results = engine.execute(pos_query).fetchall()
            pos_count = len(pos_wiki_id_results)
            print pos_count,'positive examples'
            for result in pos_wiki_id_results:
                work = WikiWork(result)
                extract_and_cache_wikitext(work,wiki_file_dir)
                labels[work['wiki_id']] = True
                if stop_early: break


            #Get a balanced random sample of Wiki works that don't have this trope
            neg_query = '''select rand() as r, w.*
                from wiki_tvt_work_link as wtl
                left join tvt_work_trope_link as ttl
                on wtl.tvt_id = ttl.work_tvt_id and ttl.trope_tvt_id = '''+str(trope_id)+'''
                join wiki_work as w
                on wtl.wiki_id = w.wiki_id
                where ttl.trope_tvt_id is null
                order by r limit '''+str(max_negs_per_pos*pos_count)
            neg_wiki_id_results = engine.execute(neg_query).fetchall()
            neg_count = len(neg_wiki_id_results)
            print neg_count,'negative examples'
            for result in neg_wiki_id_results:
                work = WikiWork(result)
                extract_and_cache_wikitext(work,wiki_file_dir)
                labels[work['wiki_id']] = False
                if stop_early: break

            #Construct vocabulary
            vocab = nltk.FreqDist()
            for wiki_id in labels:
                text = wiki_cache[wiki_id][1]
                if text:
                    tokens = [word for sent in nltk.sent_tokenize(text.lower()) for word in nltk.word_tokenize(sent)]
                    tokens = [stemmer.stem(x) for x in tokens if x not in stopwords and x.isalpha()]
                    token_map[wiki_id] = set(tokens)
                    vocab.update(tokens)
                else:
                    pass
                    # print 'Could not find plot summary for Wikipedia article:\n'+str(wiki_cache[wiki_id][0])
            pass

            #Construct feature matrix as a dataframe and write it to CSV

            vocab_tokens = [x[0] for x in vocab.most_common(vocab_size)]
            vocab_tokens.insert(0,'has_trope')
            df = pd.DataFrame(index=token_map.keys(),columns=vocab_tokens)
            for wiki_id in token_map.keys():
                df['has_trope'][wiki_id] = int(labels[wiki_id])
                for i in range(1,len(vocab_tokens)):
                    df[vocab_tokens[i]][wiki_id] = int(vocab_tokens[i] in token_map[wiki_id])

            fname = out_dir+str(trope_id)+'_'+tu.title_to_filename(trope['title'])+'.csv'
            df.to_csv(fname,encoding='utf-8')


            #
            # if stop_early:
            #     print 'Stopping early!'
            #     break
        except:
            traceback.print_exc()

        print 'Done!'


def extract_and_cache_wikitext(wiki_work,dir):
    if not wiki_work['wiki_id'] in wiki_cache:
        text = wex.restricted_mwpfh_extract_wiki_plot_summary_text(wiki_work['file_name'],dir)
        wiki_cache[wiki_work['wiki_id']] = (wiki_work,text)





if __name__ == '__main__':
    main()