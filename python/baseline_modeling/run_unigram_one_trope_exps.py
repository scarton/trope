__author__ = 'Sam'

import platform
import os
import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn import svm, cross_validation, metrics, dummy
import sys
sys.path.append('../..')
import python.trope_util as tu
import pickle
import numpy as np


seed = 7736608
test=0.2
target = 'has_trope'
columns_to_drop = [0,1]
if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    server = 'gauss'
    csv_dir = '../../../data/features/unigram/2016_02_29_23_13'
    out_dir = '../../../data/experiments/unigram_one_trope_at_a_time/'+tu.current_date_string()+'/'
    cv = 5

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    server = 'local'
    csv_dir = '../../dev_data/features/unigram/2016_03_01_11_37/'
    out_dir = '../../dev_data/experiments/unigram_one_trope_at_a_time/'+tu.current_date_string()+'/'
    cv = 2
else:
    raise Exception('Unknown system. I do not know how to assign file names')

def main():

    perf_df = pd.DataFrame(columns=['trope','cv_acc','cv_f1','test_acc','test_f1','dummy_acc','dummy_f1'])

    #Parameters
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    open(out_dir+'/variables.txt','w').write(tu.gen_var_string())
    for fn in os.listdir(csv_dir):
        if fn.endswith('.csv'):

            print '********************** '+ fn+' **********************'
            sub_dir = out_dir +'/'+ fn[0:-4]+'/'
            if not os.path.exists(sub_dir):
                os.makedirs(sub_dir)

            df = pd.read_csv(csv_dir+'/'+fn)
            y_df = df[target]
            y_df = y_df.apply(int)
            X_df = df.drop(df.columns[[0,1]],1)

            X_train, X_test, y_train, y_test = train_test_split(X_df.values,y_df.values,test_size=test,random_state=seed,stratify=y_df.values)

            model = svm.LinearSVC(C=1.0).fit(X_train,y_train)
            d_model = dummy.DummyClassifier(strategy = 'stratified', random_state=seed).fit(X_train,y_train)


            #Save:
            #Trained model
            pickle.dump(model,open(sub_dir+'/model.pickle','w'))



            #Train and test sets
            np.savetxt(sub_dir+'/train_Xy.csv',np.column_stack((y_train,X_train)),delimiter=',')
            np.savetxt(sub_dir+'/test_Xy.csv',np.column_stack((y_test,X_test)),delimiter=',')


            #Model prediction on test set, both class and probabilities
            test_pred = model.predict(X_test)
            test_prob = model.decision_function(X_test)
            np.savetxt(sub_dir+'/test_predictions.csv',np.column_stack((y_test,test_pred,test_prob)),delimiter=',',header='true,pred,prob')


            #Basic model performance stats (prec, recall, acc, f1)
            cv_acc = np.mean(cross_validation.cross_val_score(model, X_train, y_train, cv=cv, scoring='accuracy'))
            cv_f1 = np.mean(cross_validation.cross_val_score(model, X_train, y_train, cv=cv, scoring='f1'))

            test_acc = metrics.accuracy_score(y_test,test_pred)
            test_f1 = metrics.f1_score(y_test,test_pred)

            dummy_test_pred = d_model.predict(X_test)
            dummy_acc = metrics.accuracy_score(y_test,dummy_test_pred)
            dummy_f1 = metrics.f1_score(y_test,dummy_test_pred)


            pstr = ''
            pstr += 'Cross validation accuracy:' + str(cv_acc)+'\n'
            pstr += 'Cross validation F1:' + str( cv_f1)+'\n'
            pstr += 'Test accuracy:' + str( test_acc)+'\n'
            pstr += 'Test F1:' + str( test_f1)+'\n'
            pstr += 'Dummy Test accuracy:' + str( dummy_acc)+'\n'
            pstr += 'Dummy Test F1:' + str( dummy_f1)+'\n'

            print pstr

            open(sub_dir+'/model_performance.txt','w').write(pstr)

            new_df = pd.DataFrame([[fn[0:-4],cv_acc,cv_f1,test_acc,test_f1,dummy_acc,dummy_f1]], columns = perf_df.columns)
            perf_df = perf_df.append(new_df)

            pass

    print '***************** Performance ********************'
    print perf_df
    perf_df.to_csv(out_dir+'/model_performance.csv')
    print 'Done!'






if __name__ == '__main__':
    main()