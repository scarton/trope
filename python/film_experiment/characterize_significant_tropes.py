__author__ = 'Sam'

'''
This script takes a list of trope titles or IDs and characterizes that list.
'''

import json
from sqlalchemy import create_engine
import sys
import pandas as pd
sys.path.append('../..')
from python import trope_util as tu
import math

#This particular list represents all the tropes that:
# Appear in at least 100 films
# Significantly improve a linear regression model consisting initially of
# non-trope-related features that appear in at least 15 films (p < 0.05)
# type = 'gross'
# trope_titles = ["Oh__Crap",
# "Butt_Monkey",
# "Deadpan_Snarker",
# "Jerkass",
# "Too_Dumb_to_Live",
# "Cluster_F_Bomb",
# "Ironic_Echo",
# "Jerk_with_a_Heart_of_Gold",
# "Product_Placement",
# "Bookends",
# "Brick_Joke",
# "Karma_Houdini",
# "Heroic_BSOD",
# "Berserk_Button",
# "Ms__Fanservice"]
#
# trope_ids= [11742,
# 971,
# 1611,
# 3462,
# 7280,
# 1259,
# 3372,
# 3461,
# 5249,
# 846,
# 14442,
# 3566,
# 2964,
# 698,
# 8797]

current_date = tu.current_date_string()
filename1 = 'level_1_category_counts.csv'
filename2 = 'level_2_category_counts.csv'
server = 'gauss'
# output_dir =  "C:/Users/Sam/Desktop/trope project/film prediction experiment/iteration 3 results"

input_file = "C:/Users/Sam/Desktop/trope project/film prediction experiment/iteration 3 results/combined_processed.csv"
output_file = "C:/Users/Sam/Desktop/trope project/film prediction experiment/iteration 3 results/cat_counts.csv"

df = pd.read_csv(input_file)



with open('../db.config') as c:
    config = json.load(c)['servers'][server]

engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

catset = set()
results = {}
columns = ['category_id','category_name','category_url']

for i in range(1,len(df.columns),3):
    ids = [str(x) for x in df[df.columns[i]] if not math.isnan(x)]
    names = [x for x in df[df.columns[i+1]] if type(x) == str]
    # sigs = df[df.columns[i+2]]
    idstring = ','.join(ids)

    # Query for counting how many times each distinct category appears in a list of tropes
    l1_query  = '''select c.title,c.web_url,l.category_tvt_id, count(*) as cnt from tvt_trope_category_link as l
    join tvt_category as c on l.category_tvt_id = c.tvt_id
    where l.trope_tvt_id in ('''+idstring+''')
    group by l.category_tvt_id
    order by cnt desc'''

    # Query for counting how many times each distinct second level category (parent of a category)
    # appears in a list of tropes (any further than this and I should probably switch to something like GraphLab)
    l2_query = '''select c2_title as title, c2_tvt_id as category_tvt_id, c2_web_url as web_url, count(*) as cnt from (
        select t.title as t_title,t.tvt_id as t_tvt_id,c2.title as c2_title, c2.tvt_id as c2_tvt_id, c2.web_url as c2_web_url
        from tvt_trope_category_link as l
        join trope as t on t.tvt_id = l.trope_tvt_id
        join tvt_category as c1 on l.category_tvt_id = c1.tvt_id
        join tvt_category_category_link as ccl on ccl.child_tvt_id = l.category_tvt_id
        join tvt_category as c2 on ccl.parent_tvt_id = c2.tvt_id
        where l.trope_tvt_id in ('''+idstring+''')
        group by c2_tvt_id, t.tvt_id) as sub
    group by category_tvt_id
    order by cnt desc;'''


    result1 = pd.read_sql(l1_query,engine)
    result2 = pd.read_sql(l2_query, engine)
    catset.update(result1['category_tvt_id'])
    catset.update(result2['category_tvt_id'])
    columns.append(str(i)+'_lvl1_cnt')
    columns.append(str(i)+'_lvl2_cnt')

    results[str(i)+'_lvl1_cnt'] = result1
    results[str(i)+'_lvl2_cnt'] = result2




out_df = pd.DataFrame(index = list(catset),columns = columns)

for i,id in enumerate(catset):

    for j, col in enumerate(columns[3:]):
        df = results[col]
        if id in set(df['category_tvt_id']):
            row = (df['category_tvt_id'][df['category_tvt_id'] == id]).index[0]
            if math.isnan(out_df['category_id'][id]):
                out_df['category_id'][id] = id
                out_df['category_name'][id] = df['title'][row]
                out_df['category_url'][id] = df['web_url'][row]
            out_df[col][id] = df['cnt'][row]
        else:
            out_df[col][id] = 0

out_df.to_csv(output_file)

print 'Done'





