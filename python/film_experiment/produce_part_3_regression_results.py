__author__ = 'Sam'

'''
This is a script for running most of the scripts that constitute the third major write-up I am producing for Qiaozhu,
which involves exploring some of the weird results from the one-trope-at-a-time regression experiment and running
a forward stepwise regression experiment using the tropes.
'''

import sys
import os
import platform
import run_film_regression_experiment
import subprocess

# targets = ['gross_adjusted','imdbrating']
targets = ['gross_adjusted']


if platform.system() == 'Linux' and platform.node() == 'gauss':
    output_directory_prefix = '../../../data/full_regression_results_part_3'
    rcmd = 'Rscript'
    # supports = [[15,100],[10,50]]
    supports = [[10,50]]
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    output_directory_prefix = '../../dev_data/full_regression_results_part_3'
    rcmd = 'C:/"Program Files"/R/R-3.1.1/bin/Rscript.exe'
    supports = [[3,5],[2,3]]
else:
    raise Exception('Unknown system. I do not know how to assign file names and settings')


for target in targets:
    print '<Target>:',target

    for support in supports:
        print '*****************************************************************'
        output_directory = output_directory_prefix+'/'+target+'_'+str(support[0])+'_'+str(support[1])
        print '<Support>:',support
        print '<Output going to>:',output_directory

        print '<Running run_film_regression_experiment.py>'
        #Run run_film_regression_experiment on this target with this level of support in "regression_dump" mode
        run_film_regression_experiment.main(arg_target=target,
                                            arg_mode='regression_dump',
                                            arg_non_trope_count=support[0],
                                            arg_trope_count=support[1],
                                            arg_output_directory=output_directory)


        #Run one_trope_at_a_time_regression_exp.r on the resultant .csvs
        print '<Running one_trope_at_a_time_regression_exp.r>'
        ret = subprocess.call(rcmd+' ../../r/one_trope_at_a_time_regression_exp.r '+output_directory+'> '+output_directory+'/one_trope_at_a_time_regression_exp.Rout 2>&1',shell=True)
        print '<one_trope_at_a_time_regression_exp.r finished running with return code: '+str(ret)+'>'
        #Run stepwise_regression_exp.r on the resultant .csvs
        print '<Running stepwise_regression_exp.r>'
        ret = subprocess.call(rcmd+' ../../r/stepwise_regression_exp.r '+output_directory+'> '+output_directory+'/stepwise_regression_exp.Rout 2>&1',shell=True)
        print '<stepwise_regression_exp.r finished running with return code: '+str(ret)+'>'
        #Run release_month_only_regression_exp.r on the resultant.csvs
        print '<Running release_month_only_regression_exp.r>'
        ret = subprocess.call(rcmd+' ../../r/release_month_only_regression_exp.r '+output_directory+'> '+output_directory+'/release_month_only_regression_exp.Rout 2>&1',shell=True)
        print '<release_month_only_regression_exp.r finished running with return code: '+str(ret)+'>'
        print '=================================================================='





