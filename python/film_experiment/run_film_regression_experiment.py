# -*- coding: utf-8 -*-
__author__ = 'Sam'
import pickle
import platform
# from sklearn.feature_extraction import *
# from sklearn import preprocessing, linear_model
import pandas as pd
import numpy as np
import sys
import math
import random

from sklearn.grid_search import RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from scipy.stats import randint as sp_randint
from sklearn import *
import matplotlib
import os

sys.path.append('../..')
from python import trope_util as tu

# mode = 'regression'
mode = 'regression_dump'
# mode = 'classification'




# target = 'imdbrating'
target = 'gross_adjusted'
seed = 24361109
n_iter = 150
current_date = tu.current_date_string()

split_method = 'year' #Set to 'year' to do train-test splitting on a year. Set to 'film' to randomly split across films


normalize_by_year = True
log_target = True

include_features=None
# include_features=['gross_adjusted','budget_adjusted','director','starring']
exclude_features = ['metascore']
mean_response_file = 'mean_response_by_year.png'
response_hist_file = 'response_histogram.png'

#Data matrix files
base_train_X_file = 'non_trope_train_X.csv'
supp_train_X_file = 'trope_train_X.csv'
train_target_file = 'train_y.csv'

base_test_X_file = 'non_trope_test_X.csv'
supp_test_X_file = 'trope_test_X.csv'
test_target_file = 'test_y.csv'



if platform.system() == 'Linux' and platform.node() == 'gauss':
    inputfile = '../../../data/wiki_film_feature_objects.pickle'
    output_directory = '../../../data/film_regression_results/'+mode+'_'+target+'_'+platform.node()+'_'+current_date
    debug = False
    default_min_non_trope_count = 15
    default_min_trope_count = 100
    test_fraction = 0.20
    n_jobs = 8
    show_figs = False
    matplotlib.use('Agg')
    split_year = 2008


elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    # inputfile = '../../dev_data/feature_objects_for_first_50_post_1990_films.pickle'
    inputfile = '../../dev_data/wiki_film_feature_objects.pickle'
    output_directory = '../../dev_data/film_regression_results/'+mode+'_'+target+'_'+platform.node()+'_'+current_date
    debug = True
    default_min_non_trope_count = 10
    default_min_trope_count = 50
    test_fraction = 0.20
    n_jobs = 4
    show_figs = False
    split_year = 2008


else:
    raise Exception('Unknown system. I do not know how to assign file names and settings')

import matplotlib.pyplot as plt #This has to come after the call to matplotlib.use()



def main(arg_non_trope_count = None, arg_trope_count = None, arg_mode = None,arg_target=None, arg_output_directory = None):
    global mode
    if arg_mode:
        mode = arg_mode

    global target
    if arg_target:
        target = arg_target

    global output_directory
    if arg_output_directory:
        output_directory = arg_output_directory

    print 'Running ',mode,' experiment to predict',target,'in',mode,'mode'

    if arg_non_trope_count and arg_trope_count:
        print 'Min non-trope support and min trope support specified in function arguments, so setting them to',arg_non_trope_count,'and',arg_trope_count,'respectively'
        min_non_trope_count = arg_non_trope_count
        min_trope_count = arg_trope_count
    elif len(sys.argv) >= 3:
        print 'Min non-trope support and min trope support specified in command line arguments, so setting them to',sys.argv[1],'and',sys.argv[2],'respectively'
        min_non_trope_count = int(sys.argv[1])
        min_trope_count = int(sys.argv[2])
    else:
        print 'Using default min non-trope support and min trope support of',default_min_non_trope_count,'and',default_min_trope_count,'respectively'

        min_non_trope_count = default_min_non_trope_count
        min_trope_count = default_min_trope_count

    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    with open(inputfile) as ifile:
        wffs = pickle.load(ifile)
    print len(wffs),'feature objects loaded'

    #Remove any item for which the target value is None, too low or too high, before we even begin
    missing_target_wffs = []
    ok_wffs= []
    missing_target_count = 0
    too_low_gross_count = 0
    too_high_gross_count = 0
    for wff in wffs:
        if target not in wff.features or wff.features[target] is None:
            missing_target_count += 1
            missing_target_wffs.append(wff)
        elif wff.features['gross_adjusted'] < 100000:
            too_low_gross_count += 1
            # print '\tToo low: ',wff.title,': ', wff.features['gross_adjusted']
        elif wff.features['gross_adjusted'] > 4000000000:
            too_high_gross_count += 1
            # print '\tToo high: ',wff.title,': ', wff.features['gross_adjusted']
        else:
            ok_wffs.append(wff)

    random.seed(seed)
    print missing_target_count, 'examples dropped because of missing target value,including:'
    random.shuffle(missing_target_wffs)
    print '\n'.join(['\t'+x.title.encode('utf-8') for x in missing_target_wffs[0:min(len(missing_target_wffs),30)]])
    print too_low_gross_count, 'examples dropped because of too low adjusted gross'
    print too_high_gross_count, 'examples dropped because of too high adjusted gross'



    #Shuffle examples and split them into training and test sets
    print 'Shuffling WikiFilmFeature instances and splitting them into training and test sets'
    train_wffs,test_wffs = split_train_test(ok_wffs)
    print len(train_wffs),'examples set for training,',len(test_wffs),'for testing'
    
    print 'Summary of training set: '
    summarize_instances(train_wffs)

    if include_features is not None:
        print 'ONLY INCLUDING FOLLOWING NON-TROPE FEATURES IN ANALYSIS:'
        print include_features

    # Count how many time each nominal value occurs for each feature, and how many times each non-nominal
    # feature is present
    print 'Counting occurrences of features'
    non_trope_counts, trope_counts, distinct_ftr_vals = count_distinct_feature_values(train_wffs)

    print 'Finished counting'
    print distinct_ftr_vals,'distinct non-trope feature values found spread across',len(non_trope_counts),'non-trope features'
    print len(trope_counts),'distinct tropes found.'


    #Neither pandas nor scikit-learn apparently has code for creating
    #a feature matrix from data where some of the feature values are lists,
    #even though that's a relatively simple thing. So I have to do it myself.

    #figure out how many columns our data matrix is going to have and what the column names are going to be
    non_trope_column_names = []
    trope_column_names = []
    non_trope_count = 0
    trope_count = 0
    for feature_name in sorted(non_trope_counts.keys(),key=lambda x:(len_or_1(x),x)):
        if type(non_trope_counts[feature_name]) == int:
            non_trope_column_names.append(feature_name)
            non_trope_count += 1
        elif type(non_trope_counts[feature_name]) == dict:
            for feature_val in non_trope_counts[feature_name]:
                if non_trope_counts[feature_name][feature_val] >= min_non_trope_count:
                    non_trope_column_names.append((feature_name,feature_val))
                    non_trope_count += 1
        else:
            raise Exception('Unexpected object type found in the count dictionary')

    for trope in sorted(trope_counts.keys()):
        if trope_counts[trope] >= min_trope_count:
            trope_column_names.append(trope)
            trope_count += 1

    print len(non_trope_column_names)+len(trope_column_names),'feature columns to be generated (including dummies)'
    print non_trope_count,'of those are non trope features'
    print trope_count,'of those are trope features'

    non_trope_train_df,trope_train_df,train_titles,train_target_col = wff_list_to_data_frame(train_wffs,
                                                               non_trope_column_names,
                                                               trope_column_names,
                                                               non_trope_counts,
                                                               min_non_trope_count,
                                                               trope_counts,
                                                               min_trope_count,
                                                               target)

    non_trope_test_df,trope_test_df,test_titles,test_target_col = wff_list_to_data_frame(test_wffs,
                                                           non_trope_column_names,
                                                           trope_column_names,
                                                           non_trope_counts,
                                                           min_non_trope_count,
                                                           trope_counts,
                                                           min_trope_count,
                                                           target)




    # Do some normalization of the data

    if log_target:
        print 'Logging target values'
        train_target_col = np.log(train_target_col)
        test_target_col = np.log(test_target_col)


    if normalize_by_year:
        train_target_year = pd.concat([train_target_col,non_trope_train_df['year']],axis=1)
        test_target_year = pd.concat([test_target_col,non_trope_test_df['year']],axis=1)
        print 'Dividing film grosses by their average for their year'
        func = lambda x: x/x.mean()
        train_target_col = train_target_year.groupby('year').transform(func)[target]
        test_target_col = test_target_year.groupby('year').transform(func)[target]


    train_target_year = pd.concat([train_target_col,non_trope_train_df['year']],axis=1)
    test_target_year = pd.concat([test_target_col,non_trope_test_df['year']],axis=1)
    all_target_and_year = pd.concat([train_target_year,test_target_year],axis=0)
    year_means = all_target_and_year.groupby('year').mean()

    # Do some examination of the distribution of the target column
    print 'Mean value of',target,'by year:'
    print year_means
    plt.figure(1)
    plt.title('Mean '+target+' by year')
    plt.bar(year_means.index,year_means[target])
    print 'Saving this data as bar graph to ',output_directory+'/'+mean_response_file
    plt.savefig(output_directory+'/'+mean_response_file)

    plt.figure(2)
    plt.title('Histogram of '+target+' over all examples')
    all_target_and_year[target].plot(kind='hist',bins=30,normed=0)
    plt.savefig(output_directory+'/'+response_hist_file)

    if show_figs:
        plt.show()



    combined_train_df = pd.concat([non_trope_train_df,trope_train_df],axis=1)
    combined_test_df = pd.concat([non_trope_test_df,trope_test_df],axis=1)


    if mode == 'regression_dump':

        print 'Dumping data matrixes to disk so that another program (probably R) can read them in and do stuff with them'
        dump_data_matrices_to_disk(non_trope_train_df,trope_train_df,train_target_col,train_titles,
                                              non_trope_test_df,trope_test_df,test_target_col,test_titles)
    elif mode == 'regression':
        print 'Regression experiment'
        print
        print '*********************************************************************************'
        print '*********************** both trope and non-trope features ***********************'
        print '*********************************************************************************'


        run_single_shot_regression_experiment(combined_train_df,train_target_col,train_titles,
                                  combined_test_df,test_target_col,test_titles)

        print
        print '*********************************************************************************'
        print '**************************** only non-trope features ****************************'
        print '*********************************************************************************'

        run_single_shot_regression_experiment(non_trope_train_df,train_target_col,train_titles,
                                non_trope_test_df,test_target_col,test_titles)

        print
        print '*********************************************************************************'
        print '****************************** only trope features ******************************'
        print '*********************************************************************************'

        run_single_shot_regression_experiment(trope_train_df,train_target_col,train_titles,
                                trope_test_df,test_target_col,test_titles)

    elif mode == 'classification':
        print 'Classification experiment'
        print
        print '*********************************************************************************'
        print '*********************** both trope and non-trope features ***********************'
        print '*********************************************************************************'

        params = run_classification_experiment(combined_train_df,train_target_col,train_titles,
                                  combined_test_df,test_target_col,test_titles)
        print
        print '*********************************************************************************'
        print '**************************** only non-trope features ****************************'
        print '*********************************************************************************'

        run_classification_experiment(non_trope_train_df,train_target_col,train_titles,
                                non_trope_test_df,test_target_col,test_titles,params=params)

        print
        print '*********************************************************************************'
        print '****************************** only trope features ******************************'
        print '*********************************************************************************'

        run_classification_experiment(trope_train_df,train_target_col,train_titles,
                                trope_test_df,test_target_col,test_titles,params=params)
    else:
        raise Exception('Unkown mode. Please pick a valid mode')


    print 'Done!'








def split_train_test(wffs):
    print 'Splitting '+str(len(wffs))+' examples by ' + split_method

    if split_method =='film':
        random.shuffle(wffs)
        num_test = int(len(wffs)*test_fraction)
        train_wffs = wffs[:-num_test]
        test_wffs = wffs[-num_test:]
        return train_wffs,test_wffs
    elif split_method == 'year':
        print 'Splitting on year: ' + str(split_year)
        random.shuffle(wffs)
        train_wffs = [wff for wff in wffs if wff.features['year'] <= split_year]
        test_wffs = [wff for wff in wffs if wff.features['year'] > split_year]
        return train_wffs,test_wffs
    else:
        raise Exception('Unknown train-test split method. Choose either "year" or "film"')


def count_distinct_feature_values(wffs):

    non_trope_counts = {}
    distinct_ftr_vals = 0
    trope_counts = {}
    for wff in wffs:
        for feature in wff.features:
            if (include_features is None or feature in include_features) and feature not in exclude_features:


                if wff.features[feature] != None:
                    if hasattr(wff.features[feature],'__iter__'):
                        if feature not in non_trope_counts:
                            non_trope_counts[feature] = {}
                        if type(wff.features[feature]) == list :
                            for val in wff.features[feature]:
                                distinct_ftr_vals = increment(non_trope_counts[feature],val,distinct_ftr_vals)
                        elif type(wff.features[feature]) == str :
                            distinct_ftr_vals = increment(non_trope_counts[feature],wff.features[feature],distinct_ftr_vals)
                    else:
                        increment(non_trope_counts,feature)

        for trope_tuple in wff.tropes:
            increment(trope_counts,trope_tuple)

    return non_trope_counts,trope_counts,distinct_ftr_vals

def wff_list_to_data_frame(wffs,non_trope_column_names,trope_column_names,non_trope_counts,min_non_trope_count,trope_counts,min_trope_count,target):
    non_trope_data_frame = pd.DataFrame(index=np.arange(0,len(wffs)),columns = non_trope_column_names)
    non_trope_data_frame = non_trope_data_frame.fillna(0.0)

    
    trope_data_frame = pd.DataFrame(index=np.arange(0,len(wffs)),columns = trope_column_names)
    trope_data_frame = trope_data_frame.fillna(0.0)

    title_data_frame = pd.DataFrame(index=np.arange(0,len(wffs)),columns = ['title'])

    for index,example in enumerate(wffs):
        title_data_frame['title'][index]=example.title
        for feature_name in example.features.keys():
            if feature_name in non_trope_counts:
                if type(non_trope_counts[feature_name]) == int and non_trope_counts[feature_name] > min_non_trope_count:
                    non_trope_data_frame[feature_name][index] = example.features[feature_name]
                    pass
                elif type(non_trope_counts[feature_name]) == dict:
                    if example.features[feature_name] != None:
                        for feature_val in example.features[feature_name]:
                            if feature_val in non_trope_counts[feature_name] \
                                    and non_trope_counts[feature_name][feature_val] > min_non_trope_count:
                                non_trope_data_frame[(feature_name,feature_val)][index] = 1.0
                else:
                    raise Exception('Unexpected type found in count dictionary')

        for trope in example.tropes:
            if trope in trope_counts and trope_counts[trope] >= min_trope_count:
                trope_data_frame[trope][index] = 1.0

    train_target = non_trope_data_frame[target]
    non_trope_data_frame.drop(target,axis=1,inplace=True)

    return non_trope_data_frame,trope_data_frame,title_data_frame,train_target






def run_classification_experiment(train_frame,train_target,train_titles,test_frame,test_target,test_titles,params = None):
    train_X = train_frame.values
    train_y = train_target.values

    test_X = test_frame.values
    test_y = test_target.values

    #Impute missing values (No need to do this for Y, since we dropped examples missing it)
    imputer = preprocessing.Imputer(copy=True)
    train_X = imputer.fit_transform(train_X)
    test_X = imputer.transform(test_X) #Base imputation in test_X on train_X


    #Scale stuff
    X_scaler = preprocessing.StandardScaler(copy=False)
    train_X = X_scaler.fit_transform(train_X)
    test_X = X_scaler.transform(test_X)

    #Not necessary to scale y since we are quantizing instead
    # y_scaler = data_processing.StandardScaler(copy=True)
    # train_y = y_scaler.fit_transform(train_y)
    # test_y = y_scaler.transform(test_y)


    train_y,test_y,quants = quantize(train_y,test_y,5)


    if params == None:
        print 'No params specified, so running random search over parameter space w/5-fold CV'
        clf = RandomForestClassifier(n_estimators=500)
        param_dist = {"max_depth": [100,50,25,10,5, None],
                  "max_features": sp_randint(1, 11),
                  "min_samples_split": sp_randint(1, 11),
                  "min_samples_leaf": sp_randint(1, 11),
                  "bootstrap": [True, False],
                  "criterion": ["gini", "entropy"]}

        random_search = RandomizedSearchCV(clf, param_distributions=param_dist,
                                       n_iter=n_iter,cv=5,n_jobs=n_jobs)

        random_search.fit(train_X, train_y)

        clf = random_search.best_estimator_
        print 'Best parameters found:\n',clf.get_params()
        print 'Best CV accuracy: ' + str(random_search.best_score_)

    else:
        print 'Params specified, so training model with those params'
        clf = RandomForestClassifier(**params)
        cv_scores = cross_validation.cross_val_score(clf,train_X,train_y,cv=5)
        print 'CV mean accuracy:' + str(cv_scores.mean())
        clf.fit(train_X,train_y)


    guess = int(round(np.median(train_y)))
    print 'Trained model'
    examine_classification_model(clf,train_frame.columns)
    print '============================================================================================='
    print 'Training performance:'
    examine_classification_performance(clf,train_X,train_y,train_titles,'train',guess)
    print '---------------------------------------------------------------------------------------------'
    print 'Testing performance:'
    examine_classification_performance(clf,test_X,test_y,test_titles,'test',guess)
    pass

    return clf.get_params()





def examine_classification_model(model,columns):
    print 'First 40 most important features as reported by model using its built-in feature-importance metric'
    imp = []
    for i in range(len(model.feature_importances_)):
            imp.append([columns[i],model.feature_importances_[i]])


    imp = sorted(imp,key=lambda x:math.fabs(x[1]),reverse=True)
    headers = ['Feature name','Importance']
    imp_frame = pd.DataFrame(imp,columns = headers)
    print 'First 40 most important coefficients that are not large beyond all reason'
    print imp_frame[0:min(40,len(imp_frame))].to_string(line_width=200)


def quantize(arr1,arr2,num):

    its = range(100/num,100,100/num)
    pcs = np.percentile(arr1,its)
    print 'Percentiles being used for quantization:'
    for i in range(0,len(its)):
        print '\t'+str(its[i]) +'%: '+'{0:,.2f}'.format(pcs[i])

    # interval = len(combined)/num
    return convert_to_quants(arr1,pcs), convert_to_quants(arr2,pcs),pcs

def convert_to_quants(arr,quants):
    rarr = np.empty(arr.shape)
    counts = [0]*(len(quants)+1)
    for i,val in enumerate(arr):
        rarr[i] = 0
        for j,qval in enumerate(quants):
            if val > quants[j]:
                rarr[i] = j +1
        counts[int(rarr[i])] += 1

    print 'Quantization produced following list of counts: ' + str(counts)
    return rarr

def dump_data_matrices_to_disk(base_train_df,supp_train_df,train_target_col,train_titles,
                                              base_test_df,supp_test_df,test_target_col,test_titles):
    '''
    Dump data to disk so that it can be read in to R

    '''

    #Dump all training data
    base_train_X, X_imputer, X_scaler = df_to_numpy_arr(base_train_df,imputer=True,scaler=True)
    train_y,_,y_scaler = df_to_numpy_arr(train_target_col,imputer = False,scaler=True)
    supp_train_X,_,_ = df_to_numpy_arr(supp_train_df)

    dump_matrix_to_file(base_train_X,base_train_X_file,base_test_df.columns)
    dump_matrix_to_file(train_y,train_target_file,[target])
    dump_matrix_to_file(supp_train_X,supp_train_X_file,supp_test_df.columns)

    #Dump all testing data
    base_test_X, _, _ = df_to_numpy_arr(base_test_df,imputer=X_imputer,scaler=X_scaler)
    test_y,_,_ = df_to_numpy_arr(test_target_col,imputer = False,scaler=y_scaler)
    supp_test_X,_,_ = df_to_numpy_arr(supp_test_df)

    dump_matrix_to_file(base_test_X,base_test_X_file,base_test_df.columns)
    dump_matrix_to_file(test_y,test_target_file,[target])
    dump_matrix_to_file(supp_test_X,supp_test_X_file,supp_test_df.columns)



def dump_matrix_to_file(matrix,file_name,headers):
    np.savetxt(output_directory+'/'+file_name,matrix,delimiter=',',comments='',header=','.join(['"'+str(x).replace('\"','\'')+'"' for x in headers]))



def df_to_numpy_arr(df,imputer = None, scaler = None):
    '''
    Convert data frame to Numpy array, scaling and doing missing data imputation if necessary
    :param df:
    :param imputer: False, True, or a preexisting Imputer
    :param scaler: False, True, or a preexisting Scaler
    :return:
    '''
    arr = np.copy(df.values)
    if imputer:
        if type(imputer) != preprocessing.Imputer:
            imputer = preprocessing.Imputer(copy=False)
            arr = imputer.fit_transform(arr)
        else:
            arr = imputer.transform(arr)
    else:
        imputer = None

    if scaler:
        if type(scaler) != preprocessing.StandardScaler:
            scaler = preprocessing.StandardScaler(copy=False)
            arr = scaler.fit_transform(arr)
        else:
            arr = scaler.transform(arr)
    else:
        scaler = None

    return arr,imputer,scaler

def run_single_shot_regression_experiment(train_frame,train_target,train_titles,test_frame,test_target,test_titles):
    '''
    Run a regression experiment
    :param data_frame:
    :param example_list:
    :return:
    '''


    # Convert from data frames to numpy arrays, with scaling and missing data imputation
    train_X,X_imputer,X_scaler = df_to_numpy_arr(train_frame,imputer=True,scaler=True)
    train_y,_,y_scaler = df_to_numpy_arr(train_target,imputer=False,scaler=True)

    test_X,_,_ = df_to_numpy_arr(test_frame,imputer=X_imputer,scaler=X_scaler)
    test_y,_,_ = df_to_numpy_arr(test_target,imputer=False,scaler=y_scaler)


    # Create linear regression object
    regr = linear_model.LinearRegression()

    # Train the model using the training sets
    print 'Training data X matrix shape:',train_X.shape
    print 'Training data y matrix shape:',train_y.shape

    regr.fit(train_X, train_y)
    # regr.fit(data_matrix, target_matrix)


    # The coefficients
    # print('Coefficients: \n', regr.coef_)
    y_mean_val = np.average(train_y)


    print 'Trained model'
    examine_regression_model(regr,train_frame.columns,y_scaler)
    print '============================================================================================='
    print 'Training performance:'
    examine_regression_performance(regr,train_X,train_y,y_scaler,train_titles,'train',y_mean_val)
    print '---------------------------------------------------------------------------------------------'
    print 'Testing performance:'
    examine_regression_performance(regr,test_X,test_y,y_scaler,test_titles,'test',y_mean_val)

    return

def examine_regression_model(model,col_names,scaler):
    coefs = []
    for i in range(len(model.coef_)):
        if math.fabs(model.coef_[i]) <= 10.0:
            coefs.append([col_names[i],model.coef_[i],'{0:,.2f}'.format(float(scaler.inverse_transform(model.coef_[i])))])

    coefs.append(['Intercept',model.intercept_,'{0:,.2f}'.format(float(scaler.inverse_transform(model.intercept_)))])

    coefs = sorted(coefs,key=lambda x:math.fabs(x[1]),reverse=True)
    headers = ['Feature name','Coefficient','Re-scaled coefficient']
    coef_frame = pd.DataFrame(coefs,columns = headers)
    print 'First 40 most important coefficients that are not large beyond all reason'
    print coef_frame[0:min(40,len(coef_frame))].to_string(line_width=200)


def examine_regression_performance(model,X,y,scaler,title_frame,type,y_mean_val):

    print 'First 40',type,' regression predictions:'
    y_mean = y_mean_val * np.ones(y.shape)
    y_pred = model.predict(X)
    predictions = []
    predictions.append(['Title','Actual','Predicted','Mean'])
    for i in range(len(y)):
        predictions.append( [title_frame['title'].iloc[i],
                             '{0:,.2f}'.format(float(scaler.inverse_transform(y[i]))),
                             '{0:,.2f}'.format(float(scaler.inverse_transform(y_pred[i]))),
                             '{0:,.2f}'.format(float(scaler.inverse_transform(y_mean[i])))])

    predictions = pd.DataFrame(predictions)
    print predictions[0:min(40,len(predictions))].to_string(line_width=200)

    # The mean square error
    print("Residual sum of squares: %.2f"
          % np.mean((model.predict(X) - y) ** 2))
    # Explained variance score: 1 is perfect prediction
    print('Variance score: %.2f' % model.score(X, y))

    #Against a mean baseline
    print("Residual sum of squares for just guessing the mean value: %.2f"
          % np.mean((y_mean_val - y) ** 2))

from sklearn.metrics import *
def examine_classification_performance(model,X,y,title_frame,type,y_naive_guess):

    y_guesses = y_naive_guess * np.ones(y.shape)
    y_pred = model.predict(X)

    print 'Model Performance:'
    print("\tAccuracy: %.2f"
          % metrics.accuracy_score(y,y_pred))
        # The mean square error
    print("\tResidual sum of squares: %.2f"
          % np.mean((y_pred - y) ** 2))




    # print("\tF1: %.2f"
    #   % metrics.f1_score(y,y_pred))
    #
    # print("\tRecall: %.2f"
    #   % metrics.recall_score(y,y_pred))
    #
    # print("\tPrecision: %.2f"
    #   % metrics.precision_score(y,y_pred))

    print 'Baseline performance:'
    print("\tAccuracy: %.2f"
          % metrics.accuracy_score(y,y_guesses))
        #Against a mean baseline
    print("\tResidual sum of squares: %.2f"
          % np.mean((y_naive_guess - y) ** 2))

    # print("\tF1: %.2f"
    #   % metrics.f1_score(y,y_guesses))
    #
    # print("\tRecall: %.2f"
    #   % metrics.recall_score(y,y_guesses))
    #
    # print("\tPrecision: %.2f"
    #   % metrics.precision_score(y,y_guesses))

    print 'First 40',type,'predictions:'

    predictions = []
    predictions.append(['Title','Actual','Predicted','Mean'])
    for i in range(len(y)):
        predictions.append( [title_frame['title'].iloc[i],
                             y[i],
                             y_pred[i],
                             y_guesses[i]])

    predictions = pd.DataFrame(predictions)
    print predictions[0:min(40,len(predictions))].to_string(line_width=200)



def increment(count_dict,val, count = None):
    if not val in count_dict:
        count_dict[val] = 1
        if count != None:
            count += 1
    else:
        count_dict[val] +=1
    return count

def summarize_instances(wffs):
    # Non-trope features
    # feature_name:[num_present,num_missing,count,value]
    summary = {'tropes':[0.0,0.0,0.0,0.0]}
    for wff in wffs:
        for feature in wff.features:
            if not feature in summary:
                summary[feature] = [0.0,0.0,0.0,0.0]
            val = wff.features[feature]
            if val is None:
                summary[feature][1] += 1
            elif type(val) == list:
                if len(val) == 0:
                    summary[feature][1] += 1
                else:
                    summary[feature][0] += 1
                    summary[feature][2] += len(val)
            elif type(val) == str or type(val) == unicode:
                summary[feature][0] += 1
                summary[feature][2] += 1
            else:
                summary[feature][0] += 1
                summary[feature][3] += val
        tropes = wff.tropes
        if len(tropes) == 0:
            summary['tropes'][1] += 1
        else:
            summary['tropes'][0] += 1
            summary['tropes'][2] += len(tropes)

    print 'Example set summary:'
    print str(len(wffs)) + ' examples'
    for feature,vals in summary.items():
        print '\n',feature
        print '\tMissing:',vals[1],'({:.1f}%)'.format(100*vals[1]/(vals[0]+vals[1]))
        print '\tPresent:',vals[0],'({:.1f}%)'.format(100*vals[0]/(vals[0]+vals[1]))
        print '\tAverage count (when present): {:.2f}'.format(vals[2]/(vals[0]))
        print '\tAverage value (when present): {0:,.2f}'.format(vals[3]/(vals[0]))

def len_or_1(obj):
    if hasattr(obj,'__len__'):
        return len(obj)
    elif obj is not None:
        return 1
    else:
        return len(obj)


if  __name__ =='__main__':
    main()
