__author__ = 'Sam'
import re

input_file_name = '../../data/downloaded_rdf_triples_for_matched_wiki_films.nt'
output_file_name = '../../data/cleaned_downloaded_rdf_triples_for_matched_wiki_films.nt'
error_file_name = '../../data/trashed_downloaded_rdf_triples_for_matched_wiki_films.nt'


def main():
    print 'Clearing HTML out of',input_file_name,'and saving cleaned file to',output_file_name
    print 'Saving error lines to',error_file_name
    ifile = open(input_file_name,'r')
    ofile = open(output_file_name,'w')
    efile = open(error_file_name,'w')

    icount = 0
    ocount = 0
    ecount = 0
    html_block = False
    for line in ifile:
        icount += 1
        if re.match('\s*<html>\s*',line):
            html_block = True

        if html_block:
            efile.write(line)
            ecount += 1
        else:
            ofile.write(line)
            ocount += 1

        if re.match('\s*</html>\s*',line):
            html_block = False

    ifile.close()
    ofile.close()
    efile.close()

    print icount,'total lines read'
    print ocount,'of those (',(100*ocount/float(icount)),'%) written to cleaned version'
    print ecount,'of those (',(100*ecount/float(icount)),'%) written to error lines file'


if  __name__ =='__main__':
    main()