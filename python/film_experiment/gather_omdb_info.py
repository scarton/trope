__author__ = 'Sam'
import platform
import pandas
import json
from sqlalchemy import *
import urllib
import sys
import time

if platform.system() == 'Linux' and platform.node() == 'gauss':
    output_file = '../../../data/OMDB_info_for_post_1990_films.json' #for deployment on gauss
    query = 'select a.link_id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.year from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id' #For deployment on gauss

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    output_file = '../../dev_data/OMDB_info_for_50_post_1990_films.json' #for local testing
    query = 'select a.link_id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.year from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id limit 50' #for local testing
else:
    raise Exception('Unknown system. I do not know how to assign file names')

def main():

    print 'Loading OMDB information from OMDB API'

    with open('../db.config') as c:
        config = json.load(c)['servers']['gauss']

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    # Select all films into a data frame
    films = pandas.read_sql(query,engine)

    gathered = {}
    ofile = open(output_file,'a+')
    for line in ofile:
        obj = json.loads(line)
        gathered[obj['wiki_id']] = obj


    print len(gathered),'objects loaded from file'

    count = 0
    for index in films.index:


        if films.wiki_id[index].item() not in gathered:
            try:
                title = films.title[index]
                title = title.encode('utf8')
                api_title = urllib.quote(title)
                url = 'http://www.omdbapi.com/?t='+str(api_title)+'&plot=short&r=json'
                print title,api_title,url
                obj = json.loads(urllib.urlopen(url).read())
                obj['wiki_id'] = films.wiki_id[index].item()
                print '\t'+str(len(obj)) + 'fields in json'
                ofile.write(json.dumps(obj))
                ofile.write('\n')
                count += 1
                time.sleep(0.5)
            except Exception as ex:
                sys.stderr.write(ex.message)
        else:
            print 'Film',films.title[index].encode('utf-8'),'has already been downloaded.'


    ofile.close()
    print count,'items downloaded from API. Finished.'


if  __name__ =='__main__':
    main()

