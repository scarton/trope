# -*- coding: utf-8 -*-
__author__ = 'Sam'
import json
from sqlalchemy import *
import pandas
import rdflib
import urllib
import sys
import time
# import urllib2
# from rdflib.namespace import RDF
import re
import platform
# wiki_file_directory = '../dev_data/first_50_post_2000_films'
# rdf_filename = '../dev_data/rdf_triples_for_first_50_post_2000_films.nt'

sys.path.append('../..')
from python.db_entity.entity import *

from python import trope_util as tu


if platform.system() == 'Linux' and platform.node() == 'gauss':
    output_file = '../../../data/downloaded_rdf_triples_for_matched_wiki_films.nt' #for deployment on gauss
    query = 'select a.link_id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.year from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id' #For deployment on gauss

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    output_file = '../../dev_data/downloaded_rdf_triples_for_first_50_post_1990_films.nt' #for local testing
    query = 'select a.link_id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.year from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id limit 50' #for local testing
else:
    raise Exception('Unknown system. I do not know how to assign file names')


def main():

    # raise Exception('This code is currently broken. It is overwriting the previous triples ifle rather than adding to it.')

    with open('../db.config') as c:
        config = json.load(c)['servers']['gauss']

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])


    films = pandas.read_sql(query,engine)

    print str(len(films)) + ' films found. '

    print 'Loading previously downloaded triples'
    g = rdflib.Graph()
    try:
        g.parse(output_file,format='nt')
    except Exception as ex:
        sys.stderr.write(ex.message)

    print len(g),'previously downloaded triples found.'

    out = open(output_file,'a+')
    print 'Loading RDF tiples from DBPedia.org for each film and writing them to ' + output_file

    count = 0
    errors = 0
    downloads = 0
    skips = 0
    for index in films.index:
        count +=1
        if count % 50 == 1:
            print str(count) + ' films accounted for.'
        web_title = tu.wiki_to_web_title(films.title[index])


        print web_title

        ref = rdflib.URIRef('http://dbpedia.org/resource/'+web_title)
        objs = g.triples((ref,None,None))
        num = sum(1 for x in objs)
        if num > 0:
            print num,'triples have already been collected for this film. Skipping.'
            skips += 1
        else:
            try:
                url = 'http://dbpedia.org/data/'+web_title+'.ntriples'
                content = urllib.urlopen(url.encode("UTF-8")).read()
                downloads += 1
                print len(content),'characters:', url
                out.write(content)
                time.sleep(0.5)
            except Exception as ex:
                sys.stderr.write(ex.message)
                sys.stderr.write('Continuing loop')
                errors +=1


    out.close()
    print 'Done', count , 'films downloaded total.'
    print errors,'errors encountered.'
    print skips,'skipped because we already had triples for them'
    print downloads,'sets of triples downloaded.'


if  __name__ =='__main__':
    main()