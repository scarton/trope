# -*- coding: utf-8 -*-

__author__ = 'Sam'
import rdflib
from rdflib.namespace import RDF

from sqlalchemy import *
import pandas
import json
import re
import urllib
import pickle
import mwparserfromhell
import sys
import requests
import platform
sys.path.append('../..')
from python import trope_util as tu

from WikiFilmFeatures import *

# For deployment on Gauss
if platform.system() == 'Linux' and platform.node() == 'gauss':
    ntfile = '../../../data/cleaned_downloaded_rdf_triples_for_matched_wiki_films.nt'
    outputfile = '../../../data/wiki_film_feature_objects.pickle'
    query = 'select a.link_id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.year from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id'
    wiki_file_directory = '../../../data/wiki_films'
    omdbfile='../../../data/OMDB_info_for_post_1990_films.json'
    debug = False
# For local testing
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    ntfile = '../../dev_data/downloaded_rdf_triples_for_first_50_post_1990_films.nt'
    outputfile = '../../dev_data/feature_objects_for_first_50_post_1990_films.pickle'
    query = 'select a.link_id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.year from wiki_tvt_work_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.year >= 1990) order by a.wiki_id limit 50'
    wiki_file_directory = '../../dev_data/first_50_post_2000_films'
    omdbfile = '../../dev_data/OMDB_info_for_50_post_1990_films.json'
    debug = True
else:
    raise Exception('This code is not running on a recognized system, so it does not know where to read files from or write them to')


#Invariant
query2 = """select wiki.*,tlink.trope_tvt_id as trope_id,trope.title as trope_title, trope.file_name as trope_file_name
from ("""+query+""") as wiki
join tvt_work_trope_link as tlink
on (wiki.tvt_id = tlink.work_tvt_id)
join trope
on (tlink.trope_tvt_id = trope.tvt_id);"""


def main():
    print 'Generating feature representations of films'
    g = rdflib.Graph()
    g.parse(ntfile,format='nt')
    print len(g),'rdf triples found.'

    with open('../db.config') as c:
        config = json.load(c)['servers']['gauss']

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])
    # conn = engine.connect()

    omdb_jsons = {}
    ofile = open(omdbfile,'r')
    for line in ofile:
        obj = json.loads(line)
        omdb_jsons[obj['wiki_id']] = obj


    # Select all films into a data frame
    ftrs = []
    ftr_map = {}
    films = pandas.read_sql(query,engine)
    count = 0
    print 'Extracting non-trope-related features about each film'
    for index in films.index:
        count += 1
        # if films.title[index] != 'Lara Croft: Tomb Raider':
        #      continue
        ftr = WikiFilmFeatures(films, index)
        web_title = tu.wiki_to_web_title(films['title'][index])
        if debug: print web_title
        ref = rdflib.URIRef('http://dbpedia.org/resource/'+web_title)


        #Features from DBPedia triples
        ftr.features['writer'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/ontology/writer','http://dbpedia.org/property/writer'])
        ftr.features['director'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/director','http://dbpedia.org/ontology/director'])
        ftr.features['starring'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/starring','http://dbpedia.org/ontology/starring'])
        ftr.features['studio'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/studio','http://dbpedia.org/ontology/studio'])
        ftr.features['producer'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/producer','http://dbpedia.org/ontology/producer'])
        ftr.features['runtime'] = graceful_max(extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/runtime','http://dbpedia.org/ontology/runtime'],process_and_adjust_runtime))
        # ftr.features['country'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/country','http://dbpedia.org/ontology/country'])
        ftr.extra['budget'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/budget','http://dbpedia.org/ontology/budget'],process_number)
        ftr.features['budget_adjusted'] = graceful_max([adjust_dollar_amount_for_inflation(x,ftr.release_year) for x in ftr.extra['budget']])
        ftr.extra['gross'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/gross','http://dbpedia.org/ontology/gross'],process_number)
        ftr.features['gross_adjusted'] = graceful_max([adjust_dollar_amount_for_inflation(x,ftr.release_year) for x in ftr.extra['gross']])
        ftr.features['genre'] = extract_dbpedia_feature(g,ref,['http://dbpedia.org/property/genre','http://dbpedia.org/ontology/genre'])

        #Features from OMDB API
        wiki_id = films.wiki_id[index]
        try:
            if wiki_id in omdb_jsons:
                omdb_obj = omdb_jsons[wiki_id]
                ftr.features['rating'] = [omdb_obj['Rated']]
                ftr.features['genre'] = comma_split(omdb_obj['Genre'])
                ftr.features['release_month'] = extract_release_months(omdb_obj['Released'])
                ftr.features['language'] = comma_split(omdb_obj['Language'])
                ftr.features['country'] = comma_split(omdb_obj['Country'])
                ftr.features['metascore'] = na_float(omdb_obj['Metascore'])
                ftr.features['imdbrating'] = na_float(omdb_obj['imdbRating'])

        except Exception as ex:
            # print web_title
            # print omdb_jsons[wiki_id]
            # raise ex
            sys.stderr.write(ex.message)



        # #Features from wikitext
        # file_name = films.file_name[index]
        # file = open(wiki_file_directory+'/'+file_name,'r')
        # wikitext = file.read().decode('utf8')
        # file.close()
        #
        # # It turns out that the best way to see how the text is actually rendered is to replace all <br> tags
        # # with newlines and then strip out all the HTML, so do the first part of that here and the second part later on
        # wikitext = re.sub('<br ?/?>','\n',wikitext)
        #
        # wikicode = mwparserfromhell.parse(wikitext)
        # try:
        #     infobox=wikicode.filter_templates(matches='infobox')[0]
        #     for param in infobox.params:
        #         entities =  extract_entities_from_infobox_param_value(param.value)
        #         if re.search('[\s]*release[\s]*',str(param.name),re.IGNORECASE):
        #             ftr.features['release_month'] = extract_release_months(param.value)
        # except Exception as ex:
        #     sys.stderr.write(ex.message)
        #     sys.stderr.write('Error occurred in film: '+str(count)+ ' ' + web_title)


        if debug:
            triples = list(g.triples((ref,None,None)))
            print web_title
            print str(len(triples)) + ' triples found for this film.'
        else:
            if count % 50 == 0:
                print '\t',count, 'films scanned...'
        # print ftr



        ftrs.append(ftr)
        ftr_map[ftr.tvt_id] = ftr
        if debug: print '----------------------------------------------------------------'

    print 'Done.',len(films),'films scanned...'
    print 'Figuring out which movies have which tropes'
    film_tropes = pandas.read_sql(query2,engine)
    print len(film_tropes),'trope occurrences found for the set of films'
    for index in film_tropes.index:
        if film_tropes.tvt_id[index] in ftr_map:
            ftr = ftr_map[film_tropes.tvt_id[index]]
            ftr.tropes.append((film_tropes.trope_id[index],film_tropes.trope_title[index]))

    if debug:
        print 'Full feature set: \n'
        for ftr in ftrs:
            print ftr
            print '---------------------------------------------------'

    # Dump our feature collection objects to a file
    print 'Pickling',len(ftrs),'feature objects to', outputfile
    with open(outputfile,'w') as f:
        pickle.dump(ftrs,f)

    print 'Done. Have a great day.'

def na_float(astr):
    if astr == 'N/A':
        return None
    else:
        return float(astr)

def comma_split(astr):
    lst = astr.split(',')
    return [x.lower().strip() for x in lst]

def graceful_max(coll):
    if hasattr(coll,'__iter__'):
        if len(coll) > 0:
            return max(coll)
        else:
            return None
    else:
        return coll

def normalize_string(string):
    rlist = []
    string_list = re.split('\n,',string)
    for astr in string_list:
        adjusted = astr
        try:
            adjusted = adjusted.encode('utf8')
        except UnicodeEncodeError:
            pass

        adjusted = re.sub(' ','_',adjusted.strip())
        adjusted = re.sub('\([^\(\)]*\)','',adjusted)
        adjusted = re.sub('^\*_','',adjusted)
        adjusted = re.sub('_+$','',adjusted)

        # if not '%' in astr:
        #     adjusted = safe_percent_encode(adjusted)
        if '%' in adjusted:
            adjusted = urllib.unquote(adjusted)

        rlist.append(adjusted.lower())

    return rlist


def extract_dbpedia_feature(graph,sub_ref,pred_refs,func = normalize_string):
    rList = set()
    dblist = []

    if not type(pred_refs) == list:
        pred_refs = [pred_refs]

    for pred_ref in pred_refs:
        if not type(pred_ref) == rdflib.URIRef:
            pred_ref = rdflib.URIRef(pred_ref)
        obj_refs = graph.triples((sub_ref,pred_ref,None))
        for s,p,obj_ref in obj_refs:
            if func != None:
                dblist.append(process_dbpedia_uri_ref(obj_ref))
                val = func(process_dbpedia_uri_ref(obj_ref))
                if type(val) == list:
                    for val_elmnt in val:
                        rList.add(val_elmnt)
                elif val != None:
                    rList.add(val)

            else:
                rList.add(process_dbpedia_uri_ref(obj_ref))

    return list(rList)

def process_dbpedia_uri_ref(uri_ref):
    return uri_ref.split('/')[-1]


def safe_percent_encode(astr):
    return urllib.quote(astr,safe="\",():;'!&/*")


def extract_entities_from_infobox_param_value(param_value):
    '''
    This takes a the value of an infobox parameter such as [u' Roderick Jaynes', u'<br />', u'[[Tricia Cooke]]', u'\n'] for the key 'editing' in the article on O Brother Where Art Thou
    and returns a list of the distinct entities in the value. For Wikilinks, it shows the display text rather than the URL text. So for this one, it would return
    ['Roderick Jaynes','Tricia Cooke']
    :param param_value: a mwparserfromhell.wikicode.Wikicode object
    :return: a list of strings
    '''
    entity_list = []

    # Sometimes instead of an HTML list, the infobox has a list template
    # In which case we need to dig out the contents of that template
    # And then do our thing on them
    tpls = param_value.filter_templates()
    if len(tpls) == 1 and 'list' in tpls[0].name.lower():
        param_value = tpls[0].params[0].value

    stripped = param_value.strip_code().strip()
    lines = re.split('\n',stripped)
    for line in lines:
        lstr = re.sub('\(.*\)','',line).strip()
        if line != '':
            entity_list.append(lstr)

    return entity_list

def process_number(num_str):
    try:
        num = float(num_str)
        return num
    except Exception as ex:
        mod_map = {'thousand':1000.0,'million':1000000.0,'billion':1000000000.0}
        for mod in mod_map:
            match = re.search('([0-9]+\.?[0-9]*) '+mod,num_str)
            if match:
                return float(match.group(1))*mod_map[mod]
        sys.stderr.write(ex.message)
        return None


def process_and_adjust_runtime(time_str):
    time_num = process_number(time_str)
    if time_num != None:
        if time_num > 1000:
            time_num /= 60

    return time_num


def extract_release_months(wikicode):
    '''
    Extract all months from a wikicode object. Looks for both text of month names, and numbers corresponding to
    release months in  film release templates like this one:
    {{Film date|2000|7|6|Hong Kong|2000|7|7|Taiwan|2000|12|8|United States}}
    for crouching tiger hidden dragon
    :param wikicode:
    :return:
    '''
    month_dict = {'january':1,'jan':1,
    'february':2,'feb':2,
    'march':3,'mar':3,
    'april':4,'apr':4,
    'may':5,
    'june':6, 'jun':6,
    'july':7,'jul':7,
    'august':8,'aug':8,
    'september':9,'sep':9,
    'october':10,'oct':10,
    'november':11,'nov':11,
    'december':12,'dec':12}

    wikistr = wikicode.encode('utf8').lower()
    months = set()
    # Look for month names
    for name in month_dict:
        if re.search(name,wikistr):
            months.add(month_dict[name])

    # Look for month number patterns
    for m in re.finditer("[0-9]{4}\|([0-9]{1,2})\|[0-9]{1,2}", wikistr):
        months.add(int(m.group(1)))

    return list(months)


inflation_adjustment_cache = {}
def adjust_dollar_amount_for_inflation(amount,org_year,target_year=2015):
    '''
    Adjust a dollar amount for inflation based on the original year and a target year
    :param amount:
    :param org_year:
    :param target_year:
    :return:
    '''

    if org_year not in inflation_adjustment_cache or target_year not in inflation_adjustment_cache[org_year]:
        params = {
            'country':'united-states',
            'start':str(org_year)+'/1/1',
            'end':str(target_year)+'/1/1',
        }
        r = requests.get('https://www.statbureau.org/calculate-inflation-rate-json',params=params)
        multiplier = 1+float(r.json())/100

        if org_year not in inflation_adjustment_cache: inflation_adjustment_cache[org_year] = {}
        inflation_adjustment_cache[org_year][target_year] = multiplier
        # print 'Multiplier between years ' + str(org_year) + ' and ' + str(target_year) + ' is ' + str(multiplier)
    else:
        multiplier = inflation_adjustment_cache[org_year][target_year]

    return multiplier * amount




if  __name__ =='__main__':
    main()

