__author__ = 'Sam'
from sqlalchemy import *
import pandas
import mwparserfromhell
from mediawiki_parser.preprocessor import make_parser
from mediawiki_parser.html import make_parser
import re
import requests
import math
import json

'''
This script scans through Wikipedia film articles, parses their contents and pulls out features
such as how much money they made, who produced them, etc. It saves the output to a pickle file
of a list of WikiFilmFeatures object

After tweaking this a bunch, I realize that Wikipedia is just too inconsistent in how it formats
for me to easily parse infoboxes on my own. I am gonna have to use a combination of DBPedia and the
OMDB API to get what I want. I will do that in a different script.
'''
wiki_file_directory = '../dev_data/first_50_post_2000_films'

max_count = 10
def main():
    with open('db.config') as c:
        config = json.load(c)['servers']['gauss']

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])    # conn = engine.connect()

    # Select all films into a data frame
    query = 'select a.id, a.tvt_id,a.wiki_id,b.title,b.file_name,b.release_year from wiki_tvtropes_link as a join wiki_work as b on (a.wiki_id = b.wiki_id ) where (b.release_year >= 2000) order by a.wiki_id limit 50'
    films = pandas.read_sql(query,engine)
    # print films.to_string(line_width = 200)
    # print str(len(films)) + ' films found.'

    film_features= []
    count = 0
    for index in films.index:
        count += 1
        if count >= max_count:
            return(0)
        print '#' + str(count)

        file_name = films.file_name[index]
        file = open(wiki_file_directory+'/'+file_name,'r')
        # file = open('../o_brother_where_art_thou.wiki.txt','r')
        wikitext = file.read().decode('utf8')
        file.close()

        # It turns out that the best way to see how the text is actually rendered is to replace all <br> tags
        # with newlines and then strip out all the HTML, so do the first part of that here and the second part later on
        wikitext = re.sub('<br ?/?>','\n',wikitext)

        wikicode = mwparserfromhell.parse(wikitext)
        infobox=wikicode.filter_templates(matches='infobox')[0]

        # print '\n'.join(str(x) for x in infobox.params)

        ftr =  WikiFilmFeatures(films, index)
        for param in infobox.params:
            entities =  extract_entities_from_infobox_param_value(param.value)

            # print param.name
            # print param.value
            # print entities
            # print '-------------------------------------'
            if re.search('[\s]*director[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['director'] = entities
            elif re.search('[\s]*starring[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['starring'] = entities
            elif re.search('[\s]*writ((er)|(ten by))[\s]*',str(param.name),re.IGNORECASE) or re.search('[\s]*screenplay[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['writer'] = entities
            elif re.search('[\s]*produce[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['producer'] = entities
            elif re.search('[\s]*studio[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['studio'] = entities
            elif re.search('[\s]*distributor[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['distributor'] = entities
            elif re.search('[\s]*runtime[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['runtime'] = entities
            elif re.search('[\s]*country[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['country'] = entities
            elif re.search('[\s]*language[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['language'] = entities
            elif re.search('[\s]*budget[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['budget'] = parse_money_amount(str(param.value))
                ftr.features['budget_adjusted'] = adjust_dollar_amount_for_inflation(ftr.features['budget'],films.release_year[index])
            elif re.search('[\s]*gross[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['gross'] = parse_money_amount(param.value.encode('utf-8'))
                ftr.features['gross_adjusted'] = adjust_dollar_amount_for_inflation(ftr.features['gross'],films.release_year[index])
            elif re.search('[\s]*release[\s]*',str(param.name),re.IGNORECASE):
                ftr.features['release_month'] = extract_release_months(param.value)

        print ftr
        print '========================================================'
        if count == 9:
            pass

def extract_entities_from_infobox_param_value(param_value):
    '''
    This takes a the value of an infobox parameter such as [u' Roderick Jaynes', u'<br />', u'[[Tricia Cooke]]', u'\n'] for the key 'editing' in the article on O Brother Where Art Thou
    and returns a list of the distinct entities in the value. For Wikilinks, it shows the display text rather than the URL text. So for this one, it would return
    ['Roderick Jaynes','Tricia Cooke']
    :param param_value: a mwparserfromhell.wikicode.Wikicode object
    :return: a list of strings
    '''
    entity_list = []

    # Sometimes instead of an HTML list, the infobox has a list template
    # In which case we need to dig out the contents of that template
    # And then do our thing on them
    tpls = param_value.filter_templates()
    if len(tpls) == 1 and 'list' in tpls[0].name.lower():
        param_value = tpls[0].params[0].value

    stripped = param_value.strip_code().strip()
    lines = re.split('\n',stripped)
    for line in lines:
        lstr = re.sub('\(.*\)','',line).strip()
        if line != '':
            entity_list.append(lstr)

    return entity_list

inflation_adjustment_cache = {}
def adjust_dollar_amount_for_inflation(amount,org_year,target_year=2015):
    '''
    Adjust a dollar amount for inflation based on the original year and a target year
    :param amount:
    :param org_year:
    :param target_year:
    :return:
    '''

    if org_year not in inflation_adjustment_cache or target_year not in inflation_adjustment_cache[org_year]:
        params = {
            'country':'united-states',
            'start':str(org_year)+'/1/1',
            'end':str(target_year)+'/1/1',
        }
        r = requests.get('https://www.statbureau.org/calculate-inflation-rate-json',params=params)
        multiplier = 1+float(r.json())/100

        if org_year not in inflation_adjustment_cache: inflation_adjustment_cache[org_year] = {}
        inflation_adjustment_cache[org_year][target_year] = multiplier
        # print 'Multiplier between years ' + str(org_year) + ' and ' + str(target_year) + ' is ' + str(multiplier)
    else:
        multiplier = inflation_adjustment_cache[org_year][target_year]

    return multiplier * amount



    # # Select all film occurrences into a data frame
    # query2 = 'select t1.*, t2.trope_id,t3.title as trope_title, t3.file_name as trope_file_name from ('+query+') as t1 ' \
    #                        'join work_trope_link as t2 on (t1.tvt_id = t2.tvt_id) ' \
    #                        'join trope as t3 on (t2.trope_id = t3.id)'
    # trope_occurrences = pandas.read_sql(query2, engine)
    # print trope_occurrences.to_string(line_width = 200)
    # print str(len(trope_occurrences)) + ' trope occurrences found.'


    # conn.close()exit

def extract_release_months(wikicode):
    '''
    Extract all months from a wikicode object. Looks for both text of month names, and numbers corresponding to
    release months in  film release templates like this one:
    {{Film date|2000|7|6|Hong Kong|2000|7|7|Taiwan|2000|12|8|United States}}
    for crouching tiger hidden dragon
    :param wikicode:
    :return:
    '''
    month_dict = {'january':1,'jan':1,
    'february':2,'feb':2,
    'march':3,'mar':3,
    'april':4,'apr':4,
    'may':5,
    'june':6, 'jun':6,
    'july':7,'jul':7,
    'august':8,'aug':8,
    'september':9,'sep':9,
    'october':10,'oct':10,
    'november':11,'nov':11,
    'december':12,'dec':12}

    wikistr = str(wikicode)
    months = set()
    # Look for month names
    for name in month_dict:
        if re.search(name,wikistr):
            months.add(month_dict[name])

    # Look for month number patterns
    for m in re.finditer("[0-9]{4}\|([0-9]{1,2})\|[0-9]{1,2}", wikistr):
        months.add(int(m.group(1)))

    return list(months)


def parse_money_amount(money_str):
    new_str = re.sub('\$,','',money_str)
    num_str = re.search('[0-9]+\.?[0-9]*',new_str).group(0)
    gross = float(num_str)
    mod = re.search('(thousand)|(million)|(billion)',new_str)
    if mod:
        mod = mod.group(0)
        if mod == 'thousand':
            gross *= 1000
        elif mod == 'million':
            gross *= 1000000
        elif mod  == 'billion':
            gross *= 1000000000
    return gross



class WikiFilmFeatures():

    def __init__(self,df=pandas.DataFrame,index=None):

        if not df.empty and index != None:
            self.tvt_id = df.tvt_id[index]
            self.wiki_id = df.wiki_id[index]
            self.title = df.title[index]
            self.file_name = df.file_name[index]
            self.release_year = df.release_year[index]
        else:
            self.tvt_id = None
            self.wiki_id = None
            self.title = None
            self.file_name = None
            self.release_year = None

        self.features = {}
        self.features['producer'] = None
        self.features['director'] = None
        self.features['writer'] = None
        self.features['budget'] = None
        self.features['year'] = None
        self.features['budget_adjusted'] = None
        self.features['gross'] = None
        self.features['gross_adjusted'] = None
        self.features['starring'] = None
        self.features['genre'] = None
        self.features['release_month'] = None
        self.features['runtime'] = None

    def __str__(self):
        mystr= 'DB TVTropes ID: '+str(self.tvt_id) + \
        '\nDB Wikipedia ID: '+str(self.wiki_id) + \
        '\nDB Title: '+self.title.encode('utf-8') + \
        '\nDB File name: '+str(self.file_name) + \
        '\nDB Release year: '+str(self.release_year) + \
        '\nFeatures:'

        for key in self.features:
            mystr += '\n\t'+key+': ' + str(self.features[key])

        return mystr


if  __name__ =='__main__':
    main()