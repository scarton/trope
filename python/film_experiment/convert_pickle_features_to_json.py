__author__ = 'Sam'
import platform
import pickle

if platform.system() == 'Linux' and platform.node() == 'gauss':
    inputfile = '../../data/wiki_film_feature_objects.pickle'
    outputfile = '../../data/wiki_film_feature_objects.json'

    # include_features=['gross_adjusted','budget_adjusted','director','starring']
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    inputfile = '../dev_data/feature_objects_for_first_50_post_2000_films.pickle'
    outputfile = '../dev_data/feature_objects_for_first_50_post_2000_films.json'

else:
    raise Exception('Unknown system. I do not know how to assign file names')

def main():

    print 'Converting from pickle file',inputfile,'\nto json file',outputfile

    with open(inputfile,'r') as ifile:
        ftrs = pickle.load(ifile)
    print len(ftrs),'feature objects loaded'

    with open(outputfile,'w') as ofile:
        for ftr in ftrs:
            ofile.write(ftr.as_json())
            ofile.write('\n')

    print 'Done'

if  __name__ =='__main__':
    main()
