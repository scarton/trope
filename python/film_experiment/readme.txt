This is for scripts that have to to with every aspect of this film regression experiment, where
I see if I can do a better job predicting film grosses and film critical ratings using trope
features in addition to just conventional features like film budgets.

There are also some R scripts that are part of this in the R part of the git repo