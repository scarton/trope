__author__ = 'Sam'
import pandas
import json
import numpy

class WikiFilmFeatures():

    def __init__(self,df=pandas.DataFrame,index=None,jsons=None, full=False):

        if not df.empty and index != None:
            self.tvt_id = df.tvt_id[index]
            self.wiki_id = df.wiki_id[index]
            self.title = df.title[index]
            self.file_name = df.file_name[index]
            self.release_year = df.year[index]
        elif jsons is not None:
            json_dict =json.loads(jsons)
            self.tvt_id=json_dict['tvt_id']
            self.wiki_id=json_dict['self.wiki_id']
            self.title=json_dict['self.title']
            self.file_name=json_dict['self.file_name']
            self.release_year=json_dict['self.release_year']
            self.features=json_dict['features']
            self.tropes=json_dict['tropes']
        else:
            self.tvt_id = None
            self.wiki_id = None
            self.title = None
            self.file_name = None
            self.release_year = None


        if not full:
            self.extra = {} #For stuff we want to save but don't want to treat as a feature
            self.extra['budget'] = None
            self.extra['gross'] = None


            self.features = {}
            self.features['producer'] = None
            self.features['director'] = None
            self.features['writer'] = None
            self.features['year'] = self.release_year
            self.features['budget_adjusted'] = None
            self.features['gross_adjusted'] = None
            self.features['starring'] = None
            self.features['genre'] = None
            self.features['release_month'] = None
            self.features['runtime'] = None

            self.tropes = []

    def __str__(self):
        mystr= 'DB TVTropes ID: '+repr(self.tvt_id) + \
        '\nDB Wikipedia ID: '+repr(self.wiki_id) + \
        '\nDB Title: '+self.encode_if_not_none(self.title) + \
        '\nDB File name: '+repr(self.file_name) + \
        '\nDB Release year: '+repr(self.release_year) + \
        '\nFeatures:'

        for key in self.features:
            mystr += '\n\t'+key+': ' + repr(self.features[key])

        mystr += '\nTropes: '
        mystr += repr(sorted(self.tropes))

        return mystr

    def encode_if_not_none(self,uni):
        if uni != None:
            return uni.encode('utf8')
        else:
            return uni

    def as_json(self):
        json_dict = {}
        json_dict['tvt_id'] = self.tvt_id
        json_dict['wiki_id'] = self.wiki_id
        json_dict['title'] = self.title
        json_dict['file_name'] = self.file_name
        json_dict['release_year'] = self.release_year
        json_dict['features']=self.features
        json_dict['tropes']=self.tropes
        json_dict = self.convert_from_numpy(json_dict)
        return json.dumps(json_dict)

    def convert_from_numpy(self,obj):
        if type(obj) == dict:
            new_obj = {}
            for key in obj:
                new_obj[self.convert_from_numpy(key)] = self.convert_from_numpy(obj[key])
            obj = new_obj

        elif type (obj) == list:
            for i,val in enumerate(obj):
                obj[i] = self.convert_from_numpy(val)
        elif type (obj) == tuple:
            new_obj = list(obj)
            for i,val in enumerate(obj):
                new_obj[i] = self.convert_from_numpy(val)
            obj = tuple(new_obj)
        elif hasattr(obj,'item'):
            obj = obj.item()

        return obj


