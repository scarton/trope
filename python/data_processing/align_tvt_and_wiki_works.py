__author__ = 'Sam'
import platform
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import text
import json
import re

import sys
sys.path.append('../..')
from python.db_entity.entity import *

import pprint

'''
Script for loading TVTropes.org objects into database

It fills out the following tables:
 * tvt_work
 * trope
 * tvt_work_trope_link

It may eventually fill out tables pertaining to TVTropes.org categories, but
that is for later
'''


if platform.system() == 'Linux' and platform.node() == 'gauss': #for deployment on gauss
    tvt_file_dir = '../../data/tvtropes_scraped/web_pages/'
    wiki_xml_dump = '../../data/enwiki_2015-10-02_dump/enwiki-20151002-pages-articles.xml.bz2'
    server = 'gauss'
    debug_output = False
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    tvt_file_file = '../dev_data/tvtropes_pages_sample/'
    wiki_xml_dump = '../dev_data/enwiki_film_sample.xml.bz'
    server = 'local'
    debug_output = True

else:
    raise Exception('Unknown system. I do not know how to assign file names')


def main():

    wiki_title_map = {} # Maps cleaned titles to wiki article IDs
    tvt_title_map = {} # Maps cleaned titles to TVTropes article IDs
    links = []

    print 'Aligning Wikipedia and TVTropes works by title and date (when possible)'
    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    # Get all wiki_works out of the db
    wiki_works = get_wiki_works_from_db(engine)
    print len(wiki_works),'Wikipedia works loaded from DB'


    # Get all tvt_works out of the db
    tvt_works = get_tvt_works_from_db(engine)
    print len(tvt_works),'TVTropes works loaded from DB'

    print 'Cleaning titles and zeroing match counts'
    # For each wiki_work:
    for wiki_work in wiki_works:
        wiki_work['tvt_name_match_count'] = 0
        wiki_work['tvt_name_date_match_count'] = 0

        # Clean its title and add an entry for it to wiki_title_map
        ctitle = clean_title(wiki_work['title'])
        if ctitle not in wiki_title_map:
            wiki_title_map[ctitle] = []
        wiki_title_map[ctitle].append(wiki_work)

    #For each tvtropes work
    for tvt_work in tvt_works:
        tvt_work['wiki_name_match_count'] = 0
        tvt_work['wiki_name_date_match_count'] = 0
        # Clean its title and add an entry for it to tvt_title_map
        ctitle = clean_title(tvt_work['title'])
        if ctitle not in tvt_title_map:
            tvt_title_map[ctitle] = []
        tvt_title_map[ctitle].append(tvt_work)

    print 'Aligning articles by title and date'
    # For each title in tvt_title_map:
    matched_wiki_works = 0
    matched_tvt_works = 0
    ambiguous_wiki_works = 0
    ambiguous_tvt_works = 0

    #For each title that appears in both title maps
    for ctitle in (tvt_title_map.viewkeys() & wiki_title_map.viewkeys()):

        tvt_works_sub = tvt_title_map[ctitle]
        wiki_works_sub = wiki_title_map[ctitle]
        if debug_output:
            print ctitle
            print '\t',len(tvt_works_sub),'tvt works'
            print '\t',len(wiki_works_sub),'wiki works'



        links_sub = []


        matched_tvt_works += len(tvt_works_sub)
        matched_wiki_works += len(wiki_works_sub)

        # For each tvt_article with that title:
        for tvt_work in tvt_works_sub:

            # For each wiki article with that title:
            for wiki_work in wiki_works_sub:

                type_match = do_types_match(tvt_work['type'],wiki_work['type'])
                date_match = (wiki_work['year'] >= tvt_work['min_year'] and wiki_work['year'] <= tvt_work['max_year'])
                ambiguous = (wiki_work['year'] is None or tvt_work['min_year'] is None)

                #We only allow, as candidates, links where the types match and the dates either match or are missing on at least one side
                if type_match and (ambiguous or date_match):
                    tvt_work['wiki_name_match_count'] += 1
                    wiki_work['tvt_name_match_count'] += 1

                    link = WikiTVTLink(wiki_id=wiki_work['wiki_id'],
                                       tvt_id=tvt_work['tvt_id'],
                                        name_match=True,
                                        name_date_match=date_match,
                                        unique_link=False,
                                        wiki_work = wiki_work,
                                        tvt_work = tvt_work)

                    if date_match:
                        tvt_work['wiki_name_date_match_count'] += 1
                        wiki_work['tvt_name_date_match_count'] += 1


                    links_sub.append(link)

        # For each link, if it is the only link or the only strong link among one or more weak links, between its two endpoints, save it
        # The second part of this is a bit liberal--you could have an error if one of the ambiguous links was also a date match.
        # In practice, I think doing this helps recall much more than it hurts precision.
        unique_links = 0
        for link in links_sub:
            if (link['wiki_work']['tvt_name_match_count'] == 1 and link['tvt_work']['wiki_name_match_count'] == 1) \
                    or \
                    (link['name_date_match'] and link['wiki_work']['tvt_name_date_match_count'] == 1 and link['tvt_work']['wiki_name_date_match_count'] == 1):
                link['unique_link'] = True
                links.append(link)
                unique_links += 1
        print '\t',unique_links,'unique links found'
        ambiguous_wiki_works += (len(wiki_works_sub) - unique_links)
        ambiguous_tvt_works += (len(tvt_works_sub) - unique_links)


    unmatched_wiki_works = len(wiki_works) - matched_wiki_works
    unmatched_tvt_works = len(tvt_works) - matched_tvt_works

    print 'Of',len(wiki_works),'Wikipedia works in database,',matched_wiki_works,'could be matched by title to at least one TVTropes work, and',unmatched_wiki_works,'could not.'
    print 'Of',matched_wiki_works,'Wikipedia works matched by name to TVTropes works,',ambiguous_wiki_works,'had to be dropped because they could not be unambiguously matched to a single TVTropes work'

    print 'Of',len(tvt_works),'TVTropes works in database,',matched_tvt_works,'could be matched by title to at least one Wikipedia work, and',unmatched_tvt_works,'could not.'
    print 'Of',matched_tvt_works,'TVTropes works matched by name to Wikipedia works,',ambiguous_tvt_works,'had to be dropped because they could not be unambiguously matched to a single Wikipedia work'

    print len(links),'unique links found between Wikipedia and TVTropes works'

    print 'Writing links into DB'
    #For each link in links:
    for link in links:
        # Add link to DB
        write_wiki_tvt_link_to_db(link,engine)

    print 'Writing updated wiki works into DB'
    #For each wiki work, update it with link information
    for work in wiki_works:
        replace_wiki_work_into_db(work,engine)

    print 'Writing updated tvt works into DB'
    #For each tvt work, update it with link information
    for work in tvt_works:
        replace_tvt_work_into_db(work,engine)

    print 'Updating trope entries to reflect results of alignment process'
    update_trope_matched_counts(engine)


    print 'Done!'


def do_types_match(tvt_type,wiki_type):
    if tvt_type == wiki_type:
        return True
    elif tvt_type == 'literature' and wiki_type == 'book':
        return True
    else:
        return False

def update_trope_matched_counts(engine):
    query_1 = 'update trope set matched_tvt_work_count = 0'

    query_2 = '''
    update trope, (
        select ttl.trope_tvt_id, count(*) as matched_count from wiki_tvt_work_link as wtl
        join tvt_work_trope_link as ttl
        on wtl.tvt_id= ttl.work_tvt_id
        group by ttl.trope_tvt_id
    ) as matched_counts
    set trope.matched_tvt_work_count = matched_counts.matched_count
    where trope.tvt_id = matched_counts.trope_tvt_id'''

    engine.execute(query_1)
    engine.execute(query_2)
def replace_wiki_work_into_db(work,engine):
    query = text('''
        replace into wiki_work values (
        :wiki_id,
        :title,
        :type,
        :year,
        :file_name,
        :web_url,
        :tvt_name_match_count,
        :tvt_name_date_match_count
        )
    ''')
    engine.execute(query,**work)

def replace_tvt_work_into_db(work, engine):
    query = text('''
    replace into tvt_work values (
      :tvt_id,
      :title,
      :type,
      :redirect,
      :redirect_type,
      :min_year,
      :max_year,
      :file_name,
      :web_url,
      :trope_count,
      :wiki_name_match_count,
      :wiki_name_date_match_count
    )
    ''')
    engine.execute(query,**work)


def write_wiki_tvt_link_to_db(link, engine):
    query = text('''
    replace into wiki_tvt_work_link (
    wiki_id,
    tvt_id,
    name_match,
    name_date_match,
    unique_link
    )
     values (
    :wiki_id,
    :tvt_id,
    :name_match,
    :name_date_match,
    :unique_link
    )
    ''')

    # if debug: print query
    engine.execute(query,**link)

def clean_title(title):
    '''
    Lower case, remove any characters inside parentheses, remove non-alphanumeric characters, strip, normalize spacing,
    :param title:
    :return:
    '''
    cleaned = title.lower()
    cleaned = re.sub('\(.*\)','',cleaned)
    cleaned = re.sub('[^a-zA-Z0-9]+','',cleaned)

    cleaned = cleaned.strip()
    cleaned = re.sub('[\s]+',' ',cleaned)
    return cleaned


def get_wiki_works_from_db(engine):
    works = []
    query = text('select * from wiki_work')
    result = engine.execute(query)
    for row in result:
        work = WikiWork(row)
        works.append(work)
    return works

def get_tvt_works_from_db(engine):
    works = []
    query = text('select * from tvt_work')
    result = engine.execute(query)
    for row in result:
        work = TVTWork(row)
        works.append(work)
    return works


if __name__ == '__main__':
    main()