__author__ = 'Sam'
import platform
import os
import re
import urllib
from bs4 import BeautifulSoup
import requests
from sqlalchemy import create_engine
import json
import sys
import traceback
from sqlalchemy.sql.expression import text

sys.path.append('../..')
from python.db_entity.entity import *

from python import trope_util as tu
import cProfile



'''
Script for loading TVTropes.org objects into database

It fills out the following tables:
 * tvt_work
 * trope
 * tvt_work_trope_link

It may eventually fill out tables pertaining to TVTropes.org categories, but
that is for later
'''


if platform.system() == 'Linux' and platform.node() == 'gauss':
    tvt_file_dir = '../../../data/tvtropes_scraped/web_pages/' #for deployment on gauss
    server = 'gauss'
    debug_output = True
    download_missing = False
    break_early =False
    max_level = 2
    extract_tropes = True
    extract_categories = True


elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    tvt_file_dir = '../../dev_data/tvtropes_film_sample/' #for local testing
    server = 'local'
    debug_output = True
    download_missing = False
    break_early = False
    max_level = 1
    extract_tropes = True
    extract_categories = False
else:
    raise Exception('Unknown system. I do not know how to assign file names')



#File name pattern for TVTropes articles about films or books
allowed_work_types = ['film','literature']
work_type_pattern = '('+'|'.join(['(?:'+str(x)+')' for x in allowed_work_types])+')'
tvt_article_url_pattern = "http://tvtropes\\.org/pmwiki/pmwiki\\.php/"+work_type_pattern+"/(.+)"

#Some globals it will be easier not to have to pass around as arguments
tvt_works = {} #keys: article IDs, values: work objects
tropes = {}
trope_url_map = {}
work_trope_links = set() #A set of (work_id,trope_id) tuples

categories = {}
category_url_level_map = {} # key: category page url; value: (category object, numeric level indicating what level of recursion it was discovered at)
work_category_links = set()
trope_category_links = set()
category_category_links = set()

file_count = 0
work_count = 0
film_count = 0
literature_count = 0
trope_count = 0
first_time = tu.current_milli_time()
last_time = first_time

def main():
    print 'Loading  TVTropes items into the DB from', tvt_file_dir

    if extract_tropes: print "Extracting tropes"
    else: print "Not extracting tropes"
    if extract_categories: print "Extracting categories"
    else: print "Not extracting categories"

    global file_count
    global work_count
    global film_count
    global literature_count
    global trope_count
    global first_time
    global last_time

    files = os.listdir(tvt_file_dir)
    interval = len(files)/100
    print len(files),'files found in directory. Looking through all of those that correspond to films, grabbing any tropes we find along the way'


    #For each tvttropes file that represents a film:
    for fn in os.listdir(tvt_file_dir):
        if break_early and len(tvt_works) >= 1:
            print 'Breaking after first tvt work for debugging purposes'
            break
        try:

            url = urllib.unquote(fn)
            match = re.match(tvt_article_url_pattern,url, flags = re.IGNORECASE)
            if match:
                work_type = match.group(1).lower()
                if debug_output: print "[",work_type,"]",url

                #Extract basic information about the film/work into a tvt_work object, including whether it's a redirect and if so, to what kind of media.
                tvt_work = extract_tvt_work_from_file(tvt_file_dir,fn,work_type)
                file_count += 1

                if tvt_work:
                    work_count += 1
                    if work_type == 'film':
                        film_count += 1
                    elif work_type == 'literature':
                        literature_count += 1
                    else:
                        raise Exception ('Unknown work type: '+fn)

                    #Add the work to tvt_works
                    tvt_works[tvt_work['tvt_id']] = tvt_work

                    if extract_tropes:
                        #For each trope linked to from the article:
                        for trope_url in tvt_work['trope_urls']:

                            trope = extract_trope_from_file(tvt_file_dir,trope_url)
                            if trope:
                                trope_count += 1
                                file_count += 1

                                # Add the trope to tropes
                                trope_url_map[trope_url] = trope['tvt_id']
                                tropes[trope['tvt_id']] = trope

                                # Add a link between the two to work_trope_links
                                work_trope_links.add((tvt_work['tvt_id'],trope['tvt_id']))
                    del tvt_work['trope_urls']


                    if extract_categories:
                        #for each category linked to from the article:
                        for category_url in tvt_work['category_urls']:
                            #Recursively get information about the category and all its parents and all their parents and so on
                            category = extract_category_from_file_recursive(tvt_file_dir,category_url,prefix='\t')
                            if category:
                                work_category_links.add((tvt_work['tvt_id'],category['tvt_id']))
                    del tvt_work['category_urls']

        except KeyboardInterrupt:
            sys.exit(-1)
        except:
            sys.stderr.write('Error occurred while trying to process '+fn+'\n')
            traceback.print_exc()
            continue
        finally:
            if work_count > 0 and work_count % 100 == 0:
                current_time = tu.current_milli_time()
                print '100 works found in ',(current_time-last_time)/1000,'seconds.'
                print (current_time-first_time)/1000,' seconds elapsed so far'
                print 'Current counts:'
                print_counts('\t')
                last_time = current_time

    print 'Final counts:'
    print_counts()

    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    print 'Writing stuff to DB'
    #For each work in tvt_works, add it to DB
    for tvt_id in tvt_works:
        tvt_work = tvt_works[tvt_id]
        try:
            write_tvt_work_to_db(tvt_work, engine)
        except:
            raise


    #For each trope in tvt_works, add it to DB
    for tvt_id in tropes:
        trope = tropes[tvt_id]
        try:
            write_trope_to_db(trope, engine)
        except:
            raise

    #For each link in work_trope_links, add it to DB

    for link in work_trope_links:
        try:
            write_work_trope_link_to_db(link, engine)
        except:
            raise

    #For each discovered category, add it toDB
    for tvt_id in categories:
        category = categories[tvt_id]
        try:
            write_category_to_db(category,engine)
        except:
            raise

    #For each discovered category-category link, add it to DB
    for link in category_category_links:
        try:
            write_category_category_link_to_db(link, engine)
        except:
            raise

    #For each discovered work-category link, add it to DB
    for link in work_category_links:
        try:
            write_work_category_link_to_db(link, engine)
        except:
            raise

    #For each discovered trope-category link, add it to DB
    for link in trope_category_links:
        try:
            write_trope_category_link_to_db(link, engine)
        except:
            raise

    print 'Done!'


def print_counts(prefix=''):
    global file_count
    global work_count
    global film_count
    global literature_count
    global trope_count
    global first_time
    global last_time

    print prefix,file_count,'Files scanned'
    print prefix,len(tvt_works),'TVTropes works discovered'
    print prefix,film_count,'TVTropes films discovered'
    print prefix,literature_count,'TVTropes works of literature discovered'
    print prefix,len(tropes),'TVTropes tropes discovered'
    print prefix,len(work_trope_links),'work-trope links discovered.'

    print prefix,len(categories),'TVTropes categories discovered.'
    print prefix,len(category_category_links),'category-category links discovered.'
    print prefix,len(work_category_links),'work-category links discovered.'
    print prefix,len(trope_category_links),'trope-category links discovered.'


def write_work_category_link_to_db(link,engine):
    query = text('''
    replace into tvt_work_category_link (
    work_tvt_id,
    category_tvt_id
    )
    values(
      :work_tvt_id,
      :category_tvt_id
    )
    ''')
    # if debug: print query
    engine.execute(query,work_tvt_id=link[0],category_tvt_id=link[1])
    
def write_trope_category_link_to_db(link,engine):
    query = text('''
    replace into tvt_trope_category_link (
    trope_tvt_id,
    category_tvt_id
    )
    values(
      :trope_tvt_id,
      :category_tvt_id
    )
    ''')
    # if debug: print query
    engine.execute(query,trope_tvt_id=link[0],category_tvt_id=link[1])


def write_category_category_link_to_db(link,engine):
    query = text('''
    replace into tvt_category_category_link (
    child_tvt_id,
    parent_tvt_id
    )
    values(
      :child_tvt_id,
      :parent_tvt_id
    )
    ''')
    # if debug: print query
    engine.execute(query,child_tvt_id=link[0],parent_tvt_id=link[1])

def write_category_to_db(category,engine):
    query = text('''
    replace into tvt_category values(
    :tvt_id,
    :title,
    :file_name,
    :web_url
    )
    ''')
    engine.execute(query,**category)




def write_trope_to_db(trope, engine):
    query = text('''
    replace into trope values (
      :tvt_id,
      :title,
      :file_name,
      :web_url,
      :tvt_work_count,
      :matched_tvt_work_count
    )
    ''')

    # if debug: print query
    engine.execute(query,**trope)
    # engine.execute(query,
    #                tvt_id=trope['tvt_id'],
    #                title=trope['title'],
    #                file_name=trope['file_name'],
    #                web_url=trope['web_url'],
    #             tvt_work_count= trope['tvt_work_count'])
def write_work_trope_link_to_db(link,engine):
    query = text('''
    replace into tvt_work_trope_link (
    work_tvt_id,
    trope_tvt_id
    )
    values(
      :work_tvt_id,
      :trope_tvt_id
    )
    ''')

    # if debug: print query
    engine.execute(query,work_tvt_id=link[0],trope_tvt_id=link[1])

def write_tvt_work_to_db(work, engine):
    query = text('''
    replace into tvt_work values (
      :tvt_id,
      :title,
      :type,
      :redirect,
      :redirect_type,
      :min_year,
      :max_year,
      :file_name,
      :web_url,
      :trope_count,
      :wiki_name_match_count,
      :wiki_name_date_match_count
    )
    ''')
    engine.execute(query,**work)

def sql_str(integer):
    if integer is not None:
        return str(integer)
    else:
        return 'NULL'

def extract_category_urls_from_page(doc):
    urls = []
    wiki_walk_div = doc.find('div',attrs={'class':'wiki-walk'})
    walk_rows = wiki_walk_div.find_all('div',attrs={'class':'walk-row'})
    for row in walk_rows:
        spans = row.find_all('span')
        if len(spans) > 1:
            center_span = spans[1]
            center_link = center_span.find('a')
            if center_link:
                url = tu.append_tvt_prefix(center_link['href'])

            # Only return pages that are in the Main pagespace
            if tu.is_main_page(url):
                urls.append(url)
    return urls

def extract_category_from_file_recursive(dir,url,prefix = '',level =0):



    # global categories
    # global category_url_map
    # global category_category_links
    if debug_output: print prefix,'[category]',url
    if url in category_url_level_map and level >= category_url_level_map[url][1]:
        category =  category_url_level_map[url][0]
        if debug_output: print prefix,'Category cache hit'
    else:
        if debug_output and url in category_url_level_map:
            print prefix,'Category cache hit, but this one is shallower'

        filename = urllib.quote(url,safe = '')
        if download_missing and not os.path.exists(dir+filename) and url.startswith('http://tvtropes.org/pmwiki/pmwiki.php/Main'):
            print prefix,'Could not find category:',url,'. Downloading it from interwebs.'
            response = requests.get(url)
            with open(dir+filename,'w') as f:
                f.write(response.text.encode('utf-8'))

        try:
            file = open(dir+filename)
            category = Category()
            category['file_name'] = filename
            category['web_url'] = url

            doc = BeautifulSoup(file,'html.parser')
            category['tvt_id'] = extract_tvt_article_id(doc)
            #Trope title
            category['title'] = extract_tvt_title(doc)

            category_url_level_map[url] = category,level
            categories[category['tvt_id']] = category

            #Don't go more than three steps up the category heirarchy from any given trope or work page
            if level < max_level:
                parent_urls = extract_category_urls_from_page(doc)
                for parent_url in parent_urls:
                    parent = extract_category_from_file_recursive(dir,parent_url,prefix = prefix +'\t',level=level+1)
                    if parent:
                        category_category_links.add((category['tvt_id'],parent['tvt_id']))
            else:
                if debug_output: print prefix,'Stopping'
            file.close()
        except KeyboardInterrupt:
            sys.exit(-1)
        except Exception as ex:
            traceback.print_exc()
            return None


    return category

def extract_tvt_title(doc):
    title_tag = doc.find('div',attrs={"class" : "article_title"})
    # title_string = unicode.encode(title_tag.get_text().strip(),'utf-8')
    title_string = title_tag.get_text().strip()

    return title_string

def extract_trope_from_file(dir,trope_url,debug = debug_output):
    # global trope_category_links
    trope = None
    try:
        if debug_output: print '\t','[Trope]',trope_url
        filename = urllib.quote(trope_url,safe='')

        trope_present = True
        if not os.path.exists(tvt_file_dir+filename):
            trope_present = False
            print '\tTrope file missing'
            if download_missing:
                print '\tDownloading trope from',trope_url
                response = requests.get(trope_url)
                with open(tvt_file_dir+filename,'w') as f:
                    f.write(response.text.encode('utf-8'))
                # print '\t\t\t...done.'
                trope_present = True


        if trope_present:
            if trope_url in trope_url_map:
                print '\tTrope cache hit'
                trope_id = trope_url_map[trope_url]
                trope = tropes[trope_id]
            #Otherwise:
            else:
                # Open up and extract basic information about the trope into a trope object
                file = open(dir+filename)
                trope = Trope()
                trope['file_name'] = filename
                trope['web_url'] = urllib.unquote(filename)
                trope['tvt_work_count'] = 0
                trope['matched_tvt_work_count']=0

                # if trope['web_url'] == 'http://tvtropes.org/pmwiki/pmwiki.php/Main/AdvertOverloadedFuture':
                #     pass

                doc = BeautifulSoup(file,'html.parser')

                #Trope ID
                trope['tvt_id'] = extract_tvt_article_id(doc)

                #Trope title

                trope['title'] = extract_tvt_title(doc)

                if debug: print '\t\tTrope ID:',trope['tvt_id']
                if debug: print '\t\tTrope title:',trope['title']

                if extract_categories:
                    category_urls = extract_category_urls_from_page(doc)
                    #for each category linked to from the trope article:
                    for category_url in category_urls:
                        category = extract_category_from_file_recursive(dir,category_url,prefix='\t\t')
                        if category:
                            trope_category_links.add((trope['tvt_id'],category['tvt_id']))

                file.close()
    except:
        traceback.print_exc()
    return trope

def extract_tvt_article_id(doc):
    id_tag = doc.find('input',attrs={"id" : "article_id"})
    id = int(id_tag['value'])
    return id

date_range_work_pattern = work_type_pattern = '(?:'+'|'.join(['(?:'+str(x)+'s?)' for x in allowed_work_types])+')'
date_range_pattern = re.compile("(?:http://tvtropes.org)?/pmwiki/pmwiki.php/Main/"+date_range_work_pattern+"Of([0-9]{4})([0-9]{4})",flags=re.IGNORECASE)
date_range_pattern_2 = re.compile("(?:http://tvtropes.org)?/pmwiki/pmwiki.php/Main/"+date_range_work_pattern+"OfThe([0-9]{4})s",flags=re.IGNORECASE)

main_page_pattern = '(?:http://tvtropes.org)?/pmwiki/pmwiki.php/Main/.+'
tvt_article_title_pattern = "([A-Za-z ]+): (.+)"

def extract_tvt_work_from_file(dir,filename,url_work_type):
    '''
    Extract information about a TVTropes work article from its downloaded HTML file
    :param dir:
    :param filename:
    :return:
    '''

    url = urllib.unquote(filename)
    file = open(dir+filename)
    tvt_work = TVTWork()
    tvt_work['type']=url_work_type.lower()
    tvt_work['file_name'] = filename
    tvt_work['web_url'] = url
    tvt_work['trope_count'] = 0
    tvt_work['wiki_name_match_count'] = 0
    tvt_work['wiki_name_date_match_count'] = 0

    doc = BeautifulSoup(file, 'html.parser')

    # Article ID

    tvt_work['tvt_id'] = extract_tvt_article_id(doc)
    # if debug_output: print '\tArticle tvt_id:',tvt_work['tvt_id']

    # Article title and type
    # Note that the <og:meta> tags are not reliable. See Rosencrantz and Guildenstern for an example
    title_string = extract_tvt_title(doc)
    try:
        match = re.search(tvt_article_title_pattern,title_string)
        title_work_type = match.group(1).lower()
        #If the page is a redirect, then check if it's real type is one of our allowed types. If not, include it with
        #an indication that it is a redirect. If not, throw it away.
        if title_work_type != url_work_type:
            if (title_work_type in allowed_work_types):
                return None
                if debug_output: print '\t',url_work_type,url,'was found to be a redirect to a',title_work_type,'page which we will or have already extracted elsewhere. Therefore, discarding.'
            else:
                if debug_output: print '\t',url_work_type,url,'was found to be a redirect to a',title_work_type,'page. Keeping it because the type it redirects to is not one we are otherwise getting.'
                tvt_work['redirect'] = True
                tvt_work['redirect_type'] = title_work_type
        else:
            tvt_work['redirect'] = False
            tvt_work['redirect_type'] = None
    except Exception as ex:
        sys.stderr.write('No type found in '+filename+'\n')
        traceback.print_exc()
        raise ex

    title = match.group(2)
    tvt_work['title'] = title

    # if debug_output: print '\tArticle type:',title_work_type
    # if debug_output: print '\tArticle title:',title

    # Article trope urls
    tvt_work['trope_urls'] = []
    uls = doc.find('div',attrs={"id" : "wikitext"}).find_all('ul',recursive=False)
    trope_ul = longest(uls)
    trope_urls = extract_twikilink_urls_from_ul(trope_ul,main_page_pattern)
    if len(trope_urls) > 3:
        tvt_work['trope_urls'].extend(trope_urls)
    else:
        sys.stderr.write('WARNING: Could only find '+str(len(trope_urls)) + ' tropes for '+url_work_type+' '+title+'. Unacceptably high risk of invalid page, so discarding.\n')
        return None


    #Article category urls
    tvt_work['category_urls'] = extract_category_urls_from_page(doc)


    # Year range
    # Basically, look for a trope category that indicates a year range for this work
    # Later I'll extend this code into just grabbing every category this trope is a part of

    tvt_work['min_year'] = None
    tvt_work['max_year'] = None
    for category_url in tvt_work['category_urls']:
        # print category_url
        match = re.match(date_range_pattern,category_url)
        if match:
            tvt_work['min_year'] = int(match.group(1))
            tvt_work['max_year'] = int(match.group(2))

        match = re.match(date_range_pattern_2,category_url)
        if match:
            tvt_work['min_year'] = int(match.group(1))
            tvt_work['max_year'] = tvt_work['min_year'] + 9

    file.close()
    return tvt_work

def extract_twikilink_urls_from_ul(ul,pattern=None):
    urls = []
    lis = ul.find_all('li',recursive=False)
    for li in lis:
        try:
            url = li.find('a',{'class':'twikilink'})['href']

            if pattern is None or re.match(pattern,url):
                urls.append(url)
        except:
            traceback.print_exc()
    return urls



def longest(some_list):
    longest = some_list[0]
    if len(some_list) > 1:
        for i in range(1,len(some_list)):
            if len(some_list[i]) > len(longest):
                longest = some_list[i]
    return longest

if __name__ == '__main__':

    # cProfile.run('main()')
    main()