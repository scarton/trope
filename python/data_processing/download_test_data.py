__author__ = 'Sam'
import os
import urllib
import requests
import bz2
import xml.etree.ElementTree as et



tvt_urls_file = '../../dev_data/tvtropes_film_sample_urls.txt'
tvt_sample_dir = '../../dev_data/tvtropes_film_sample/'

wiki_titles_file = '../../dev_data/wiki_film_sample_url_titles.txt'
wiki_sample_file = '../../dev_data/wiki_film_sample.xml.bz2'

def main():
    print 'Downloading sample data from Wikipedia.org and TVTropes.org'

    print 'Downloading TVTropes data into',tvt_sample_dir
    with open(tvt_urls_file) as f:
        tvt_urls = f.readlines()
    if not os.path.exists(tvt_sample_dir):
        os.makedirs(tvt_sample_dir)

    for url in tvt_urls:
        url = url.strip()
        if '%' in url:
            filename = url
            url = urllib.unquote(url)
        else:
            filename = urllib.quote(url,safe='')

        print url,'to',filename

        if not os.path.exists(tvt_sample_dir+filename):
            response = requests.get(url)
            with open(tvt_sample_dir+filename,'w') as f:
                f.write(response.text.encode('utf-8'))
            print '\t...downloaded.'

        else:
            print '\tAlready downloaded'


    print 'Getting Wikipedia data and adding it to',wiki_sample_file
    with open(wiki_titles_file) as f:
        wiki_titles = f.readlines()
    first_page = True

    if not os.path.exists(wiki_sample_file):
        wiki_file = bz2.BZ2File(wiki_sample_file,'w')
        for title in wiki_titles:
            title = title.strip()
            url = 'https://en.wikipedia.org/wiki/Special:Export/'+title
            print url
            response = requests.get(url)
            print '\t',len(response.text),'characters'

            if first_page:
                out_root = et.fromstring(response.text.encode('utf-8'))
                first_page = False
            else:
                root = et.fromstring(response.text.encode('utf-8'))
                page = root.find("{http://www.mediawiki.org/xml/export-0.10/}page")
                out_root._children.append(page)

        wiki_file.write(et.tostring(out_root,encoding='utf-8',method='xml'))
        wiki_file.close()
        print '\t...downloaded.'
    else:
        print '\tFile already exists'

    print 'Done'

if __name__ == '__main__':
    main()