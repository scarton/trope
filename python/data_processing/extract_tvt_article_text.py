__author__ = 'Sam'

import platform
import json
from sqlalchemy import create_engine
import sys
from bs4 import BeautifulSoup
sys.path.append('../..')
from python.db_entity.entity import *
from python.trope_util import *

if platform.system() == 'Linux' and platform.node() == 'gauss':
    tvt_file_dir = '../../../data/tvtropes_scraped/web_pages/' #for deployment on gauss
    server = 'gauss'


elif platform.system() == 'Windows' and platform.node() == 'Gondolin':
    tvt_file_dir = '../../dev_data/tvtropes_film_sample/' #for local testing
    server = 'local'

else:
    raise Exception('Unknown system. I do not know how to assign file names')

def test():
    #List of tropes and what their descriptions should start and end with
    tests = {
    7677:('I Have the High Ground, with a body of water substituted for the slender pole. As ',"Compare with Walk, Don't Swim, which is about walking under water."),
    2394:('The act of taking a single (often minor) action or trait of a character within a work','''The opposite to this trope is Character Development, naturally.
Here's a list of cases of Flanderization:'''), #Has quote at beginning
    1832:('1. A finale to a movie, a TV series or an episode of that TV series, a video game,','''Compare Downer Beginning.
Naturally, every single example is a spoiler.'''), #Has quote
    396:('Whenever the hero really starts beating the snot out of the bad guys,','Compare Punctuated Pounding. Even villains can get in on the action thanks to Moral Myopia.'), #Has quote
    26356:("An intentional exploitation of the camera's 2-D vision. Place an object closer to the camera,",'Compare Depth Deception (when this happens in-story for comedic effect), Perspective Magic, Vertigo Effect.'),
    7166:('In a God Guise or Time Travel scenario, a modern person','Nothing to do with an actual Boom Stick.'), #Has quote
    4333:('Somehow, a disproportionate number of fictional fights break out','you so gleefully smash in your work.'),#has quote
    2283:('Sometimes, when a role in a film or TV show requires a character to speak in a certain language, and there are no suitable',"It's either this or As Long as It Sounds Foreign."),
    28773:('Audience expectations are that The Protagonist has Plot Armor.',"""multiple protagonists.
You Have Been Warned."""),
    2351:("A major character doesn't just get killed straight away. Oh, no. ",'''doesn't notice.
This is a Death Trope. Expect unmarked spoilers ahead.'''), #Has quote and list inside it
    }

    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    for id in tests:
        print '***************************************************************************************'
        text,trope = extract_trope_article_text(id,engine,tvt_file_dir,return_trope =True)
        st = tests[id][0]
        en = tests[id][1]
        print id,' | ',trope['title'],' | ',trope['web_url']
        if text.startswith(st) and text.endswith(en):
            print '<Text correctly extracted>'
        else:
            print('<Text not correctly extracted.>')
            print('<Start>')
            print('<'+st+'>')
            print('<'+text[0:len(st)]+'>')
            print('<End>')
            print('<'+en+'>')
            print('<'+text[-len(en):]+'>')


def extract_trope_article_text(tvt_id, engine, file_dir, return_trope = False,ignore_lists = True, ignore_quotes =True):
    query = 'select * from trope where tvt_id='+str(tvt_id)

    row = engine.execute(query).fetchone()
    trope = Trope(row)

    file_name = row['file_name']

    file = open(file_dir+'/'+file_name)

    doc = BeautifulSoup(file,'html.parser')
    file.close()
    #Simple algorithm: grab everything between the beginning of the wikitext div and the first <hr> tag, which
    #usually denotes the beginning of the example list
    #if ignore_quotes is on, ignore anything inside a div with class "indent"
    #if ignore_lists is on, ignore anything inside a ul or ul

    wikitext_div = doc.find('div',attrs={"id" : "wikitext"})
    hr = wikitext_div.find('hr')
    str_list = []

    #Throw out stuff after the first ,hr> divider
    for after in wikitext_div.select('hr ~ *'):
        after.extract()

    #Throw out quotes
    if ignore_quotes:
        for quote in wikitext_div.find_all('div',attrs={'class':'indent'}):
            quote.extract()

    #Throw out lists
    if ignore_lists:
        for lst in wikitext_div.find_all('ul'):
            lst.extract()
        for lst in wikitext_div.find_all('ol'):
            lst.extract()

    #Throw out captions
    for caption in wikitext_div.find_all('div',attrs={'class':'acaptionright'}):
        caption.extract()

    #Get rid of subsection/list headings
    for heading in wikitext_div.find_all('h3'):
        heading.extract()

    text = wikitext_div.get_text().strip()

    if return_trope:
        return text,trope
    else:
        return text

if __name__ == '__main__':
    test()