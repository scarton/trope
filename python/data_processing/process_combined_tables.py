__author__ = 'Sam'

import pandas as pd
import re

'''
This script fixes a stupid error in how some output tables are formatted
'''

input_path = "C:/Users/Sam/Desktop/trope project/film prediction experiment/iteration 3 results/combined_unprocessed.csv"
output_path = "C:/Users/Sam/Desktop/trope project/film prediction experiment/iteration 3 results/combined_processed.csv"

patterns = [
    # '       base+X.7287..u.Took.a.Level.in.Badass..',
            ' +base\+X\.([0-9]+)\.\.u\.([A-Za-z\.]+)..',
            # '        + X.5249..u.Product.Placement..',
            ' +\+ ?X\.([0-9]+)\.\.u\.([A-Za-z\.]+)..',]


df = pd.read_csv(input_path)

repls = 0

for i in range(1,len(df.columns)):
    column = df.columns[i]
    for j,row in enumerate(df[column]):
        for pattern in patterns:
            if type(row) == str:
                match = re.match(pattern,row)
                if match:
                    id = match.group(1)
                    name = match.group(2)
                    df[df.columns[i-1]][j] = id
                    df[df.columns[i]][j] = name
                    repls += 1

print repls,'replacements made'

df.to_csv(output_path)

print 'Done'