__author__ = 'Sam'
from sqlalchemy import create_engine
import pandas
import json
import random
import pprint
import csv
import sys
sys.path.append('../..')
from python import trope_util as tu


'''
This script generates an annotation task in the form of a csv file for getting a qualitative sense of what tropes are
discoverable in the text of plot summaries.
'''

server = 'gauss'
# output_file = '../../dev_data/trope_annotation/10_tropes_each_from_5_films.csv'
# output_file = '../../dev_data/trope_annotation/5_films_each_for_10_tropes_ann.csv'
output_dir = '../../dev_data/trope_annotation'


random_seed = 120920
# random_seed = 120911 #used for Molly

num_films = 7
num_tropes = 7
include_example = True

films_then_tropes = False #if true, get n films and sample m tropes from each. Otherwise get m tropes and sample n films from each

if films_then_tropes:
    #Get all tropes for a limited sample of films
    query = '''
            select w.title as wiki_title,
            w.wiki_id as wiki_id, 
            w.web_url as wiki_url, 
            t.title as tvtropes_title, 
            t.tvt_id as tvtropes_id, 
            t.web_url as tvtropes_url, 
            tr.title as trope_title, 
            tr.tvt_id as trope_id, 
            tr.web_url as trope_url
            from 
                (select *, rand('''+str(random_seed)+''') as r
                from (wiki_tvt_work_link)
                order by r limit '''+str(num_films)+''')
            as l 
            join wiki_work as w on l.wiki_id = w.wiki_id 
            join tvt_work as t on l.tvt_id = t.tvt_id
            join tvt_work_trope_link as tl on tl.work_tvt_id = l.tvt_id
            join trope as tr on tl.trope_tvt_id = tr.tvt_id
            order by wiki_title, trope_title
            '''
else:
    #Get all films for a limited sample of tropes
    query = '''
        select w.title as wiki_title,
        w.wiki_id as wiki_id, 
        w.web_url as wiki_url, 
        t.title as tvtropes_title, 
        t.tvt_id as tvtropes_id, 
        t.web_url as tvtropes_url, 
        tr.title as trope_title, 
        tr.tvt_id as trope_id, 
        tr.web_url as trope_url
        from (select *,rand('''+str(random_seed)+''') as r from trope
        	where matched_tvt_work_count >= '''+str(num_films)+'''
            order by r limit '''+str(num_tropes)+''') as tr
        join tvt_work_trope_link as tl on tl.trope_tvt_id = tr.tvt_id
        join tvt_work as t on t.tvt_id = tl.work_tvt_id
        join wiki_tvt_work_link as l on l.tvt_id = tl.work_tvt_id
        join wiki_work as w on l.wiki_id = w.wiki_id
        order by trope_title, wiki_title;
        '''


def main():


    print 'Creating annotation task to see if a human can detect tropes in film plot summaries'
    if films_then_tropes:
        print 'Sampling ' + str(num_films) + ' films, then ' + str(num_tropes) + ' tropes from each film'
    else:
        print 'Sampling ' + str(num_tropes) + ' tropes, then ' + str(num_films) + ' films from each trope'


    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    sample_dict = {}
    if films_then_tropes:
        col = 'wiki_id'
        sample_num = num_tropes
    else:
        col = 'trope_id'
        sample_num = num_films

    print 'Query: '
    print query
    result = engine.execute(query)
    headers = result._metadata.keys
    for row in result:
        key = row[col]
        if key not in sample_dict:
            sample_dict[key] = []
        sample_dict[key].append(row)


    random.seed(random_seed)
    out_lst = []
    for key in sample_dict:
        lst = list(sample_dict[key])
        random.shuffle(lst)
        out_lst.extend(lst[:min(sample_num,len(lst))])

    # print '\n'.join(str(x[2]) + ' | ' + str(x[8]) for x in out_lst)
    pprint.pprint(out_lst)
    user_input_headers = ['found','notes','snippet_1','snippet_2','snippet_3','snippet_4','snippet_5','snippet_6']
    headers.extend(user_input_headers)

    if films_then_tropes:
        output_file_name = str(num_tropes)+'_tropes_each_from_'+str(num_films)+'_films_'+str(random_seed)+'_'+tu.current_date_string()+'.csv'
    else:
        output_file_name = str(num_films)+'_films_each_for_'+str(num_tropes)+'_tropes_'+str(random_seed)+'_'+tu.current_date_string()+'.csv'

    output_file = output_dir+'/'+output_file_name

    print 'Writing to',output_file
    ofile = open(output_file,'w')
    writer = csv.writer(ofile,quoting=csv.QUOTE_NONNUMERIC,lineterminator='\n')
    writer.writerow(headers)

    if include_example:
        example = ['Frankenstein (1931 film)',73488,'https://en.wikipedia.org/wiki/Frankenstein_(1931_film)','Frankenstein (1931)',297503,'http://tvtropes.org/pmwiki/pmwiki.php/Film/Frankenstein1931','Mad Scientist',3981,'http://tvtropes.org/pmwiki/pmwiki.php/Main/MadScientist',2,"This is an example of snippets I think are indicative of the presence of the 'Mad Scientist' trope in the original Frankenstein movie. Not that none of them is necessarily a dead ringer, but taken together they are fairly definitive.",'a young scientist','hunchback','secludes himself', 'creating life',"It's alive!.", '']
        writer.writerow(example)

    for tup in out_lst:
        out = list(tup)
        for i,val in enumerate(out):
            if type(val) == str or type(val) == unicode:
                out[i] = val.encode('utf-8')

        out.append(0)
        out.extend(['']*(len(user_input_headers)-1))
        writer.writerow(out)

    ofile.close()

    print 'Done!'




if  __name__ =='__main__':
    main()