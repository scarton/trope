__author__ = 'Sam'
'''
Make all the tables
'''
import json
from sqlalchemy import create_engine
import sys
import traceback
import fix_title_columns

import platform

fix_title_cols = True

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    server = 'gauss'
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    server = 'local'
else:
    raise Exception('Unknown system. I do not know how to assign file names')

def main():
    print 'Creating all tables on',server,'server'
    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    queries = [
    '''create table if not exists trope (
             tvt_id int primary key,
             title varchar(256), 
             file_name varchar(256), 
             web_url varchar(256), 
             tvt_work_count int,
             matched_tvt_work_count int)''',

    '''create table if not exists tvt_work (
             tvt_id int primary key,
             title varchar(256), 
             type varchar(64),
             redirect boolean, 
             redirect_type varchar(64), 
             min_year int, 
             max_year int, 
             file_name varchar(256), 
             web_url varchar(256), 
             trope_count int,
             wiki_name_match_count int,
             wiki_name_date_match_count int)''',

    #I've decided not to bother with supplements for now. Too much pain, not much gain.
    # '''create table if not exists tvt_work_supplement (tvt_id int primary key,
    #          work_tvt_id int,
    #          file_name varchar(256),
    #          web_url varchar(256))''',

    '''create table if not exists wiki_tvt_work_link (
             link_id int auto_increment primary key,
             wiki_id int, 
             tvt_id int, 
             name_match boolean, 
             name_date_match boolean, 
             unique_link boolean,
             unique key(wiki_id,tvt_id),
             key(tvt_id))''',
    '''create table if not exists wiki_work (
             wiki_id bigint primary key,
             title varchar(256), 
             type varchar(64),
             year int, 
             file_name varchar(256), 
             web_url varchar(256),
             tvt_name_match_count int,
             tvt_name_date_match_count int)''',

    '''create table if not exists tvt_work_trope_link (
             link_id int auto_increment primary key,
             work_tvt_id int,
             trope_tvt_id int,
             unique key(work_tvt_id,trope_tvt_id))''',

    '''create table if not exists tvt_category (
            tvt_id int primary key,
            title varchar(256),
            file_name varchar(256),
            web_url varchar(256))''',

    '''create table if not exists tvt_trope_category_link (
            id int auto_increment primary key,
            trope_tvt_id int,
            category_tvt_id int,
            unique key (trope_tvt_id,category_tvt_id),
            key (category_tvt_id))''',

    '''create table if not exists tvt_work_category_link (
            link_id int auto_increment primary key,
            work_tvt_id int,
            category_tvt_id int,
            key(category_tvt_id),
            unique key(work_tvt_id,category_tvt_id))''',

    '''create table if not exists tvt_category_category_link (
            link_id int auto_increment primary key,
            child_tvt_id int,
            parent_tvt_id int,
            key(child_tvt_id),
            unique key(parent_tvt_id,child_tvt_id))'''
    ]

    for query in queries:
        try:
            print query
            engine.execute(query)
        except Exception as ex:
            traceback.print_exc()

    if fix_title_cols:
        fix_title_columns.fix_title_columns()

    print 'Done'

if __name__ == '__main__':
    main()