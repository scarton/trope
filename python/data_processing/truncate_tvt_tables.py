__author__ = 'Sam'
'''
Make all the tables
'''
import json
from sqlalchemy import create_engine
import traceback
import platform

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    server = 'gauss'
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    server = 'local'
else:
    raise Exception('Unknown system. I do not know how to assign file names')

def main():
    print 'Truncating all tables on',server,'server'

    sure = raw_input('Are you sure you want to truncate all TVTROPES tables? (Y/n): ')

    if sure == 'Y':
        with open('../db.config') as c:
            config = json.load(c)['servers'][server]

        engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

        queries = [
        '''truncate table trope''',
        '''truncate table tvt_work''',
        # '''truncate table tvt_work_supplement''',
        # '''truncate table wiki_tvt_work_link''',
        # '''truncate table wiki_work''',
        '''truncate table tvt_work_trope_link''',
        '''truncate table tvt_category''',
        '''truncate table tvt_category_category_link''',
        '''truncate table tvt_work_category_link''',
        '''truncate table tvt_trope_category_link''']

        for query in queries:
            try:
                print query
                engine.execute(query)
            except Exception as ex:
                traceback.print_exc(limit=3)


        print 'Done'
    else:
        print 'Aborting. No tables truncated.'

if __name__ == '__main__':
    main()