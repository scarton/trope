__author__ = 'Sam'
import json
from sqlalchemy import create_engine
import sys
import traceback
import platform

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    server = 'gauss'
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    server = 'local'
else:
    raise Exception('Unknown system. I do not know how to assign file names')

'''Make sure title columns are able to store unicode'''

def fix_title_columns():
    print 'Fixing title columns'
    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])
    queries = [
    '''ALTER TABLE trope.trope MODIFY COLUMN title VARCHAR(256)
      CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL''',
    '''ALTER TABLE trope.tvt_category MODIFY COLUMN title VARCHAR(256)
        CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL''',
    '''ALTER TABLE trope.tvt_work MODIFY COLUMN title VARCHAR(256)
        CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL'''
    ]

    for query in queries:
        try:
            print query
            engine.execute(query)
        except Exception as ex:
            traceback.print_exc()
    print 'Done'

if __name__ == "__main__":
    fix_title_columns()
