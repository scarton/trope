__author__ = 'Sam'
import platform
import bz2
import xml
from xml.sax import make_parser
from xml.sax.handler import feature_namespaces
import re
import sys
sys.path.append('../..')
from python.db_entity.entity import *
from python import trope_util as tu
import os
import json
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import text
import traceback


'''
Script for parsing a Wikipedia dump and loading information about films into the DB.
'''
if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    wiki_xml_dump = '../../../data/enwiki_2015-12-01_dump/enwiki-20151201-pages-articles.xml.bz2'
    wiki_file_dir = '../../../data/wiki_films/'
    # wiki_title_file = '../../../data/wiki_discovered_titles.txt'
    server = 'gauss'
    page_tag = 'page'
    revision_tag = 'revision'
    text_tag = 'text'
    title_tag = 'title'
    id_tag = 'id'

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    wiki_xml_dump = '../../dev_data/wiki_film_sample.xml.bz2'
    wiki_file_dir = '../../dev_data/wiki_film_sample_files/'
    # wiki_title_file = '../../dev_data/wiki_discovered_titles.txt'
    server = 'local'
    #Some weirdness in how I downloaded test xml data from Wikipedia lead to the inclusion of a dumb
    #ns0 namespace in the sample data that is not present in the real data
    page_tag = 'ns0:page'
    revision_tag = 'ns0:revision'
    text_tag = 'ns0:text'
    title_tag = 'ns0:title'
    id_tag = 'ns0:id'
else:
    raise Exception('Unknown system. I do not know how to assign file names')


save_wiki_titles = False


def main():
    # Open up a streaming interface to the compressed XML file representing the Wikipedia dump

    print 'Beginning parse of Wikipedia dump located at',wiki_xml_dump
    print 'When a film article is encountered, its text will be written to:',wiki_file_dir

    if not os.path.exists(wiki_file_dir):
        os.makedirs(wiki_file_dir)

    wf = bz2.BZ2File(wiki_xml_dump,'r')

    parser = make_parser()
    parser.setFeature(feature_namespaces,0)
    parser.returns_unicode = True

    # if save_wiki_titles:
    #     print 'Saving titles of all discovered Wikipedia pages to',wiki_title_file
    #     title_file = open(wiki_title_file,'w')
    #     handler = WikiWorkFinder(title_file)
    # else:
    handler = WikiWorkFinder()

    parser.setContentHandler(handler)
    start_time = tu.current_milli_time()
    try:
        parser.parse(wf)
    except:
        traceback.print_exc()
    end_time = tu.current_milli_time()

    print 'Done parsing pages.'
    print handler.page_count,'pages processed total, over',(end_time-start_time)/1000,'seconds.'
    print handler.work_count,'works found total.'
    print handler.film_count,'films found total.'
    print handler.book_count,'books found total.'

    print 'Writing page metadata to database'
    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    for film in handler.wiki_works:
        write_wiki_work_to_db(film,engine)


    wf.close()

    # if save_wiki_titles:
    #     title_file.close()

    print 'Done'

    # For each XML object representing a Wikipedia article
    # If that article has an infobox, and the infobox indicates that it is a film:
        # Extract basic information about it into a wiki_film object
        # Add that object to wiki_films

    # For each element in wiki_films
        # Add that film to the database

def write_wiki_work_to_db(wiki_work,engine):
    query = text('''
    replace into wiki_work values (
    :wiki_id,
    :title,
    :type,
    :year,
    :file_name,
    :web_url,
    :tvt_name_match_count,
    :tvt_name_date_match_count
    )
    ''')
    engine.execute(query,**wiki_work)


class WikiWorkFinder(xml.sax.handler.ContentHandler):
    '''
    This is a content handler that parses a wikipedia xmldump, pulling out pages about films (for now) and
    dumping their contents to a file and metadata about them to a database.

    It maintains a bunch of state flags to track where it is and what it has found. It does some light content
     parsing of the actual wikitext, just enough to figure out if the article is a film or book and to pull out its
     release year.

    '''

    #Turns out TV films count as television, so we need to get these too
    allowed_infobox_types = ['film','television','book']
    allowed_type_pattern = '('+'|'.join('(:?'+str(x)+')' for x in allowed_infobox_types) + ')'
    infobox_pattern = re.compile('\{\{Infobox '+allowed_type_pattern,flags =re.IGNORECASE)
    # release_year_pattern = re.compile('.*(?:(?:release_date)|(?:released)).*=.*([0-9]{4}).*',flags =re.IGNORECASE)


    release_year_pattern = re.compile('.*release.*=.*([0-9]{4}).*',flags =re.IGNORECASE)

    #TODO check whether this is too specific
    #So actually, even though it appears as "Publication date", the wikitext used is release_date
    publication_year_pattern = re.compile('.*(?:(?:pub_date)|(?:release_date)).*=.*([0-9]{4}).*',flags =re.IGNORECASE)

    # How to find the release year in the infobox, depending on the media type
    infobox_year_patterns = {'film':release_year_pattern,
                             'television':release_year_pattern,
                             'book':publication_year_pattern}

    #Captures parenthesized meta-data in a title, E.g. the "2007" and "film" in "Bridge to Terabithia (2007 film)"
    title_info_pattern = re.compile('.*\((?:([0-9]{4}) )?([a-zA-Z]+)\)')
    # title_year_pattern = re.compile('.*\(([0-9]{4}) ([a-zA-Z]+\))')

    max_infobox_lines = 120 #How many lines to scan after the beginning of the infobox before we assume that we have finished with it

    lines_to_search = 30 #If we don't find the beginning of an infobox within this many lines of the beginning of the
    #wikitext, then don't bother reading further.

    def __init__(self,title_file = None):

        self.wiki_works = [] #Where we will accumulate Wikipedia film metadata

        #State variables
        self.in_page = False
        self.in_revision = False
        self.in_page_id = False
        self.in_title = False
        self.in_text = False
        self.in_infobox = False

        #A title can have three pieces of information in it, e.g
        self.title = None
        self.title_year = None
        self.title_type = None

        self.id = None

        self.text_arr = None #A byte array representing the current text
        # self.text_bytes = 0 #How many bytes total the current text has
        # self.text_cursor = 0 #How many bytes into the current text we are
        self.line = None #What line of the text we are on

        self.valid_infobox_found = False #Whether we have found the start of an infobox indicating a film or a book
        self.infobox_type = None #Type of media found via infobox definition
        self.infobox_year = None


        self.first_time =  tu.current_milli_time()
        self.last_time = self.first_time
        self.page_count = 0
        self.work_count = 0
        self.film_count = 0
        self.book_count = 0


    def startElement(self, name, attrs):
        if name == page_tag:
            self.in_page = True
        elif name == revision_tag:
            self.in_revision = True

        #Make sure we are going to get the page ID, not the revision ID
        elif name == id_tag and self.in_page and not self.in_revision:
            self.in_page_id = True
        elif name == title_tag:
            self.in_title = True
        elif name == text_tag:
            # self.text_bytes = int(attrs._attrs['bytes'])
            self.text_arr = []
            # self.text_cursor = 0
            self.line =  0

            self.in_text = True


    def reset_page(self):
        self.title = None
        self.title_year = None
        self.title_type = None

        self.id = None
        del(self.text_arr)
        self.valid_infobox_found = False
        self.infobox_year = None
        self.infobox_type = None
        self.line = None
        # self.text_cursor = None
        self.text_bytes = None

    def endElement(self,name):
        if name == page_tag:
            self.in_page = False
            self.page_count += 1
            self.process_page()
            self.reset_page()
            if self.page_count % 10000 == 0:
                current_time = tu.current_milli_time()
                print '\t10,000 pages processed in',(current_time-self.last_time)/1000,'seconds.',self.page_count,'pages processed total.',(current_time-self.first_time)/1000,'seconds elapsed total.'
                self.last_time = current_time

            # if self.film_count >= 2:
            #     raise Exception('Found 2 films. Exiting for debug purposes to test rest of code')

        elif name == revision_tag:
            self.in_revision = False
        elif name == id_tag and self.in_page and not self.in_revision:
            self.in_page_id = False
        elif name == title_tag:
            self.in_title = False
        elif name == text_tag:
            self.in_text = False

    def characters(self,content):

        if self.in_title:
            self.title = content
            match = self.title_info_pattern.match(self.title)
            if match:
                if match.group(1):
                    self.title_year = int(match.group(1))
                if match.group(2):
                    self.title_type = match.group(2).lower()

        elif self.in_page_id:
            self.id = long(content)
        elif self.in_text:

            try:
                #Don't bother copying down this line of content if the article isn't about a film
                if self.valid_infobox_found or self.line < self.lines_to_search:
                    content = content.encode('utf-8')
                    # print 'Content size:',len(content)
                    self.text_arr.append(content)

                    if not self.valid_infobox_found:
                        match = self.infobox_pattern.match(content)
                        if match:
                            self.valid_infobox_found = True
                            self.infobox_type = match.group(1).lower()
                            self.in_infobox = True
                            self.infobox_lines = 0
                    elif self.in_infobox:
                        self.infobox_lines += 1
                        if not self.infobox_year:
                            # if self.infobox_type == 'book':
                            #     print content
                            #     pass
                            infobox_year_pattern = self.infobox_year_patterns[self.infobox_type]
                            match = infobox_year_pattern.match(content)
                            if match:
                                self.infobox_year = int(match.group(1))
                                self.in_infobox = False

                        #Rather than specifically looking for the end of the infobox, just assume that it ends at some point
                        if self.infobox_lines > self.max_infobox_lines:
                            self.in_infobox = False
            except:
                sys.stderr.write('Error encountered when trying to parse page #'+str(self.page_count)+' title:'+self.title)
                traceback.print_exc()


                # self.text += content.encode('utf-8')
            self.line += 1


    def process_page(self):
        '''
        This runs when we find an ending <page> tag
        :return:
        '''
        # print 'Processing wikipedia page with following attributes:'
        try:
            # if self.id == 4194975:
            #     pass
            if self.valid_infobox_found:

                #special rule for films
                if self.title_type == 'film' and self.infobox_type == 'television':
                    type = 'film'
                else:
                    type = self.infobox_type

                if self.title_year and self.infobox_year:
                    year = self.title_year
                elif self.title_year:
                    year = self.title_year
                elif self.infobox_year:
                    year = self.infobox_year
                else:
                    year = None

                if type in self.allowed_infobox_types:

                    self.work_count += 1
                    if type == 'film':
                        self.film_count += 1
                    elif type == 'book':
                        self.book_count += 1

                    #Write film wikitext to file
                    web_title  = tu.wiki_to_web_title(self.title)
                    file_name =  '_'+web_title+ '.wiki'
                    with open(wiki_file_dir+file_name,'w') as f:
                        for char in self.text_arr:
                            f.write(char)

                    #Write film metadata to DB

                    wf = WikiWork()
                    wf['wiki_id'] = self.id
                    wf['title'] = self.title
                    wf['type'] = type
                    wf['year'] = year
                    wf['web_url'] = 'https://en.wikipedia.org/wiki/' + web_title
                    wf['file_name'] = file_name
                    wf['tvt_name_match_count'] = 0
                    wf['tvt_name_date_match_count'] = 0

                    self.wiki_works.append(wf)
        except:
            sys.stderr.write('Error encountered when trying to parse page #'+str(self.page_count)+' title:'+str(self.title))
            traceback.print_exc()

if __name__ == '__main__':
    main()