__author__ = 'Sam'

import align_tvt_and_wiki_works
import create_tables
import create_trope_annotation_task
import download_test_data
import drop_tables
import fix_title_columns
import load_tvt_items_into_db
import load_wiki_articles_into_db
import truncate_tables
import truncate_tvt_tables
import truncate_wiki_tables
import wiki_xml_parser