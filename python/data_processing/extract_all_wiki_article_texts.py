__author__ = 'Sam'
import platform
import json
from sqlalchemy import create_engine, text
import traceback
import  extract_wiki_article_text as ext
import os
import datetime

'''
Extract article text for every Wiki aticle with a corresponding TVTropes article
'''

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
	server = 'gauss'
	wiki_file_dir = '../../../data/wiki_films/'
	out_dir = '../../../data/wiki_summaries/'

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
	server = 'local'
	wiki_file_dir = '../../dev_data/wiki_film_sample_files/'
	out_dir = '../../dev_data/wiki_summaries/'

else:
	raise Exception('Unknown system. I do not know how to assign file names')

failure_fn = "summary_failures.log"
success_fn = "summary_successes.log"

def main():

	if not os.path.exists(out_dir):
		os.makedirs(out_dir)

	with open('../db.config') as c:
		config = json.load(c)['servers'][server]

	engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

	aq = '''
	alter table wiki_work add column has_summary boolean, add column summary_file_name varchar (256)
	'''
	try:
		print "Adding summary columns to wiki_work table"
		engine.execute(aq)
	except Exception as ex:
		print ex.message
		print "Couldn't add new columns, probably because they are already there."

	gq = '''select w.* from wiki_work as w
				where has_summary is null'''



	rows = engine.execute(gq).fetchall()

	print len(rows),'Wiki articles found for which article extraction needs to be performed'

	print "Beginning to extract all summaries"
	successes = []
	failures = []
	last_time = datetime.datetime.now()
	for i,row in enumerate(rows):
		summary = ext.restricted_mwpfh_extract_wiki_plot_summary_text(row['file_name'],wiki_file_dir)
		id = row['wiki_id']
		if summary:
			successes.append(row['file_name'])
			append(success_fn,row['file_name'] )
			#Write summary to file
			out_fn = row['file_name'].replace(".wiki","_summary.txt")
			found = True
			with open(out_dir+'/'+out_fn, 'w') as f:
				f.write(summary.encode('utf-8'))
		else:
			out_fn = None
			found = False
			failures.append(row['file_name'])
			append(failure_fn,row['file_name'] )

		#Record success or failure to MySQL DB
		uq = text("update wiki_work set has_summary = :fnd, summary_file_name = :out_fn where wiki_id = :id")

		engine.execute(uq,
				fnd=found,
				out_fn = out_fn,
				id=id)

		if i > 0 and i % 100 == 0:
			time_diff = datetime.datetime.now() - last_time
			print i,"articles scanned of",len(rows),". | ",len(successes), ' successes so far. |', len(failures), ' failures so far. | ', time_diff, ' elapsed last round.'
			last_time = datetime.datetime.now()

	print "Done.",len(rows),"articles scanned of",len(rows),". | ",len(successes), ' successes so far. |', len(failures), ' failures so far.'


	print "Done!"

def append(filename, line):
	with open(filename,'a') as f:
		f.write(line+'\n')


if __name__ == "__main__":
	main()