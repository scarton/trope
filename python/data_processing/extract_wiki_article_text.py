# -*- coding: utf-8 -*-
import platform
import json
from sqlalchemy import create_engine
import mwparserfromhell
import re
import sys
import traceback

sys.path.append('../..')
from python.db_entity.entity import *
from python.trope_util import *

# import mediawiki_parser
'''
This is a script with functions for extracting the text of the plot summary from the wikitext of a Wikipedia
article about a movie or book.
Its primary function, restricted_mwpfh_extract_wiki_plot_summary_text_by_id(), uses regular expressions to isolate
just the chunk of Wikitext corresponding to the plot summary, and then uses mwparserfromhell to parse the wikitext and
strip out all the wikimarkup.
'''


if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss
    server = 'gauss'
    wiki_file_dir = '../../../data/wiki_films/'

elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing
    server = 'local'
    wiki_file_dir = '../../dev_data/wiki_film_sample_files/'

else:
    raise Exception('Unknown system. I do not know how to assign file names')


def test():
    with open('../db.config') as c:
        config = json.load(c)['servers'][server]

    engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

    tests = {
        # 5313:('The film is set in the Qing Dynasty','leaps over the side of the mountain.'), #Crouching tiger, hidden dragon
        # 22216:('Three convicts, Ulysses Everett McGill (George Clooney), known as Everett',#O Brother Where Art Thou
        #        'and demands that he get her ring back.'),
        # 2509613:('From a lost expedition to a plateau in Venezuela, Paula White', #The Lost World (1925) CURRENTLY FAILING due to wikicode parsing issue
        #          'while two passersby note: "That\'s Sir John Roxton—sportsman."'),
        # 23487440:('The commercial spacecraft Nostromo is on a return trip to Earth with a seven-member crew in stasis:', #Alien BROKEN DUE TO parsing issue
        #         'she places herself and Jones into stasis for the voyage home.'),
        # 36524612:None, #Henry V (2012)
        # 473086:('In 1933, at the height of the Great Depression, New York City', #King Kong (2005)
        #         'Carl takes one last glimpse at him and says, "It wasn\'t the airplanes. It was Beauty killed the Beast."'),
        # 21188512:('Six years after the disaster at Jurassic Park, mathematician Ian Malcolm, who survived the events of the previous novel',
        #           'suspects that the disease will lead to the eventual extinction of the dinosaurs on Isla Sorna.'),
        3506780:('A boy living in a polluted town visits a strange isolated man called the Once-ler "at the far end of town where the Grickle-grass grows... [on] the Street of the Lifted Lorax"',
           'protected from logging, "the Lorax, and all of his friends may come back."')
    }

    for id in tests:
        summary,wiki_work = restricted_mwpfh_extract_wiki_plot_summary_text_by_id(id,engine,wiki_file_dir,return_wiki_work=True)

        if tests[id] is None and summary is None:
            print str(wiki_work['wiki_id'])+": "+wiki_work['title']+' summary correctly deemed missing from article'
        elif tests[id] is not None and summary is None:
            print 'ERROR: '+str(wiki_work['wiki_id'])+": "+wiki_work['title']+' summary incorrectly deemed missing from article'
        elif tests[id] is  None and summary is not None:
            print 'ERROR: '+str(wiki_work['wiki_id'])+": "+wiki_work['title']+' summary discovered when it is actually missing from article'
        else:
            if summary and tests[id] and summary.startswith(tests[id][0].decode('utf-8')) \
                and summary.endswith(tests[id][1].decode('utf-8')):
                print str(wiki_work['wiki_id'])+": "+wiki_work['title']+' summary extracted correctly'
            else:
                print 'ERROR: '+str(wiki_work['wiki_id'])+": "+wiki_work['title']+' summary not extracted correctly'


allowed_titles = ['plot','plot summary','plot synopsis','summary','synopsis']
allowed_pattern = '|'.join('('+x+')' for x in allowed_titles)
allowed_section_header_pattern = re.compile('('+'|'.join('(?:\=\= ?'+x+' ?\=\=)' for x in allowed_titles)+')',flags=re.IGNORECASE)
any_section_header_pattern = re.compile('\=\=.+\=\=')

def restricted_mwpfh_extract_wiki_plot_summary_text_by_id(wiki_id, engine, file_dir,return_wiki_work=False):
    '''
        Use regular expressions to home in on the section of Wikimarkup that is the plot summary section,
        then use mwparserfromhell to parse just that chunk and get a plaintext representation. A little
        hacky, but mwparserfromhell is buggy and hasn't been identifying/extracting sections correctly
        when run on the whole wikitext.
        :return: Text of plot summary
    '''
    ret_summary = None
    try:
        query = 'select * from wiki_work where wiki_id='+str(wiki_id)

        row = engine.execute(query).fetchone()
        file_name = row['file_name']

        ret_summary = restricted_mwpfh_extract_wiki_plot_summary_text(file_name,file_dir)

    except:
        traceback.print_exc()

    if return_wiki_work:
        wiki_work = WikiWork(row)
        return ret_summary,wiki_work
    else:
        return ret_summary

def restricted_mwpfh_extract_wiki_plot_summary_text(file_name,file_dir):
        ret_summary = None
        try:
            file = open(file_dir+'/'+file_name)

            article_text = file.read().decode('utf-8')
            file.close()

            pmatch = allowed_section_header_pattern.search(article_text)

            if (pmatch):
                pheader = pmatch.group(0)
                ematch = any_section_header_pattern.search(article_text,pmatch.start(0)+len(pheader))
                if not ematch:
                    sys.stderr.write('Error: Could not find a section header after plot header for article in file {}\n'.format(file_name))

                section_doc = mwparserfromhell.parse(article_text[pmatch.start(0)+len(pheader):ematch.start(0)])
                ret_summary = section_doc.strip_code()
        except:
            sys.stderr.write('Exception while extracting from {}:\n'.format(file_name))
            traceback.print_exc()

        return ret_summary


if __name__ == '__main__':
    test()