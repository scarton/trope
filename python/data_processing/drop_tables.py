__author__ = 'Sam'
'''
Make all the tables
'''
import json
from sqlalchemy import create_engine
import traceback
import platform

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss

    server = 'gauss'
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing

    server = 'local'
else:
    raise Exception('Unknown system. I do not know how to assign file names')


def main():
    print 'Dropping all tables on',server,'server'

    sure = raw_input('Are you sure you want to do this? (Y/n): ')

    if sure == 'Y':
        with open('../db.config') as c:
            config = json.load(c)['servers'][server]

        engine = create_engine('mysql+mysqlconnector://'+config['user']+':'+config['password']+'@'+config['host']+'/'+config['database'])

        queries = [
        '''drop table trope''',
        '''drop table tvt_work''',
        '''drop table tvt_work_supplement''',
        '''drop table wiki_tvt_work_link''',
        '''drop table wiki_work''',
        '''drop table tvt_work_trope_link''']

        for query in queries:
            try:
                print query
                engine.execute(query)
            except Exception as ex:
                traceback.print_exc(limit=3)


        print 'Done'
    else:
        print 'Aborting. No tables dropped.'

if __name__ == '__main__':
    main()