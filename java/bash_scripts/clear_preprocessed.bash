#!/bin/bash

dir=..
wdir=/storage6/users/scarton/tropes_2/data/wiki_films

java -cp $dir/bin:$dir/lib/* db.scripts.TruncateMostTables

echo Removing wiki articles from $wdir
rm $wdir/*

echo done
