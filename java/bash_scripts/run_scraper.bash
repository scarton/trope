#!/bin/bash

#Redirect all output and errors to a log file
dir=/home/scarton/tropes_2/troperepo/TropePGMs
today=$(date +"%Y-%m-%d")
java -cp $dir/bin:$dir/* scrape.ScrapeTVTropes >> $dir/scrape_log_$today.txt 2>&1 &

#java -cp $dir/bin:$dir/* scrape.ScrapeTVTropes 