package scrape;

import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class encodes some stuff I know about TVTropes, like the fact that
 * I am only interested in certain kinds of URLs on the site. 
 * @author Sam
 *
 */
public class URLJudger
{
	static Pattern validURLPattern = Pattern.compile("http://tvtropes\\.org/pmwiki/pmwiki\\.php/([A-Za-z0-9]+)/([A-Za-z0-9]+)");
	static Pattern tropesXtoYPattern = Pattern.compile("Tropes[A-Z]To[A-Z]");
	static enum Category {
		main,
		film,
		westernanimation,
		series,
		myth,
		literature,
		comics,
		animeandmanga,
		videogames,
		theatre
	}
	
	static HashSet<String> categories = putCatsInSet();
	

	
	public static void main(String[] args)
	{
		String[] testCases = {"http://tvtropes.org/pmwiki/pmwiki.php/Main/HomePage",
				"http://tvtropes.org/pmwiki/pmwiki.php/Theatre/TwelfthNight",
				"http://tvtropes.org/pmwiki/pmwiki.php/MadScientist/Comics",
				"http://tvtropes.org/pmwiki/pmwiki.php/TheLordOfTheRings/TropesAToC",
				"http://tvtropes.org/pmwiki/crowner.php/ImagePickin/TheNavigator?openall=yes#glkln2ld",
				"http://tvtropes.org/pmwiki/pmwiki.php/Myth/ClassicalMythology?from=Main.GreekMythology",
				"http://tvtropes.org/pmwiki/pmwiki.php/Characters/ClassicalMythology",
				"http://tvtropes.org/pmwiki/pmwiki.php/Heartwarming/TwelfthNight"};
		
		boolean[] valid = {true,
				true,
				true,
				true,
				false,
				false,
				false,
				false};
		
		for (int i = 0; i < testCases.length; i++)
		{
			System.out.println(testCases[i]);
			if (isValidAndInterestingURL(testCases[i]) == valid[i])
				System.out.println("\tJudged as: " + isValidAndInterestingURL(testCases[i])+". Should be: " + valid[i]);
			else
				System.out.println("\t******Judged as: " + isValidAndInterestingURL(testCases[i])+". Should be: " + valid[i]+"******");

		}
	}
	
	
	
	/**
	 * First or second group should contain one of our desired categories.
	 * 
	 * First example:
	 * http://tvtropes.org/pmwiki/pmwiki.php/Main/HomePage
	 * http://tvtropes.org/pmwiki/pmwiki.php/Theatre/TwelfthNight
	 * 
	 * Second example:
	 * http://tvtropes.org/pmwiki/pmwiki.php/MadScientist/Comics
	 * 
	 * OR, the second group should look like TropesXToY where X and Y are two 
	 * letters, (which can in fact be just X and Y)
	 * 
	 * Example:
	 * http://tvtropes.org/pmwiki/pmwiki.php/TheLordOfTheRings/TropesAToC
	 * @param url
	 * @return
	 */
	static boolean isValidAndInterestingURL(String url)
	{
		Matcher matcher = validURLPattern.matcher(url);
		
		if (matcher.matches())
		{
			//System.out.println(matcher.group(1) + " " +matcher.group(2));
			if (categories.contains(matcher.group(1).toLowerCase())
					|| categories.contains(matcher.group(2).toLowerCase())
					|| tropesXtoYPattern.matcher(matcher.group(2)).matches())
			{
				return true;
			}
		}
		
		return false;
	}

	private static HashSet<String> putCatsInSet()
	{
		HashSet<String> cats = new HashSet<String>();
		for (Category category : Category.values())
		{
			cats.add(category.name());
		}
		return cats;
	}
	
}
