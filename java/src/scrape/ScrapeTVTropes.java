package scrape;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * This class is a script which scrapes TVTropes.org into a folder using urls
 * for web page names. It maintains a set of urls that it has scraped and a 
 * stack of urls that it has yet to scrape, both written to disk after every 
 * round, so it is interruptable. 
 * 
 * This literally just scrapes the entirety of TVTropes.org. It doesn't do any 
 * processing of TVTropes.org html except to look for links to other TVTropes pages
 * to follow.
 * 
 * @author Sam
 *
 */
public class ScrapeTVTropes
{
	static String logDir = "/storage6/data/tvtropes/";
	static String dataDir = "/storage6/data/tvtropes/web_pages/";
	static String defaultStartURL = "http://tvtropes.org/pmwiki/pmwiki.php/Main/HomePage";
	static String visitedFile = "visitedSet.ser";
	static String visitQueueFile = "toVisitQueue.ser";
	static String visitSetFile = "toVisitSet.ser";
	static String errorFile = "errorSet.ser";
	static String skippedFile = "skippedSet.ser";
	static String defaultPrefix = "http://tvtropes.org";
	
	static boolean debug = false;
	static int debugStopCount =1;
	static int waitInterval = 3000; //Wait 5 seconds between queries. 
	
	static int timeout = 30000; //Don't timeout for 30 seconds
	
	public static void main(String[] args)
	{
		System.out.println("Starting up TVTropes scraper.");
		
		HashSet<String> visited = readStringSet(logDir+visitedFile);
		LinkedList<String> visitQueue =  readStringQueue(logDir+visitQueueFile);
		HashSet<String> visitSet = readStringSet(logDir+visitSetFile);
		HashSet<String> errors = (HashSet<String>) readStringSet(logDir+errorFile);
		HashSet<String> skipped = (HashSet<String>) readStringSet(logDir+skippedFile);


		if (visited.isEmpty())
		{
			System.out.println("Visited list is empty or nonexistant, so adding default starting html to queue: " + defaultStartURL);
			visitQueue.push(defaultStartURL);
		}
		
		try
		{
			int vCount = 0;
			while (!visitQueue.isEmpty())
			{
				
				String url = visitQueue.pollLast();
/*				if (!isValidTVTropesPage(url))
				{
					skipped.add(url);
					System.out.println("Previously queued URL " + url + " is not a valid TVTropes page. Skipping, for a total of " + skipped.size() + " skipped.");
					continue;
				}*/
				
				System.out.println("Scraping " + url);
				
				try
				{
					Document doc = getHTML(url);
					writeToFile(doc.outerHtml(),dataDir+URLEncoder.encode(url,"UTF-8"));
					int added = 0;
					for (Element el : doc.getElementsByTag("a"))
					{
						
						if (el.hasAttr("href"))
						{
							String newURL=addPrefixIfNecessary(el.attr("href"));
							//Add url to queue if we haven't scraped it or already added it to the queue, 
							//and if it is valid
							if (!visited.contains(newURL)
									&& !url.equals(newURL)
									&& !visitSet.contains(newURL)
									&& isValidTVTropesPage(newURL))
							{
								visitQueue.push(newURL);
								visitSet.add(newURL);
								added++;
								//System.out.println("Yes: "+newURL);
							}
							else
							{
								skipped.add(newURL);
							}
						}
						
	
					}
					
					visited.add(url);
					System.out.println("\t"+added + " added to queue for total of " + visitQueue.size()+". " + visited.size() + " pages visited so far, " + skipped.size() + " skipped.");
					
					vCount++;
					if (debug && vCount >= debugStopCount) //don't do too much if we are just trying to debug
						break;	
				}
				catch (Exception ex)
				{
					errors.add(url);
					System.err.println("Error while trying to scrape URL: " + url);
					ex.printStackTrace();
					System.out.println(errors.size() + " errors found total.");
					continue;
				}
				
				
				//Pause a bit between requests.
				Thread.sleep(waitInterval);
			}
			System.out.println("To-visit queue is empty, so we appear to be done. " + vCount + " pages scanned this round, for a total of " + visited.size());
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			System.out.println("An error occurred. The scraper may need to be restarted.");
		}
		finally
		{
			System.out.println("Serializing various sets and queues to disk");
			writeCollection(visited,logDir+visitedFile);
			writeCollection(visitQueue,logDir+visitQueueFile);
			writeCollection(visitSet,logDir+visitSetFile);
			writeCollection(errors,logDir+errorFile);
			writeCollection(skipped,logDir+skippedFile);


		}
		System.out.println("Done.\n\n\n");
	}
	
	
	/**
	 * TVTropes has a lot of shortened URLs, like 
	 * /pmwiki/pmwiki.php/Administrivia/WelcomeToTVTropes
	 * to mean
	 * http://tvtropes.org/pmwiki/pmwiki.php/Administrivia/WelcomeToTVTropes
	 * 
	 * This method just adds on that first part if it is missing
	 * 
	 * I guess I could have just used the baseURI feature of the Jsoup.parse()
	 * method... oh well. 
	 * 
	 * @param url
	 * @return
	 */
	private static String addPrefixIfNecessary(String url)
	{
		if (url.startsWith("/"))
			return defaultPrefix+url;
		else
			return url;
	}

	/**
	 * Check if a URL is valid for exploration. We only want TVTropes URLs, and
	 * we only want certain kinds. 
	 * @param url
	 * @return
	 */
	private static boolean isValidTVTropesPage(String url)
	{
		return URLJudger.isValidAndInterestingURL(url);
	}

	private static void writeToFile(String html, String filePath) throws Exception
	{
		System.out.println("\tWriting html to " + filePath);

		File newFile = new File(filePath);
		if (!newFile.exists())
			newFile.createNewFile();
		FileUtils.writeStringToFile(newFile, html);
	}

	static HashSet<String> readStringSet(String filePath) 
	{
		System.out.println("Reading serialized object from " + filePath);
		HashSet<String> set = null;
		try
		{
			FileInputStream fIn = new FileInputStream(filePath);
			ObjectInputStream oIn = new ObjectInputStream(fIn);
			set = (HashSet<String>) oIn.readObject();
			oIn.close();
			fIn.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		if (set == null)
		{
			System.out.println("No set found in file. Creating new one.");
			set = new HashSet<String>();
		}
		else
			System.out.println("Successfully read set. " + set.size() + " objects in set");

		return set;
	}
	
	static LinkedList<String> readStringQueue(String filePath) 
	{
		System.out.println("Reading serialized object from " + filePath);
		LinkedList<String> queue = null;
		try
		{
			FileInputStream fIn = new FileInputStream(filePath);
			ObjectInputStream oIn = new ObjectInputStream(fIn);
			queue = (LinkedList<String>) oIn.readObject();
			oIn.close();
			fIn.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
		if (queue == null)
		{
			System.out.println("No queue found in file. Creating new one.");
			queue = new LinkedList<String>();
		}
		else
			System.out.println("Successfully read queue. " + queue.size() + " objects in queue");

		return queue;
	}
	
	private static Document getHTML(String url) throws Exception
	{

			Connection connection = Jsoup.connect(url);
			connection.timeout(timeout);
			Document doc = connection.get();
			return doc;
	}
	
	private static void writeCollection(Collection obj, String filePath)
	{
		try
		{
			
			FileOutputStream fOut = new FileOutputStream(filePath);
			ObjectOutputStream oOut = new ObjectOutputStream(fOut);
			oOut.writeObject(obj);
			oOut.close();
			fOut.close();
			System.out.println("Collection (" +obj.size()+" elements) written to "+ filePath);
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}
	

}
