package db.scripts;

import db.TropeDatabase;

/**
 * Java pseudo script that drops the tables created by the CreateAllDatabases
 * class
 * @author Sam
 *
 */
public class DropMostTables
{
	public static void main(String[] args) throws Exception
	{
		System.out.println("Dropping tables...");
		TropeDatabase db = new TropeDatabase();
		
		try
		{
			db.dropTables();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			db.close();
		}
		
		System.out.println("Done");
	}
}
