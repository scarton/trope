package db.scripts;

import db.TropeDatabase;

/**
 * Java pseudo script that creates the DBs used in the project
 * @author Sam
 *
 */
public class CreateAllTables
{

	public static void main(String[] args) throws Exception
	{
		System.out.println("Creating tables...");
		TropeDatabase db = new TropeDatabase();
		
		try
		{
			db.createTables();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			db.close();
		}
		
		System.out.println("Done");

	}

}
