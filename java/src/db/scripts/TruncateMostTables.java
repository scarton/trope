package db.scripts;

import db.TropeDatabase;

public class TruncateMostTables
{
	public static void main(String[] args) throws Exception
	{
		System.out.println("Truncating tables...");
		TropeDatabase db = new TropeDatabase();
		
		try
		{
			db.truncateTables();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			db.close();
		}
		
		System.out.println("Done");
	}
}

