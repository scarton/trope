package db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

import db.schema.Field;
import db.schema.TVTropesWorkTable;
import db.schema.Table;
import db.schema.TropeLinkTable;
import db.schema.TropeTable;
import db.schema.WikiTVTropesLinkTable;
import db.schema.WikiWorkTable;
import db.schema.TVTropesWorkSupplementTable;
import db.schema.WorkTable;
import db.schema.WorkTropeLinkTable;
import entity.FilmInstance;
import entity.NaiveBayesExperimentData;
import entity.TVTropesFilm;
import entity.Trope;
import entity.TropeCoOccurrenceLink;
import entity.WikiFilm;

/**
 * Wrapper around a MySQL database of info pertaining to a TVTropes machine 
 * learning experiment. 
 * 
 * Please call the close() method before terminating. 
 * @author Sam
 *
 */
public class TropeDatabase
{
	private String host;
	private String database;
	private String user;
	private Connection connection;
	
	//Schema for this database, awkwardly expressed in Java data structures 
	//I guess this is what JPA is for? I don't know. Can't be bothered. 
	public enum EnumTable{
		Trope(new TropeTable()),
		WorkTropeLink(new WorkTropeLinkTable()),
		TVTropesWorkSupplement(new TVTropesWorkSupplementTable()),

		TVTropesWork(new TVTropesWorkTable()),
		WikiWork(new WikiWorkTable()),
		WikiTVTropesLink(new WikiTVTropesLinkTable());
		
//		TopTrope(new TropeTable("top_trope")),
//		TrainWikiTVTropesLink(new WikiTVTropesLinkTable("train_wiki_tvtropes_link")),
//		TestWikiTVTropesLink(new WikiTVTropesLinkTable("test_wiki_tvtropes_link")),
//		TrainWorkTropeLink(new WorkTropeLinkTable("train_work_trope_link")),
//		TestWorkTropeLink(new WorkTropeLinkTable("test_work_trope_link")),
//		
//		TropeLink(new TropeLinkTable());


		

		
		public final Table table;
		
		EnumTable(Table table)
		{
			this.table = table;
		}
		
		public String getName()
		{
			return this.table.getName();
		}
		
		public String toString()
		{
			return this.table.getName();
		}
	}
	

	
	public TropeDatabase () throws Exception
	{
		this.establishConnectionToDatabase(Constants.host, Constants.database, Constants.user, Constants.pass);
	}
	
	private void establishConnectionToDatabase(String host, String database, String user, String pass)  throws Exception
	{
		this.host = host;
		this.database = database;
		this.user = user;
		
		connection = this.getNewConnection(database,pass);	
		
	}
	
	/**
	 * Anything calling TropeDatabase should call this method before terminating
	 * @throws Exception
	 */
	public void close()
	{
		try
		{
			connection.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
	private Connection getNewConnection(String databaseName, String pass) throws Exception
	{
		
		String url = "jdbc:mysql://" + host + ":3306/"+ databaseName;
		Class.forName("com.mysql.jdbc.Driver").newInstance();


		//connection = DriverManager.getConnection(url, userName, password);
		Properties props = new Properties();
		props.setProperty("user", user);
		if (null != pass && !pass.isEmpty()) props.setProperty("password", pass);
		props.setProperty("useUnicode","true");
		props.setProperty("characterEncoding", "UTF-8");
		props.setProperty("connectionCollation", "utf8_unicode_ci");
		props.setProperty("useJvmCharsetConverters", "true");
		try
		{
			Connection conn = DriverManager.getConnection(url, props);
			return conn;
		}
		catch (Exception ex)
		{
			System.err.println(url);
			System.err.println(props);
			throw ex;
			
		}
		
		
	}
	
	
	/**
	 * Create tables specified in schema enum
	 */
	public void createTables() throws Exception
	{
		
		for (EnumTable eTable: EnumTable.values())
		{
			createTable(eTable);
			
		}
		
		
	}
	
	public void createTable(EnumTable eTable) throws Exception
	{
		Statement s = connection.createStatement();

		System.out.println("Creating table " + eTable + " if it doesn't already exist");
		StringBuilder qBuilder = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
		Table table = eTable.table;
		qBuilder.append(table.getName() +" (");
		for (int i =0; i < table.getFields().length ; i++)
		{
			Field field = table.getFields()[i];
			//System.out.println("\t"+field.getName());
			if (i > 0) qBuilder.append(", ");
			qBuilder.append(field.getName() + " " + field.getType());
			
			if (field.getAutoInc())
				qBuilder.append(" AUTO_INCREMENT"); 
		}
		
		if (table.getPrimaryKey() != null)
			qBuilder.append(", PRIMARY KEY ("+table.getPrimaryKey().getName()+")");
		
		for (Field field : table.getIndexes())
		{
			qBuilder.append(", KEY ("+field.getName()+")");
		}
		
		qBuilder.append(")");
		System.out.println(qBuilder.toString());
		s.execute(qBuilder.toString());
		s.close();

	}

	public void dropTables() throws Exception
	{
		Statement s = connection.createStatement();
		
		for (EnumTable eTable: EnumTable.values())
		{
			System.out.println("Dropping table " + eTable);
			StringBuilder qBuilder = new StringBuilder("DROP table IF EXISTS ");
			Table table = eTable.table;
			qBuilder.append(table.getName());
			s.execute(qBuilder.toString());
			
		}
		
		s.close();
		
	}

/*	*//**
	 * Insert a work into the work table. Assume that the wiki and tvtropes
	 * titles are identical
	 * @param title
	 * @param string
	 * @param string2
	 *//*
	public void insertWork(String title, String tropeFileName, String wikiFileName, boolean isRedirect) throws Exception
	{
		try
		{

			PreparedStatement p = connection.prepareStatement("insert into " + EnumTable.Work.table.getName() 
					+ " (wiki_title,trope_title,trope_file_name,wiki_file_name,trope_redirect)"
					+ " values (?,?,?,?,?)");
			
			String query = "insert into " 
					+ " values ( '"+title+"','"+title+"','"+tropeFileName+"','"+wikiFileName+"',"+isRedirect+")";
			
			p.setString(1,title);
			p.setString(2,title);
			p.setString(3, tropeFileName);
			p.setString(4,wikiFileName);
			p.setBoolean(5,isRedirect);
			
			p.execute();
			p.close();
		}
		catch (Exception ex)
		{
			//System.out.println(query);
			throw ex;
		}

		
	}
*/
	public void insertWork(String title, TVTropesFilm tvTropesFilm, String string)
	{
		// TODO Auto-generated method stub
		
	}

	public void insertTVTWork(TVTropesFilm film) throws Exception
	{
		String query = "insert into " + EnumTable.TVTropesWork.table.getName() 
					+ " (tvt_id,title,type,redirect,redirect_type,min_year,max_year,file_name)"
					+ " values (?,?,?,?,?,?,?,?)";
		
/*		tvt_id("tvt_id","int"),
		title("title","varchar(256)"),
		type("type","varchar(64)"),
		redirect("redirect","boolean"),
		redirect_type("redirect_type","varchar(64)"),
		min_year("min_year","int"),
		max_year("max_year","int");*/
		String redirectType = (film.isRedirect ? film.type : null);
		

		PreparedStatement p = connection.prepareStatement(query);
		p.setInt(1, film.articleID);
		p.setString(2, film.title);
		p.setString(3,"film");
		p.setBoolean(4, film.isRedirect);
		setValue(p,5,redirectType,Types.VARCHAR);
		setValue(p,6, film.minYear,Types.INTEGER);
		setValue(p,7, film.maxYear, Types.INTEGER);
		setValue(p,8, film.fileName);

		p.execute();

		
	}
	

	public void setValue(PreparedStatement s, int index, Object obj) throws SQLException
	{
		setValue(s,index,obj,-1);
	}
	
	public void setValue(PreparedStatement s, int index, Object obj,int type) throws SQLException
	{
		
		if (obj == null)
		{	
			if (type != -1)
			{
				s.setNull(index, type);
			}
			else
			{
				if (obj instanceof String)
				{
					s.setNull(index, Types.VARCHAR);
				}
				else if (obj instanceof Integer)
				{
					s.setNull(index, Types.INTEGER);
				}
				else if (obj instanceof Long)
				{
					s.setNull(index, Types.BIGINT);
				}
				else if (obj instanceof Boolean)
				{
					s.setNull(index, Types.BOOLEAN);
				}
				else
				{
					throw new SQLException("Unsupported type for null insertion");
				}
			}
			
		}
		else
		{
			if (obj instanceof String)
			{
				s.setString(index, (String) obj);
			}
			else if (obj instanceof Integer)
			{
				s.setInt(index, (Integer) obj);
			}
			else if (obj instanceof Long)
			{
				s.setLong(index, (Long) obj);
			}
			else if (obj instanceof Boolean)
			{
				s.setBoolean(index, (Boolean) obj);
			}
			else
			{
				throw new SQLException("Unsupported type for non-null insertion");
			}
		}
	}

	public void insertWikiWork(WikiFilm film) throws Exception
	{
		/*Fields:
		wiki_id("wiki_id","bigint"),
		title("title","varchar(256)"),
		type("type","varchar(64)"),
		file_name("file_name","varchar(256)"),
		release_year("release_year","int");		
	
*/
		PreparedStatement s = connection.prepareStatement("insert into " + EnumTable.WikiWork.table.getName() 
					+ " (wiki_id,title,type,file_name,release_year,url_title)"
					+ " values (?,?,?,?,?,?)");
		
		setValue(s, 1, film.wikiID);
		setValue(s,2,film.title);
		setValue(s,3,"film");
		setValue(s,4,film.fileName);
		setValue(s,5,film.releaseYear,Types.INTEGER);
		setValue(s,6,film.urlTitle);

		
		s.execute();
		s.close();
		
	}

	public void insertWikiTVTropesLink(Long wikiID, int articleID, int mStrength) throws Exception
	{
		/*Fields:
		id("id","int",true), //auto increment
		wiki_id("wiki_id","bigint"),
		tvt_id("tvt_id","int"),
		strength("strength","int")*/
		
		PreparedStatement s = connection.prepareStatement("insert into " + EnumTable.WikiTVTropesLink.table.getName() 
				+ " (wiki_id,tvt_id,strength)"
				+ " values (?,?,?)");
	
		setValue(s, 1, wikiID);
		setValue(s,2,articleID);
		setValue(s,3,mStrength);

		s.execute();
		s.close();
		
	}

	public void truncateTables() throws Exception
	{
		Statement s = connection.createStatement();
		
		for (EnumTable eTable: EnumTable.values())
		{
			System.out.println("Truncating table " + eTable);
			StringBuilder qBuilder = new StringBuilder("truncate table ");
			Table table = eTable.table;
			qBuilder.append(table.getName());
			s.execute(qBuilder.toString());
			
		}
		
		s.close();
		
	}

	/**
	 * Get all the TVTropes films for which we were able to find a matching Wikipedia film
	 * @return
	 * @throws Exception
	 */
	public ArrayList<TVTropesFilm> getAllMatchedTVTFilms() throws Exception
	{
		/*	DB Schema:	
		 * tvt_id("tvt_id","int"),
		title("title","varchar(256)"),
		type("type","varchar(64)"),
		redirect("redirect","boolean"),
		redirect_type("redirect_type","varchar(64)"),
		min_year("min_year","int"),
		max_year("max_year","int"),
		file_name("file_name","varchar(512)");
		
		Class constructor:
		TVTropesFilm(int articleID, String type, String title, String titleC, Integer minYear, Integer maxYear, boolean isRedirect, boolean valid,
			String fileName)
		*/
		
		String query = "select t.tvt_id,t.type,t.title,t.min_year,t.max_year,t.redirect,t.file_name from " + 
		EnumTable.TVTropesWork.table.getName() + " as t join ("+EnumTable.WikiTVTropesLink.table.getName() + 
		" as l) on (t.tvt_id = l.tvt_id)" ;
		
		ArrayList<TVTropesFilm> rList = new ArrayList<TVTropesFilm>();
		Statement s = connection.createStatement();
		ResultSet rSet = s.executeQuery(query);
		
		while (rSet.next())
		{
			TVTropesFilm film = new TVTropesFilm(rSet.getInt(1),
					rSet.getString(2),
					rSet.getString(3),
					"",
					rSet.getInt(4),
					rSet.getInt(5),
					rSet.getBoolean(6),
					true,
					rSet.getString(7));
			rList.add(film);
		}
		

		s.close();

		// TODO Auto-generated method stub
		return rList;
	}

	public void addSupplementalTVTFile(int articleID, String supFileName) throws Exception
	{
		String query = "insert into " + EnumTable.TVTropesWorkSupplement.table.getName() + " (tvt_id,file_name) values (?,?)";
		PreparedStatement s = connection.prepareStatement(query);
		s.setInt(1, articleID);
		s.setString(2, supFileName);
		s.execute();
		
		s.close();
		
		
	}

	public void insertWorkTropeLink(int workID, Integer tropeID, String anchorText) throws Exception
	{
		String query = "insert into " + EnumTable.WorkTropeLink.table.getName() + " (tvt_id,trope_id,anchor_text) values (?,?,?)";
		
		PreparedStatement s = connection.prepareStatement(query);
		s.setInt(1, workID);
		s.setInt(2, tropeID);
		s.setString(3, anchorText);
		try
		{
		s.execute();
		}
		catch (Exception ex)
		{
			System.err.println(s);
			throw ex;
		}
		
		s.close();
		
	}

	public void insertTrope(Trope trope) throws Exception
	{
		String query = "insert into " + EnumTable.Trope.table.getName() + " (tvt_id,title,file_name) values (?,?,?)";
		PreparedStatement s = connection.prepareStatement(query);
		s.setInt(1, trope.ID);
		s.setString(2, trope.title);
		s.setString(3,trope.fileName);
		s.execute();
		
		s.close();
		
	}

	public ArrayList<WikiFilm> getAllWikiFilms() throws Exception
	{
		
		PreparedStatement s = connection.prepareStatement("select (wiki_id,title,type,file_name,release_year) from" +
				 EnumTable.WikiWork.table.getName());
	
		ArrayList<WikiFilm> rList = new ArrayList<WikiFilm>();
		ResultSet rSet = s.executeQuery();
		
		while (rSet.next())
		{
			rList.add(new WikiFilm(rSet.getLong(1),
					rSet.getString(2),
					rSet.getString(4),
					rSet.getInt(5)));
		}
		
		s.execute();
		s.close();
		
		return rList;
	}

	
	/**
	 * Create a table for all the tropes that occur more than X number of times
	 *//*
	public void createTopTropeTable(int threshold) throws Exception
	{
		createTable(EnumTable.TopTrope);
		
		String query = "insert into "+EnumTable.TopTrope.table.getName()+" "
				+ "select id,title,file_name from (select trope.*,count(*) as cnt from "+EnumTable.WorkTropeLink.table.getName()+" as link "
				+ "join ("+EnumTable.Trope.table.getName()+") on "
				+ "(link.trope_id = trope.id) group by trope_id order by cnt desc) as T where cnt >= "+threshold;
		System.out.println(query);
		PreparedStatement s = connection.prepareStatement(query);
		s.execute();
		s.close();
		
		
	}

	public void createTrainingAndTestTables(double split) throws Exception
	{
		//Create tables if they don't exist

		createTable(EnumTable.TrainWikiTVTropesLink);
		createTable(EnumTable.TestWikiTVTropesLink);
		createTable(EnumTable.TrainWorkTropeLink);
		createTable(EnumTable.TestWorkTropeLink);
		
		String q1 = "select count(*) from "+EnumTable.WikiTVTropesLink.getName();
		System.out.println(q1);
		PreparedStatement s = connection.prepareStatement(q1);
		ResultSet r1 = s.executeQuery();
		r1.first();
		int count = r1.getInt(1);
		s.close();
		
		long train = Math.round(split*count);
		long test = count - train;
		
		//Insert a random selection from wiki_work into train_wiki_work
		String q2 = "insert into " + EnumTable.TrainWikiTVTropesLink + 
				" select * from " + EnumTable.WikiTVTropesLink + " order by rand() limit " + train;
		System.out.println(q2);
		//s = connection.prepareStatement(q2);
		s.execute();
		s.close();
		
		//Insert the rest of wiki_work into test_wiki_work
		String q3 = "insert into test_wiki_tvtropes_link select link.* from wiki_tvtropes_link "
				+ "as link left join train_wiki_tvtropes_link as train on link.wiki_id = train.wiki_id "
				+ "where train.wiki_id is null";
		System.out.println(q3);
		//s = connection.prepareStatement(q3);
		s.execute();
		s.close();
		
		//Insert appropriate links into train_work_trop_link table
		
		String q4 = " replace into train_work_trope_link select link.* from work_trope_link as link join (train_wiki_tvtropes_link as work, top_trope as trope) on link.tvt_id = work.tvt_id and link.trope_id = trope.tvt_id";
		
		
		//insert appropriate links into test_work_trope_link table
		String q5 = "replace into test_work_trope_link select link.* from work_trope_link as link join (test_wiki_tvtropes_link as work, top_trope as trope) on link.tvt_id = work.tvt_id and link.trope_id = trope.tvt_id";
		
		
		
		
	}*/

	public void createTropeCoOccurrenceTable(int maxDegree)
	{
		// TODO Auto-generated method stub
		
	}

	public ArrayList<TropeCoOccurrenceLink> getTrainingTropeCoOccurrenceLinks() throws Exception
	{
		String query = "select * from train_trope_link order by strength desc";
		ArrayList<TropeCoOccurrenceLink> links = new ArrayList<TropeCoOccurrenceLink>();
		
		PreparedStatement s = connection.prepareStatement(query);
		ResultSet rSet = s.executeQuery();
		
		while (rSet.next())
		{
			TropeCoOccurrenceLink link = new TropeCoOccurrenceLink(rSet.getInt(1),
					rSet.getInt(2),
					rSet.getInt(3),
					rSet.getDouble(4),
					rSet.getDouble(5));
			links.add(link);
		}
		s.close();
		
		return links;
	}

	public Integer getNumberOfTrainingWorks() throws Exception
	{
		Integer num;
		String query = "select count(*) from train_wiki_tvtropes_link";
		
		PreparedStatement s = connection.prepareStatement(query);
		ResultSet rSet = s.executeQuery();
		
		rSet.first();
		num = rSet.getInt(1);
		s.close();
		
		return num;
	}

	public HashMap<Integer, Integer> getTrainingDegreeMap() throws Exception
	{
		String query = "select * from train_trope_degree";
		HashMap<Integer, Integer> rMap = new HashMap<Integer, Integer>();
		
		PreparedStatement s = connection.prepareStatement(query);
		ResultSet rSet = s.executeQuery();
		
		while (rSet.next())
		{
			rMap.put(rSet.getInt(1),rSet.getInt(2));
		}
		s.close();
		
		return rMap;
	}

	public void updateTropeCoOccurrenceLinks(ArrayList<TropeCoOccurrenceLink> links) throws Exception
	{
		String query = "update train_trope_link set likelihood = ?,expected =? where trope_id_1 = ? and trope_id_2 = ?";
		PreparedStatement s = connection.prepareStatement(query);
		int i =0;
		
		for (TropeCoOccurrenceLink link : links)
		{
			s.setDouble(1, link.getLikelihood());
			s.setDouble(2, link.getExpected());
			s.setInt(3, link.getTropeID1());
			s.setInt(4, link.getTropeID2());
			s.addBatch();

		}

		s.executeBatch();
		
		s.close();
	}

	public NaiveBayesExperimentData readNaiveBayesExperimentData() throws Exception
	{
		
		ArrayList<FilmInstance> trainFilms= readFilmInstanceList("train_wiki_tvtropes_link","train_work_trope_link");
		
		ArrayList<FilmInstance> testFilms = readFilmInstanceList("test_wiki_tvtropes_link","test_work_trope_link"); 

		ArrayList<Trope> topTropes = this.getTopTropes();
		
		return new NaiveBayesExperimentData(topTropes, trainFilms, testFilms);
	}

	public ArrayList<Trope> getTopTropes() throws Exception
	{
		String query = "select * from top_trope order by tvt_id";
		PreparedStatement s = connection.prepareStatement(query);
		ResultSet rSet = s.executeQuery();
		ArrayList<Trope> tropes = new ArrayList<Trope>();
		
		while (rSet.next())
		{
			tropes.add(new Trope(rSet.getString("title"),
					rSet.getInt("tvt_id"),
					rSet.getString("file_name")));
		}
		
		s.close();
		return tropes;
	}

	private ArrayList<FilmInstance> readFilmInstanceList(String workTableName, String linkTableName) throws Exception
	{
		
		//Each row read in here is one instance of a trope occurring in a film that has previously been
		//matched between Wikipedia and TVTropes. 
		String query = 	"select work.id,work.wiki_id,work.tvt_id,wiki.title,wiki.file_name,link.trope_id from "+workTableName+" as work "
				+ "join (wiki_work as wiki, "+linkTableName+" as link) "
						+ "on work.wiki_id = wiki.wiki_id and work.tvt_id = link.tvt_id" ;
		
		System.out.println(query);
		HashMap<Integer,FilmInstance> map = new HashMap<Integer, FilmInstance>();
		
		PreparedStatement s = connection.prepareStatement(query);
		ResultSet rSet = s.executeQuery();
		
		while (rSet.next())
		{
			if (!map.containsKey(rSet.getInt("id")))
			{
				FilmInstance instance = new FilmInstance(rSet.getString("file_name"),
						rSet.getLong("wiki_id"),
						rSet.getInt("tvt_id"),
						rSet.getInt("id"),
						rSet.getString("title"));
				map.put(instance.getID(),instance);
						
			}
			
			map.get(rSet.getInt("id")).getTropeIDs().add(rSet.getInt("trope_id"));


		}
		s.close();
		
		
		return new ArrayList<FilmInstance>(map.values());
	}

	public ArrayList<TropeCoOccurrenceLink> getTopTropeCoOccurrenceLinks(int n) throws Exception
	{
		ArrayList<Trope> topTropes = getTopTropes();
		HashMap<Integer,HashSet<Integer>> linkMap = new HashMap<Integer, HashSet<Integer>>();
		
		ArrayList<TropeCoOccurrenceLink> links = new ArrayList<TropeCoOccurrenceLink>();
		
		//For each trope, get the top n most unlikely edges on that trope
		for (Trope trope: topTropes)
		{
			Integer id = trope.ID;
			PreparedStatement s = connection.prepareStatement("select * from train_trope_link where trope_id_1 = "+id+" or trope_id_2 = "+id+" order by likelihood asc limit " + n);
			ResultSet rSet = s.executeQuery();
			while (rSet.next())
			{
				TropeCoOccurrenceLink link = new TropeCoOccurrenceLink(rSet.getInt("trope_id_1"), 
						rSet.getInt("trope_id_2"), 
						rSet.getInt("strength"), 
						rSet.getDouble("likelihood"), 
						rSet.getDouble("expected"));
				
				//If we haven't seen this link yet, add it to the arraylist and to the linkMap
				if (!linkMap.containsKey(link.tropeID1) || !linkMap.get(link.tropeID1).contains(link.tropeID2))
				{
					links.add(link);
					
					if (!linkMap.containsKey(link.tropeID1))
						linkMap.put(link.tropeID1, new HashSet<Integer>());
					
					linkMap.get(link.tropeID1).add(link.tropeID2);
				}
			}
			s.close();
		}
		
		
		return links;
	}

	public void writeTropeLinksToFile(String filePath) throws Exception
	{
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("title_1,title_2,strength,likelihood,expected");
		PreparedStatement s = connection.prepareStatement(" select t1.title, t2.title, l.strength, l.likelihood, l.expected from train_trope_link as l "
				+ "join (top_trope as t1, top_trope as t2) on t1.tvt_id = l.trope_id_1 and t2.tvt_id = l.trope_id_2 order by likelihood asc");
		
		ResultSet rSet = s.executeQuery();
		
		while (rSet.next())
		{
			StringBuilder builder = new StringBuilder();
			builder.append("\""+rSet.getString(1)+"\"");
			builder.append(",");
			builder.append("\""+rSet.getString(2)+"\"");
			builder.append(",");
			builder.append(rSet.getInt(3));
			builder.append(",");
			builder.append(rSet.getDouble(4));
			builder.append(",");
			builder.append(rSet.getInt(5));
			
			lines.add(builder.toString());
		}
		
	
		s.close();
		FileUtils.writeLines(new File(filePath), lines);
		
	}
	
	
}
