package db.schema;

public class Field
{
	private String name;
	private String type;
	private boolean autoInc;
	
	Field(String name, String type, boolean autoInc)
	{
		this.name = name;
		this.type = type;
		this.autoInc = autoInc;
	}
	Field(String name, String type)
	{
		this.name = name;
		this.type = type;
		this.autoInc = false;
	}
	
	public String getName()
	{
		return name;
	}
	public String getType()
	{
		return type;
	}
	
	public boolean getAutoInc()
	{
		return autoInc;
	}
}
