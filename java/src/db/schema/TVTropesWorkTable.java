package db.schema;



/**
 * Wrapper for a table that represents works
 * @author Sam
 *
 */
public class TVTropesWorkTable extends Table
{
	private static String tableName = "tvtropes_work";
	
	public static enum EnumField implements FieldHaver{
		tvt_id("tvt_id","int"),
		title("title","varchar(256)"),
		type("type","varchar(64)"),
		redirect("redirect","boolean"),
		redirect_type("redirect_type","varchar(64)"),
		min_year("min_year","int"),
		max_year("max_year","int"),
		file_name("file_name","varchar(512)");
		
		
		//Was the trope page for this a redirect to a page for some other type of media
		//E.g. http://tvtropes.org/pmwiki/pmwiki.php/Film/AmericanPsycho
		//to http://tvtropes.org/pmwiki/pmwiki.php/Literature/AmericanPsycho

		
		private Field field;
		
		EnumField(String name, String type, boolean autoInc)
		{
			this.field = new Field(name, type, autoInc);
		}
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		public Field getField()
		{
			return field;
		}

	}
	
	public static Field[] indexes = {};

	public String getName()
	{
		return tableName;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.tvt_id.getField();
	}
	
	public Field[] getIndexes()
	{
		return indexes;
	}
}
