package db.schema;

import db.schema.TVTropesWorkSupplementTable.EnumField;


/**
 * Wrapper for a table that represents occurrence links between tropes and works
 * @author Sam
 *
 */
public class WorkTropeLinkTable extends Table
{
	private static String tableName = "work_trope_link";
	private String name;
	
	public static enum EnumField implements FieldHaver{
		id("id","int", true),
		trope_id("trope_id","int"),
		tvt_id("tvt_id","int"),
		anchor_text("anchor_text","text");
		private Field field;
		
		EnumField(String name, String type, boolean auto)
		{
			this.field = new Field(name, type, auto);
		}
		
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		public Field getField()
		{
			return field;
		}

	}

	
	
	public WorkTropeLinkTable()
	{
		super();
		this.name = tableName;
	}

	public WorkTropeLinkTable(String name)
	{
		super();
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.id.getField();
	}
	
	public Field[] getIndexes()
	{
		return new Field[]{};
	}
}
