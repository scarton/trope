package db.schema;


/**
 * This links a TVTropes work with supplemental files, eg:
 * 
 * http://tvtropes.org/pmwiki/pmwiki.php/Literature/TheLordOfTheRings
 * to
 * http://tvtropes.org/pmwiki/pmwiki.php/TheLordOfTheRings/TropesAToC
 * 
 * 
 * @author Sam
 *
 */
public class TVTropesWorkSupplementTable extends Table
{
	private static String tableName = "tvtropes_work_supplement";

	public static enum EnumField implements FieldHaver{
		id("id","int",true),
		tvt_id("tvt_id","int"),
		file_name("file_name","varchar (256)");
		private Field field;
		
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		EnumField(String name, String type, boolean auto)
		{
			this.field = new Field(name, type, auto);
		}
		public Field getField()
		{
			return field;
		}

	}

	public String getName()
	{
		return tableName;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.id.getField();
	}
	
	public Field[] getIndexes()
	{
		return new Field[]{};
	}
}
