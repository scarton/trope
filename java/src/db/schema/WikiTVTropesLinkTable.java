package db.schema;



/**
 * Wrapper for a table that represents works
 * @author Sam
 *
 */
public class WikiTVTropesLinkTable extends Table
{
	private static String tableName = "wiki_tvtropes_link";
	private String name;
	
	public static enum EnumField implements FieldHaver{
		id("id","int",true),
		wiki_id("wiki_id","bigint"),
		tvt_id("tvt_id","int"),
		strength("strength","int");//How good a match this is


		
		private Field field;
		
		EnumField(String name, String type, boolean autoInc)
		{
			this.field = new Field(name, type, autoInc);
		}
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		public Field getField()
		{
			return field;
		}

	}
	
	public static Field[] indexes = {};

	public WikiTVTropesLinkTable()
	{
		this.name = tableName;
	}
	
	public WikiTVTropesLinkTable(String n)
	{
		this.name = n;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.id.getField();
	}
	
	public Field[] getIndexes()
	{
		return indexes;
	}
}
