package db.schema;

public class TropeLinkTable extends Table
{
	private static String tableName = "trope_link";
	
	private String name;
	
	public static enum EnumField implements FieldHaver{
		id("id","int",true),
		trope_id_1("trope_id_1","int"),
		trope_id_2("trope_id_2","int"),
		strength("strength","int");
		
		private Field field;
		
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		EnumField(String name, String type, boolean auto)
		{
			this.field = new Field(name, type, auto);
		}
		
		public Field getField()
		{
			return field;
		}

	}
	
	public TropeLinkTable(String name)
	{
		this.name = name;
	}
	
	public TropeLinkTable()
	{
		this.name = tableName;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.id.getField();
	}
	
	public Field[] getIndexes()
	{
		return new Field[]{};
	}
}
