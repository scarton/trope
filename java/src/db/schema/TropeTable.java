package db.schema;

public class TropeTable extends Table
{
	private static String tableName = "trope";
	
	private String name;
	
	public static enum EnumField implements FieldHaver{
		tvt_id("tvt_id","int"),
		title("title","varchar(256)"),
		file_name("file_name","varchar(512)");
		
		private Field field;
		
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		public Field getField()
		{
			return field;
		}

	}
	
	public TropeTable(String name)
	{
		this.name = name;
	}
	
	public TropeTable()
	{
		this.name = tableName;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.tvt_id.getField();
	}
	
	public Field[] getIndexes()
	{
		return new Field[]{};
	}
}
