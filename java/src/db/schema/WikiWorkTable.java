package db.schema;



/**
 * Wrapper for a table that represents works
 * @author Sam
 *
 */
public class WikiWorkTable extends Table
{
	private static String tableName = "wiki_work";
	private String name;
	
	public static enum EnumField implements FieldHaver{
		wiki_id("wiki_id","bigint"),
		title("title","varchar(256)"),
		type("type","varchar(64)"),
		file_name("file_name","varchar(256)"),
		release_year("release_year","int"),
		url_title("url_title","varchar(256)");		

		
		private Field field;
		
		EnumField(String name, String type, boolean autoInc)
		{
			this.field = new Field(name, type, autoInc);
		}
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		public Field getField()
		{
			return field;
		}

	}
	
	public static Field[] indexes = {};
	
	public WikiWorkTable()
	{
		this.name = tableName;
	}
	
	public WikiWorkTable(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.wiki_id.getField();
	}
	
	public Field[] getIndexes()
	{
		return indexes;
	}
}
