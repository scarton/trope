package db.schema;


public abstract class Table
{
	
	public abstract String getName();
	public abstract Field getPrimaryKey();
	
	public abstract Field[] getFields();  
	public abstract Field[] getIndexes();

	public static Field[] getFields(FieldHaver[] fieldHavers)
	{
		Field[] arr = new Field[fieldHavers.length];
		for (int i = 0; i < arr.length; i++)
		{
			arr[i] = fieldHavers[i].getField();
		}
		
		return arr;

	}
}
