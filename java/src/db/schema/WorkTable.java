package db.schema;

import db.schema.TVTropesWorkSupplementTable.EnumField;


/**
 * Wrapper for a table that represents works
 * @author Sam
 *
 */
public class WorkTable extends Table
{
	private static String tableName = "work";
	
	public static enum EnumField implements FieldHaver{
		id("id","int",true),
		wiki_title("wiki_title","varchar(256)"),
		trope_title("trope_title","varchar(256)"),
		trope_file_name("trope_file_name","varchar(256)"),
		wiki_file_name("wiki_file_name","varchar(256)"),
		
		
		//Was the trope page for this a redirect to a page for some other type of media
		//E.g. http://tvtropes.org/pmwiki/pmwiki.php/Film/AmericanPsycho
		//to http://tvtropes.org/pmwiki/pmwiki.php/Literature/AmericanPsycho
		trope_redirect("trope_redirect","boolean"); 
		
		private Field field;
		
		EnumField(String name, String type, boolean autoInc)
		{
			this.field = new Field(name, type, autoInc);
		}
		EnumField(String name, String type)
		{
			this.field = new Field(name, type);
		}
		
		public Field getField()
		{
			return field;
		}

	}

	public String getName()
	{
		return tableName;
	}
	
	@Override
	public Field[] getFields()
	{
		return getFields(EnumField.values());
	}
	
	public Field getPrimaryKey()
	{
		return EnumField.id.getField();
	}
	
	public Field[] getIndexes()
	{
		return new Field[]{};
	}
}
