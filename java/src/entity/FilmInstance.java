package entity;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import db.Constants;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.Section;
import de.tudarmstadt.ukp.wikipedia.parser.Table;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParser;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParserFactory;


/**
 * This represents a combined wikipedia/tvtropes work. 
 * @author Sam
 *
 */
public class FilmInstance
{
	
	private static HashSet<String> synopsisSectionTitles = populateSynopsisSectionTitles();
	private static Pattern titlePattern = Pattern.compile("(plot)|(synopsis)|(story)|(summary)");
	
	HashSet<Integer> tropeIDs;
	String wikiFileName;
	Long wikiID;
	Integer tvtID;
	Integer ID;
	String wikiTitle;
	
	String synopsisText;

	public FilmInstance(String wikiFileName, Long wikiID, Integer tvtID, Integer iD, String wikiTitle)
	{
		super();
		this.tropeIDs = new HashSet<Integer>();
		this.wikiFileName = wikiFileName;
		this.wikiID = wikiID;
		this.tvtID = tvtID;
		ID = iD;
		this.wikiTitle = wikiTitle;
	}
	
	public HashSet<Integer> getTropeIDs()
	{
		return tropeIDs;
	}
	public void setTropeIDs(HashSet<Integer> tropeIDs)
	{
		this.tropeIDs = tropeIDs;
	}
	public String getWikiFileName()
	{
		return this.wikiFileName;
	}
	public void setWikiFileName(String wikiFileName)
	{
		this.wikiFileName = wikiFileName;
	}
	public String getSynopsisText()
	{
		return synopsisText;
	}
	public void setSynopsisText(String synopsisText)
	{
		this.synopsisText = synopsisText;
	}
	public Long getWikiID()
	{
		return wikiID;
	}
	public void setWikiID(Long wikiID)
	{
		this.wikiID = wikiID;
	}
	public Integer getTvtID()
	{
		return tvtID;
	}
	public void setTvtID(Integer tvtID)
	{
		this.tvtID = tvtID;
	}
	public Integer getID()
	{
		return ID;
	}
	public void setID(Integer iD)
	{
		ID = iD;
	}
	public String getWikiTitle()
	{
		return wikiTitle;
	}
	public void setWikiTitle(String wikiTitle)
	{
		this.wikiTitle = wikiTitle;
	}

	public boolean readPlotSummaryFromFile() throws Exception
	{
		MediaWikiParserFactory  factory = new MediaWikiParserFactory();
		MediaWikiParser parser = factory.createParser();
		
		String wikiText = FileUtils.readFileToString(new File(Constants.wikiFilmDir+wikiFileName));
		ParsedPage page = parser.parse(wikiText);
		
		
		Section synopsis = null;
		for (Section section : page.getSections())
		{
			if (section.getTitle() != null)
			{
				Matcher matcher = titlePattern.matcher(section.getTitle().toLowerCase());
				if (matcher.find())
				{
					synopsis = section;
					break;
				}
			}

		}
		
		if (synopsis != null)
		{
			this.synopsisText = synopsis.getText();
			return true;
		}
		else
		{
/*			for (Section section : page.getSections())
			{
				System.out.println("\t"+section.getTitle());
			}*/
			return false;
		}
		
	}
	
	
	
	/**
	 * List of all section titles that are likely to indicate a plot summary
	 * @return
	 */
	private static HashSet<String> populateSynopsisSectionTitles()
	{
		HashSet<String> titles = new HashSet<String>();
		
/*		titles.add("storyline");
		titles.add("plot summary");
		titles.add("plot details");
		titles.add("the plot");
		titles.add("characters and story");
		titles.add("plot");
		titles.add("synopsis");
		titles.add("plot synopsis");
		titles.add("story");
		titles.add("summary");
		titles.add("main plot"); */
		
		titles.add("storyline");
		titles.add("plot summary");
		titles.add("plot details");
		titles.add("the plot");
		titles.add("characters and story");
		titles.add("plot");
		titles.add("synopsis");
		titles.add("plot synopsis");
		titles.add("story");
		titles.add("summary");
		titles.add("main plot");
		
		
		return titles;
	}

	@Override
	public String toString()
	{
		return "FilmInstance [wikiFileName=" + wikiFileName + ", wikiID=" + wikiID + ", tvtID=" + tvtID + ", ID=" + ID + ", wikiTitle=" + wikiTitle + "]";
	}
	
	
	
}
