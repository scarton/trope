package entity;

public class WikiFilm
{
	
/*	wiki_id("wiki_id","bigint"),
	title("title","varchar(256)"),
	type("type","varchar(64)"),
	file_name("file_name","varchar(256)"),
	release_year("release_year","int");	*/
	
	public Long wikiID;
	public String title;
	public String type;
	public String fileName;
	public Integer releaseYear;
	public String urlTitle; //Title of the article as used in its URL
	
	public WikiFilm(Long wikiID, String title, String fileName, Integer releaseYear)
	{
		super();
		this.wikiID = wikiID;
		this.title = title;
		this.type = "film";
		this.fileName = fileName;
		this.releaseYear = releaseYear;
	}
	
	
	public String getUrlTitle()
	{
		return urlTitle;
	}


	public void setUrlTitle(String urlTitle)
	{
		this.urlTitle = urlTitle;
	}


	public WikiFilm(Long wikiID, String title)
	{
		super();
		this.wikiID = wikiID;
		this.title = title;
	}


	public Long getWikiID()
	{
		return wikiID;
	}
	public void setWikiID(Long wikiID)
	{
		this.wikiID = wikiID;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getFileName()
	{
		return fileName;
	}
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	public Integer getReleaseYear()
	{
		return releaseYear;
	}
	public void setReleaseYear(Integer releaseYear)
	{
		this.releaseYear = releaseYear;
	}
	
	

}
