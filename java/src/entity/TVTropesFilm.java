package entity;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import preprocess.MatchTVTropesToWikiArticles;

/**
 * Reads the HTML of a TVTropes webpage about a film. Figures out its title
 * and a time range in which it was released. This data is used elsewhere to 
 * match this article to the corresponding Wikipedia article. 
 * @author Sam
 *
 */
public class TVTropesFilm
{
	public int articleID;
	public String type;
	public String title;
	public String titleC;
	public Integer minYear;
	public Integer maxYear;
	public boolean isRedirect;
	public boolean valid;
	public String fileName;
	
	public static Pattern TVTitleStringPattern = Pattern.compile("([A-Za-z ]+): (.+)");
	public static Pattern filmCategoryPattern = Pattern.compile("Films of (?:(?:the ([0-9]{4})s)|(?:([0-9]{4})-([0-9]{4})))",Pattern.CASE_INSENSITIVE);

	public static void main(String[] args)
	{
/*		Matcher matcher = filmCategoryPattern.matcher("Films Of 2000-2004");
		matcher.matches();
		for (int i =0; i <= matcher.groupCount(); i ++)
		{
			System.out.println(matcher.group(i));
		}
		*/
		
		TVTropesFilm film = new TVTropesFilm("http://tvtropes.org/pmwiki/pmwiki.php/Film/Sharks3D");
		System.out.println(film);
		
		film = new TVTropesFilm("http://tvtropes.org/pmwiki/pmwiki.php/Film/SchindlersList");
		System.out.println(film);
	}
	
	
	public TVTropesFilm(int articleID, String type, String title, String titleC, Integer minYear, Integer maxYear, boolean isRedirect, boolean valid,
			String fileName)
	{
		super();
		this.articleID = articleID;
		this.type = type;
		this.title = title;
		this.titleC = titleC;
		this.minYear = minYear;
		this.maxYear = maxYear;
		this.isRedirect = isRedirect;
		this.valid = valid;
		this.fileName = fileName;
	}


	public TVTropesFilm(String url)
	{
		Document doc;
		try
		{
			doc = Jsoup.parse(new URL(url), 30000);
			initialize(doc,url,URLEncoder.encode(url,"UTF-8"));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TVTropesFilm(File tvtHTMLFile)
	{
		Document doc;
		try
		{
			doc = Jsoup.parse(tvtHTMLFile, "UTF-8");
			initialize(doc,URLDecoder.decode(tvtHTMLFile.getName(),"UTF-8"),tvtHTMLFile.getName());
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void initialize(Document doc, String url, String fileName)
	{
		this.fileName = fileName;
		valid = true;
		try
		{
			String idString = doc.getElementById("article_id").attr("value");
			if (idString != null && !idString.isEmpty())
			{
				articleID = Integer.parseInt(idString);
			}
			else
			{
				valid = false;
				return;
			}

			Element articleTitle = doc.getElementsByClass("article_title").first();


			Matcher matcher = TVTitleStringPattern.matcher(articleTitle.text());
			
			if (!matcher.matches())
			{
				valid = false;
				return;
			}
			type = matcher.group(1);
			title = matcher.group(2);
			titleC = MatchTVTropesToWikiArticles.cleanTitle(title);
			
			isRedirect = (!type.equalsIgnoreCase("Film")); //Valid so long as we are only looking at films

			
			Element wikiWalk = doc.getElementsByClass("wiki-walk").first();
			

			Element filmCategory = wikiWalk.getElementsMatchingOwnText(filmCategoryPattern).first();
			
			//Some film pages don't have categories. This is aight. 
			if (filmCategory != null)
			{
				Matcher fMatcher = filmCategoryPattern.matcher(filmCategory.text());
				fMatcher.find();
	/*			System.out.println(fMatcher.groupCount() + " groups");
				for (int i =0; i <= fMatcher.groupCount(); i++)
				{
					System.out.println("\t"+fMatcher.group(i));
				}*/
				//System.out.println();
				if (fMatcher.group(1) == null)
				{
					minYear = Integer.parseInt(fMatcher.group(2));
					maxYear = Integer.parseInt(fMatcher.group(3));
				}
				else
				{
					minYear = Integer.parseInt(fMatcher.group(1));
					maxYear = minYear+9;
				}
				
			}
			else
			{
				minYear = null;
				maxYear = null;
				
			}

			
		}
		catch (Exception ex)
		{
			System.err.println(url);
			System.err.println(this);
			valid = false;
			ex.printStackTrace();
		}
	}


	@Override
	public String toString()
	{
		return "TVTropesFilm [articleID=" + articleID + ", type=" + type + ", title=" + title + ", titleLC=" + titleC + ", minYear=" + minYear + ", maxYear="
				+ maxYear + ", isRedirect=" + isRedirect + ", valid=" + valid + "]";
	}
	

}
