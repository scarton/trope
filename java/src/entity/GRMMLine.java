package entity;

import java.util.ArrayList;

public class GRMMLine
{
	
	public ArrayList<String> labels;
	public ArrayList<String> features;
	
	
	public GRMMLine()
	{
		super();
		this.labels = new ArrayList<String>();
		this.features = new ArrayList<String>();
	}
	
	
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		
		for (String label : labels)
		{
			builder.append(label+" ");
		}
		
		builder.append("----");
		
		for (String feature : features)
		{
			builder.append(" " + feature);
		}
		
		return builder.toString();
	}
	
	public static ArrayList<GRMMLine> createLineList(int size)
	{
		ArrayList<GRMMLine> list = new ArrayList<GRMMLine>(size);
		for (int i =0; i < size; i++)
		{
			list.add(new GRMMLine());
		}
		return list;
	}

}
