package entity;

public class StringDouble implements Comparable
{
	public String string;
	public Double value;
	
	public StringDouble(String string, Double value)
	{
		this.string = string;
		this.value = value;
	}
	
	public int compareTo(Object obj)
	{
		return value.compareTo(((StringDouble) obj).value);
	}
	
	public String toString()
	{
		return string + ": " + value.toString();
	}
}
