package entity;

public class TropeCoOccurrenceLink
{
	public Integer tropeID1;
	public Integer tropeID2;
	public Integer strength;
	public Double likelihood;
	public Double expected;
	

	public TropeCoOccurrenceLink(Integer tropeID1, Integer tropeID2, Integer strength, Double likelihood, Double expected)
	{
		super();
		this.tropeID1 = tropeID1;
		this.tropeID2 = tropeID2;
		this.strength = strength;
		this.likelihood = likelihood;
		this.expected = expected;
	}
	public Integer getTropeID1()
	{
		return tropeID1;
	}
	public void setTropeID1(Integer tropeID1)
	{
		this.tropeID1 = tropeID1;
	}
	public Integer getTropeID2()
	{
		return tropeID2;
	}
	public void setTropeID2(Integer tropeID2)
	{
		this.tropeID2 = tropeID2;
	}
	public Integer getStrength()
	{
		return strength;
	}
	public void setStrength(Integer strength)
	{
		this.strength = strength;
	}
	public Double getLikelihood()
	{
		return likelihood;
	}
	public void setLikelihood(Double likelihood)
	{
		this.likelihood = likelihood;
	}
	public Double getExpected()
	{
		return expected;
	}
	public void setExpected(Double expected)
	{
		this.expected = expected;
	}
	
	

}
 