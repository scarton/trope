package preprocess;

import db.TropeDatabase;

public class CreateTropeCoOccurrenceNetwork
{

	/* maximum node degree for this network
	 * The purpose of this is to limit clique size to this number.
	 * A more sophisticated algorithm could allow nodes to have a highger degree than
	 * this while still limiting the clique size. 
	 */
	public static int maxDegree = 8; 
	
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Creating trope co-occurrence network with max degree " + maxDegree);
		TropeDatabase db = new TropeDatabase();
		
		db.createTropeCoOccurrenceTable(maxDegree);
		
		db.close();
	}
}
