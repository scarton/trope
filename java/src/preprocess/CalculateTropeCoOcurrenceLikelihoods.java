package preprocess;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.distribution.BinomialDistribution;

import db.TropeDatabase;
import entity.TropeCoOccurrenceLink;

public class CalculateTropeCoOcurrenceLikelihoods
{

	public static void main(String[] args) throws Exception
	{
		System.out.println("Calculating trope co-occurrence link likelihoods...");
		TropeDatabase db = new TropeDatabase();
		
		ArrayList<TropeCoOccurrenceLink> links = db.getTrainingTropeCoOccurrenceLinks();
		Integer works = db.getNumberOfTrainingWorks();
		double worksSquared = Math.pow(works, 2);
		//System.out.println("Trials: " + trials);

		HashMap<Integer,Integer> degreeMap = db.getTrainingDegreeMap();
		int sum = 0;
		for (Integer id : degreeMap.keySet())
		{
			sum += degreeMap.get(id);
		}
		sum = sum/2;
		double sumSquared = Math.pow(sum, 2);
		
		//System.out.println("N " + sum);
		
		int i = 0;
		for (TropeCoOccurrenceLink link : links)
		{
			//System.out.println();
			int d1 = degreeMap.get(link.tropeID1);
			int d2 = degreeMap.get(link.tropeID2);
			//System.out.println("Degrees: " + d1 + " and " + d2);
			
			double p = ((double)(d1*d2))/worksSquared ;
			//System.out.println("Prob: " + p);
			
			BinomialDistribution dist = new BinomialDistribution(works,p);
			double prob = dist.logProbability(link.strength);
			//System.out.println ("Log probability of " + link.strength + ": " + prob);
			double expected = dist.getNumericalMean();
			//System.out.println("Expected value: " + expected);
			
			link.setLikelihood(prob);
			link.setExpected(expected);
			
/*			
			if (i++ % 1000 == 0)
				System.out.println(i + " likelihoods calculated...");*/
			//i++;
			//if (i > 10) break;
		}

		
		System.out.println("Inserting calculated data into DB");
		db.updateTropeCoOccurrenceLinks(links);
		
		db.close();
		
		System.out.println("Done");
	}

}
