package preprocess;

import java.io.File;
import java.util.HashMap;
import java.util.regex.Pattern;

import preprocess.MatchTVTropesToWikiArticles.FilmFileFilter;
import db.Constants;
import db.TropeDatabase;
import entity.TVTropesFilm;


/**
 * This class is a script that reads through the scraped TVTropes pages and reads them into the DB
 * 
 * It replaces all of ExtractTropesFromTVTArticles and part of MatchTVTropesToWikiArticles
 * 
 * It fills out the following tables:
 * tvt_work
 * trope
 * tvt_work_trope_link
 * @author Sam
 *
 */
public class LoadTVTropesItemsIntoDB
{
	
	//	How to identify html files associated with an article about a film
	public static Pattern filmFilePattern = Pattern.compile("http://tvtropes\\.org/pmwiki/pmwiki\\.php/Film/([A-Za-z0-9_]+)");
	public static String tropeDirName = Constants.tropesDir;

	public static void main() throws Exception
	{
		System.out.println("Extracting works and tropes from downloaded TVTropes.org web pages");
		System.out.println("Only grabbing films for now, but this could change later.");
		System.out.println("Not extracting TVTropes category information, but this is probably also something I'll want to do at some point.");
		TropeDatabase db = new TropeDatabase();
		db.createAllTables();
		
		System.out.println("Identifying TVTropes film articles...");
		File tropeDir = new File(tropeDirName);
		HashMap<String,HashMap<Integer,TVTropesFilm>> tvtFilms = new HashMap<String,HashMap<Integer,TVTropesFilm>>();
		HashMap<String,Boolean> tropeRedirects = new HashMap<String,Boolean>();

		String[] typeAndTitle = new String[2];
		int nullTitleCount = 0;
		int count = 0;
		int redirectCount = 0;
		int dupCount =0;
		int dupTitleCount =0;
		
		for (File tropeFile : tropeDir.listFiles(new FilmFileFilter()))
		{
			
			TVTropesFilm film = new TVTropesFilm(tropeFile);
			
			if (film.valid)
			{
				redirectCount += (film.isRedirect ? 1:0);
				
				//We're okay with duplicate titles
				if (!tvtFilms.containsKey(film.titleC))
					tvtFilms.put(film.titleC, new HashMap<Integer,TVTropesFilm>());
				else
					dupTitleCount++;
					
				
				//...but don't insert a completely identical film into the data structure
				if (!tvtFilms.get(film.titleC).containsKey(film.articleID))
				{
					tvtFilms.get(film.titleC).put(film.articleID,film);
					try
					{
						db.insertTVTWork(film);
					}
					catch(Exception ex)
					{
						System.err.println(film);
						ex.printStackTrace();
					}
				}
				else
				{
					dupCount++;
				}
	
			}
			else
			{
				nullTitleCount++;
			}


			
			count++;
			if (count % 1000 == 0)
				System.out.println(count + " trope film articles processed...");
			if (debug && count > debugFiles)
				break;
		}
		System.out.println(tvtFilms.size() + " TVTropes film articles identified.");
		System.out.println(nullTitleCount + " instances in which either an error occurred or a title couldn't be found.");
		System.out.println(redirectCount + " instances in which film article was a redirect to a different kind of media");
		System.out.println(dupCount + " instances in which we encountered a title we already knew about.");
		
		
		
		db.close();
		System.out.println("Done!");
	}
	
	/**
	 * Check if a scraped TVTropes file is a film by looking at its URL
	 * @author Sam
	 *
	 */
	static class FilmFileFilter implements FilenameFilter
	{
		
		@Override
		public boolean accept(File dir, String name)
		{
			try
			{
				if (filmFilePattern.matcher(URLDecoder.decode(name,"UTF-8")).matches())
					return true;
				else
					return false;
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				return false;
			}
		}
		
	}
	
	public static String cleanTitle(String title)
	{
		return title.toLowerCase().replaceAll("[^a-z0-9]", "");
	}

}
