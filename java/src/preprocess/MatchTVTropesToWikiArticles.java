package preprocess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import db.Constants;
import db.TropeDatabase;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParser;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParserFactory;
import edu.jhu.nlp.wikipedia.PageCallbackHandler;
import edu.jhu.nlp.wikipedia.WikiPage;
import edu.jhu.nlp.wikipedia.WikiPageIterator;
import edu.jhu.nlp.wikipedia.WikiXMLParser;
import edu.jhu.nlp.wikipedia.WikiXMLParserFactory;
import entity.TVTropesFilm;
import entity.WikiFilm;


/**
 * A script which goes through the scraped TVTropes webpage directory and a 
 * zipped xml Wikipedia dump, and tries to match articles from the two sources
 * 
 * Only matches articles about films, at least initially. 
 * @author Sam
 *
 */
public class MatchTVTropesToWikiArticles
{
	public static boolean debug = false;
	//public static int debugLines = -1;
	public static int debugFiles = 1000;
	
	public static String wikiDumpPath = Constants.wikiDumpFilePath;
	public static String tropeDirName = Constants.tropesDir;
	public static String wikiFilmDir = Constants.wikiFilmDir;
	public static Pattern filmFilePattern = Pattern.compile("http://tvtropes\\.org/pmwiki/pmwiki\\.php/Film/([A-Za-z0-9_]+)");
	
	
	//e.g. <div class='article_title'><h1>Literature: <span itemprop="name">American Psycho</span></h1></div>
	//public static Pattern TVTTitleElementPattern = Pattern.compile("[\\s]+<div class='article_title'><h1>([A-Za-z]+): <span itemprop=\"name\">(.+)</span></h1></div>[\\s]*");
	public static Pattern TVTTitleElementPattern = Pattern.compile("<h1>([A-Za-z ]+): <span itemprop=\"name\">(.+)</span></h1>");
	public static Pattern TVTitleStringPattern = Pattern.compile("([A-Za-z ]+): (.+)");
	
	public static String filmInfoBoxPrefix = "{{Infobox film";
	public static Pattern infoBoxTitlePattern = Pattern.compile("(?:(?:name)|(?:title))[\\s]*=[\\s]*(.+)$",Pattern.CASE_INSENSITIVE|Pattern.MULTILINE);

	public static Pattern infoBoxReleasePattern = Pattern.compile("released[\\s]*=.*([0-9]{4})",Pattern.CASE_INSENSITIVE);
	

	public static void main(String[] args) throws Exception
	{
	
		System.out.println("Matching TVTropes articles to Wikipedia articles (about films)");
		TropeDatabase db = new TropeDatabase();
		db.createTables();


/*		db.close();
		System.exit(0);*/
		System.out.println("Identifying TVTropes film articles...");
		File tropeDir = new File(tropeDirName);
		HashMap<String,HashMap<Integer,TVTropesFilm>> tvtFilms = new HashMap<String,HashMap<Integer,TVTropesFilm>>();
		HashMap<String,Boolean> tropeRedirects = new HashMap<String,Boolean>();

		String[] typeAndTitle = new String[2];
		int nullTitleCount = 0;
		int count = 0;
		int redirectCount = 0;
		int dupCount =0;
		int dupTitleCount =0;
		for (File tropeFile : tropeDir.listFiles(new FilmFileFilter()))
		{
			TVTropesFilm film = new TVTropesFilm(tropeFile);
			
			
			if (film.valid)
			{
				redirectCount += (film.isRedirect ? 1:0);
				
				//We're okay with duplicate titles
				if (!tvtFilms.containsKey(film.titleC))
					tvtFilms.put(film.titleC, new HashMap<Integer,TVTropesFilm>());
				else
					dupTitleCount++;
					
				
				//...but don't insert a completely identical film into the data structure
				if (!tvtFilms.get(film.titleC).containsKey(film.articleID))
				{
					tvtFilms.get(film.titleC).put(film.articleID,film);
					try
					{
						db.insertTVTWork(film);
					}
					catch(Exception ex)
					{
						System.err.println(film);
						ex.printStackTrace();
					}
				}
				else
				{
					dupCount++;
				}
	
			}
			else
			{
				nullTitleCount++;
			}


			
			count++;
			if (count % 1000 == 0)
				System.out.println(count + " trope film articles processed...");
			if (debug && count > debugFiles)
				break;
		}
		System.out.println(tvtFilms.size() + " TVTropes film articles identified.");
		System.out.println(nullTitleCount + " instances in which either an error occurred or a title couldn't be found.");
		System.out.println(redirectCount + " instances in which film article was a redirect to a different kind of media");
		System.out.println(dupCount + " instances in which we encountered a title we already knew about.");


		
		System.out.println("Seeking matches in Wikipedia dump " + wikiDumpPath);
		WikiXMLParser wxsp = WikiXMLParserFactory.getSAXParser(wikiDumpPath);
		
		
		WikiPageHandler handler = new WikiPageHandler(tvtFilms, db);
		
		try 
		{
		    wxsp.setPageCallback(handler); 
		    wxsp.parse();
		}
		catch(Exception e) 
		{
		    e.printStackTrace();
		}
		
		db.close();
		
		System.out.println("Done searching for matching Wiki articles.");
		System.out.println(handler.summary());
		
		System.out.println("Done!");
	}
	
	/**
	 * Look at an HTML file for a TVTropes film article and extract the title of 
	 * the film
	 * @param tropeFile
	 * @param typeAndTitle 
	 * @return
	 */
	private static void extractFileTitleFromTropeFile(File tropeFile, String[] typeAndTitle)
	{
		typeAndTitle[0] = null;
		typeAndTitle[1] = null;
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(tropeFile));
			String line = null;
			Matcher matcher = null;
			int index = -1;
			while ((line = reader.readLine()) != null)
			{
				matcher = TVTTitleElementPattern.matcher(line);
				if ( matcher.find())
				{
					reader.close();
					typeAndTitle[0] = matcher.group(1);
					typeAndTitle[1] = matcher.group(2);
					return;
				}
			}
			reader.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		
	}

	/**
	 * Handle a Wikipedia page pulled out of the XML dump
	 * @author Sam
	 *
	 */
	static class WikiPageHandler implements PageCallbackHandler
	{
		int pageCount=0;
		int filmCount=0;
		int noTitleCount=0;
		int noReleaseDateCount=0;
		
		int titleMatchCount=0;
		int strongMatchCount=0;
		int weakMatchCount=0;
		int noMatchCount=0;
		
		int ambiguousStrongMatchCount =0;
		int ambiguousWeakMatchCount =0;
		
		
		HashMap<String,HashMap<Integer,TVTropesFilm>> TVTropesFilms;
		WikiFilm wikiFilm;

		TropeDatabase db;
		String title;
		String titleC;
		Matcher tMatcher;
		Matcher dMatcher;
		
		public WikiPageHandler(HashMap<String,HashMap<Integer,TVTropesFilm>> films,  TropeDatabase db)
		{

			TVTropesFilms = films;
			this.db = db;
			title = null;
			titleC = null;
			
		}
		
		//This is run on every full wikipedia article that is pulled out of the XML
		//This has gotten a little convoluted. Basically we do a basic title match first, and then
		//try to resolve any ambiguity by looking at dates and date ranges. 
		public void process(WikiPage page)
		{
			pageCount++;
			
			
			
			//if the article has an infobox, and the infobox labels the article as being about a film
			if (page.getInfoBox() != null && page.getInfoBox().dumpRaw().startsWith(filmInfoBoxPrefix))
			{
				try
				{
					filmCount++;
					
					//Try to find the title of the film
					tMatcher = infoBoxTitlePattern.matcher(page.getInfoBox().dumpRaw());
					
					//Only do any matching/insertion if we can find a title. Otherwise, we have no way of doing anything
					//with this article
					if (tMatcher.find())
					{
						
						title = tMatcher.group(1);
						titleC = cleanTitle(title);
						wikiFilm = new WikiFilm(Long.parseLong(page.getID()),title);
						
						//Try to find the release date of the film
						dMatcher =  infoBoxReleasePattern.matcher(page.getInfoBox().dumpRaw());
						if (dMatcher.find())
						{
							wikiFilm.setReleaseYear(Integer.parseInt(dMatcher.group(1)));
						}

						
						
						//Try to match this article against a TVTropes article among those we discovered earlier
						//Rules:
						//If the dates and titles match, that's a strong match
						//If the titles but not the dates match, that's not a match
						//If the titles match, but either or both are missing date information, that's a weak match
						//If the titles don't match, that's not a match
						if (TVTropesFilms.containsKey(titleC))
						{
							titleMatchCount++;
							//matchingFilmCount++;
							//writeToFile(wikiFilmDir+title+".txt",page.getText());
							//TVTropesFilm tFilm = TVTropesFilms.get(titleC);
							//db.insertWork(title,tFilm.title,titleLC.replace(" ", "_").replaceAll("[^A-Za-z0-9_]", "")+".txt",tFilm.isRedirect);
							
							int mStrength =0; //the strength of the current best match
							TVTropesFilm mFilm =null;
							int cStrength =0; //the strength of the match we just discovered
							int aStrength =0; //the strength at which we find an ambiguous match
							
							for (Integer id : TVTropesFilms.get(titleC).keySet())
							{
								TVTropesFilm tvtFilm = TVTropesFilms.get(titleC).get(id);
								//If dates both exist
								if (tvtFilm.minYear != null && wikiFilm.releaseYear != null)
								{
									//if dates match
									if (wikiFilm.releaseYear <= tvtFilm.maxYear && wikiFilm.releaseYear >=tvtFilm.minYear)
									{
										cStrength = 2; //strong match
									}
									//if they don't
									else
									{
										cStrength = 0;//not a match
									}
								}
								//if one or both dates are null
								else
								{
									cStrength =1; //weak match
								}
								
								//If we find a better match than what we had, switch the new one in 
								if (cStrength > mStrength)
								{
									mStrength = cStrength;
									mFilm = tvtFilm;
									
								}
								//If we find a worse match, ignore it
								else if (cStrength < mStrength)
								{
									//Do nothing
								}
								//If we find an equally good match, that's a problem...
								else 
								{
									aStrength = cStrength;
								}
							}
							
							//if we found an unambiguous match, write the wiki film to a file and to the database,
							//and write a match link to the DB. Also remove matching tvt film from HashMap so we don't
							//match it again
							if (mFilm != null && aStrength == 0)
							{
								String filePrefix = titleC+"_"+wikiFilm.releaseYear+"_"+wikiFilm.wikiID;
								String fileName = filePrefix+".txt";
								wikiFilm.setFileName(fileName);
								wikiFilm.setUrlTitle(page.getTitle());
								
								//Write wiki film to file
								writeToFile(wikiFilmDir+fileName,page.getWikiText());
								
	
								//write wiki film to database
								db.insertWikiWork(wikiFilm);
								
								//write match link to database
								db.insertWikiTVTropesLink(wikiFilm.getWikiID(),mFilm.articleID,mStrength);
								
								//remove tvtfilm from tvtropes datastructure so we don't match it again
								TVTropesFilms.get(titleC).remove(mFilm);
								
								//Count what kind of match this was
								if (mStrength == 2)
									strongMatchCount++;
								else if (mStrength == 1)
									weakMatchCount++;
								
							}
							//if we found an ambiguous match, add to the appropriate count and insert the wiki film into the db and onto the disk
							else if (aStrength >0)
							{
								String fileName = titleC+"_"+wikiFilm.releaseYear+"_"+wikiFilm.wikiID+".txt";
								wikiFilm.setFileName(fileName);
								
								//Write wiki film to file
								writeToFile(wikiFilmDir+fileName,page.getWikiText());
								
								//Write to file
								db.insertWikiWork(wikiFilm);
								
								if (aStrength == 2)
									ambiguousStrongMatchCount++;
								else if (aStrength == 1)
									ambiguousWeakMatchCount++;
								
							}
							else if (mFilm == null)
							{
								noMatchCount++;
							}
						}
						else
						{
							noMatchCount++;
						}
						
					
					
						
					}
					else
					{
						System.out.println("No title found...");
						System.out.println(page.getInfoBox().dumpRaw());
						noTitleCount++;
					}
					

					
				}
				catch (Exception ex)
				{
					System.out.println(page.getInfoBox().dumpRaw());
					ex.printStackTrace();
				}
				
			}
			if (pageCount % 10000 == 0)
			{
				System.out.println(this.summary());
				System.out.println();
			}
		}
		
		public static void writeToFile(String filePath, String text)
		{
			try
			{
				File newFile = new File(filePath);
				if (!newFile.exists())
					newFile.createNewFile();
				FileUtils.writeStringToFile(newFile, text);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		public String summary()
		{
		
			return pageCount+" Wikipedia pages read.\n"+
		 filmCount+" film articles found.\n"+
		 noTitleCount+" film articles found for which no title could be extracted from infobox.\n"+
		 noReleaseDateCount+" film articles found for which no release data could be extracted from infobox\n"+
		 titleMatchCount+" preliminary title matches found\n"+
		 strongMatchCount+" unambiguous strong matches found\n"+
		 weakMatchCount+" unambiguous weak matches found\n"+
		 noMatchCount+" wiki articles found for which a matching TVTropes article could not be found\n"+
		 ambiguousStrongMatchCount +" bad ambiguous strong matches found\n"+
		 ambiguousWeakMatchCount +" bad ambiguous weak matches found";
	
		}
		
	}
	
	/**
	 * Check if a scraped TVTropes file is a film by looking at its URL
	 * @author Sam
	 *
	 */
	static class FilmFileFilter implements FilenameFilter
	{
		
		@Override
		public boolean accept(File dir, String name)
		{
			try
			{
				if (filmFilePattern.matcher(URLDecoder.decode(name,"UTF-8")).matches())
					return true;
				else
					return false;
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				return false;
			}
		}
		
	}
	
	public static String cleanTitle(String title)
	{
		return title.toLowerCase().replaceAll("[^a-z0-9]", "");
	}
	

}
