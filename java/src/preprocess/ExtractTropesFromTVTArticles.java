package preprocess;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import db.TropeDatabase;
import db.Constants;
import entity.TVTropesFilm;
import entity.Trope;

/**
 * Go through the TVTropes articles in the DB, and extract tropes from them
 * @author Sam
 *
 */
public class ExtractTropesFromTVTArticles
{
	public static HashMap<String,LinkedList<String>> supFileIndex;
	//public static Pattern tvtPattern = Pattern.compile("pmwiki\\.php%2F([^%]+)%2F(.+)$");
	public static Pattern supPattern = Pattern.compile("pmwiki\\.php%2F([^%]+)%2F(tropes[A-Z]to[A-Z])$",Pattern.CASE_INSENSITIVE);
	public static Pattern tvtPattern = Pattern.compile("pmwiki\\.php%2F([^%]+)%2F(.+)$",Pattern.CASE_INSENSITIVE);
	public static Pattern tropeHrefPattern = Pattern.compile("/Main/.+$");

	//public static Pattern supPattern = Pattern.compile("tropes[A-Z]to[A-Z]",Pattern.CASE_INSENSITIVE);

	public static int errMax = 5000000;
	public static int countMax = 5000000;
	public static boolean debug = true;
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Extracting tropes from TVTropes films for which we were able to find matching Wikipedia articles...");
		TropeDatabase db = new TropeDatabase();

		//Read all tvtropes films from database
		ArrayList<TVTropesFilm> films = db.getAllMatchedTVTFilms();
		
		//Index all tvtropes files by the First part of their filenames (a little annoying that I have to do this, but whatever)
		supFileIndex = collectAllSupplementalFiles(Constants.tropesDir);
		int icount = 0;
		int pcount = 0;
		int nCount =0;
		//For each one
		HashMap<Integer,Trope> tropes = new HashMap<Integer, Trope>();
		
		int errCount = 0;
		for (TVTropesFilm film: films)
		{
			//Figure out if it has a supplemental page somewhere, and if so add that to the supplemental file table
			LinkedList<String> supFiles = findSupplementalFiles(film);
			for (String supFile : supFiles)
			{
				db.addSupplementalTVTFile(film.articleID,supFile);
			}
			
			
			//Go through either all the supplements or the main article page if it has none
			if (supFiles.isEmpty())
				supFiles.addFirst(film.fileName);
			
			//if (debug) System.out.println(film.fileName);
			
			//Add all tropes found to a list
			for (String fileName : supFiles)
			{
				Document doc = Jsoup.parse(new File(Constants.tropesDir+fileName), "UTF-8");
				
				Element body = doc.getElementsByAttributeValue("itemprop", "articlebody").first();
								
				//Get all <li> items that contain at least one <a> that consists of a link to a trope page

				Elements lis = body.getElementsByTag("li");
				try
				{
				
				
					for (Element li: lis)
					{
						//if (debug) System.out.println(li);
						
						Element link = li.getElementsByTag("a").first();
						
						if (link != null)
						{
							String href = link.attr("href");
							Matcher matcher = tropeHrefPattern.matcher(link.attr("href"));
							if (matcher.find())
							{
								try
								{	
									//skip any trope link for which there does not actually exist a page
									if ("createlink".equals(link.className()))
										continue;
									
/*									if (debug) System.out.println("Is a valid trope link!");
*/									String anchorText = li.ownText().replaceAll("\\P{Print}", "");;
									
									//Open up the file for the trope and extract its ID
									try
									{
										
										String tropeFile = URLEncoder.encode(href,"UTF-8");
										tropeFile = tropeFile.replace("www.", "");
										tropeFile = tropeFile.replace("devlive.", "");
										Trope trope = extractTropeFromFile(tropeFile);
										
										if (trope != null)
										{
											//Add the trope to the trope table if we don't already know about it
											tropes.put(trope.ID,trope);
											
											//Add the trope and anchor text from the film page to the worktropelink table
											db.insertWorkTropeLink(film.articleID,trope.ID,anchorText);
											icount++;
											if (icount % 1000 == 0)
												System.out.println(icount + " trope instances inserted into DB...");
										}
										else
											nCount++;
										
	
									}
									catch (Exception ex)
									{
										System.err.println("Inner");
										//System.err.println(film.fileName);
										System.err.println(shorten(href));
										////System.err.println(li.ownText());
										//ex.printStackTrace();
										throw ex;
										//continue;
										
									}
								}
								catch (Exception ex)
								{
									System.err.println("Middle");
									//System.err.println(film.fileName);
									System.err.println(shorten(li));
									ex.printStackTrace();
									throw ex;
									//return;
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					System.err.println("Outer");
					System.err.println(film.fileName);
					ex.printStackTrace();
					//System.err.println(body.outerHtml());
					errCount++;
					if (debug && errCount >= errMax)
						throw ex;
					else
						continue;
				}
			
			}
			
			pcount++;
			if (debug && pcount >= countMax)
				break;
		}
		System.out.println(pcount + " articles mined...");
		System.out.println("Finished inserting trope instances. " + icount + " inserted total");
		System.out.println(nCount + " null tropes encountered");
		System.out.println("Inserting tropes into DB");
		for (Integer id : tropes.keySet())
		{
			db.insertTrope(tropes.get(id));
		}
		
		db.close();
		
		System.out.println("Done! " + tropes.size() + " tropes inserted.");
	}

	private static HashMap<String,Trope> tropeCache = new HashMap<String, Trope>();
	
	private static Trope extractTropeFromFile(String fileName) throws Exception
	{

		if (tropeCache.containsKey(fileName))
			return tropeCache.get(fileName);
		else
		{
			
			Document doc;
			try
			{
				doc = Jsoup.parse(new File(Constants.tropesDir+fileName), "UTF-8");
			}
			catch (Exception ex)
			{
				doc = Jsoup.parse(new File(Constants.tropesDir+fileName.replace("-", "")), "UTF-8");

			}
			
			String idString = doc.getElementById("article_id").attr("value");
			String title = doc.getElementsByClass("article_title").first().text().replaceAll("\\P{Print}", "");
			
			if (!idString.isEmpty() && !title.isEmpty())
			{
				Trope trope = new Trope(title,Integer.parseInt(idString),fileName);
				tropeCache.put(fileName, trope);
				return trope; 
			}
			else
			{
				tropeCache.put(fileName, null);
				return null;
			}
		}

		
	}


	/**
	 * Index all tvtropes files by the First part of their filenames so we can look for supplemental files for the files we are interested in
	 * @param tropesDir
	 * @return
	 */

	private static HashMap<String, LinkedList<String>> collectAllSupplementalFiles(String tropesDirName)
	{
		System.out.println("Indexing TVTropes files by the First part of their filename/URL");
		HashMap<String, LinkedList<String>> rMap = new HashMap<String, LinkedList<String>>();
		File tropeDir = new File(tropesDirName); 
		Matcher matcher;
		for (String fileName : tropeDir.list())
		{
			matcher = supPattern.matcher(fileName);
			if (matcher.find())
			{
				
				if (!rMap.containsKey(matcher.group(1)))
					rMap.put(matcher.group(1),new LinkedList<String>());
				rMap.get(matcher.group(1)).add(fileName);
			}
				
		}
		
		System.out.println("Done. " + rMap.size() + " distinct First-parts found.");
		return rMap;
	}


	/**
	 * Find supplemental files for this film.
	 * 
	 * e.g.  
	 * http://tvtropes.org/pmwiki/pmwiki.php/Literature/TheLordOfTheRings
	 * to
	 * http://tvtropes.org/pmwiki/pmwiki.php/TheLordOfTheRings/TropesAToC
	 * 
	 * Filenames are encoded as e.g.
	 * http%3A%2F%2Ftvtropes.org%2Fpmwiki%2Fpmwiki.php%2FLiterature%2FTheLordOfTheRings
	 * and
	 * http%3A%2F%2Ftvtropes.org%2Fpmwiki%2Fpmwiki.php%2FTheLordOfTheRings%2FTropesAToC
	 * @param film
	 * @return
	 */
	private static LinkedList<String> findSupplementalFiles(TVTropesFilm film) throws Exception
	{
		try
		{
			Matcher matcher = tvtPattern.matcher(film.fileName);
			
			matcher.find();
			if (supFileIndex.containsKey(matcher.group(2)))
				return supFileIndex.get(matcher.group(2));
			else
				return new LinkedList<String>();
		}
		catch (Exception ex)
		{
			System.err.println(film.fileName);
			ex.printStackTrace();
			throw ex;
		}
	}
	
	public static String shorten(Object obj, int size)
	{
		String str = obj.toString();
		if (str == null || str.isEmpty() || size > str.length())
			return str;
		else
		{
			return str.substring(0,size/2-5) + " <<...>> " + str.substring((str.length()-size/2+5));
		}
					
	}
	
	public static String shorten(Object str)
	{
		return shorten(str,200);
					
	}
}
