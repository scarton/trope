package preprocess;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import db.TropeDatabase;
import edu.jhu.nlp.wikipedia.WikiTextParser;
import entity.WikiFilm;
import db.Constants;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParser;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParserFactory;

public class GenerateWikiFeatures
{
	public static void main(String[] args) throws Exception
	{
		TropeDatabase db = new TropeDatabase();

		ArrayList<WikiFilm> wikiFilms = db.getAllWikiFilms();
		
		//For each wikipedia article, generate a feature representation
		MediaWikiParserFactory  factory = new MediaWikiParserFactory();
		MediaWikiParser parser = factory.createParser();
		
		
		for (WikiFilm film : wikiFilms)
		{
			String wikiText = FileUtils.readFileToString(new File(Constants.wikiFilmDir+film.fileName));
			ParsedPage page = parser.parse(wikiText);
		}
		
		
		
		db.close();
	}

}
