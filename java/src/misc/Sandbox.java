package misc;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;

import preprocess.ExtractTropesFromTVTArticles;
import preprocess.MatchTVTropesToWikiArticles;

public class Sandbox
{

	public static void main(String[] args) throws Exception
	{
		System.out.println(URLEncoder.encode("http://tvtropes.org/pmwiki/pmwiki.php/Film/TwoThousandOneASpaceOdyssey", "UTF-8"));
		
		System.out.println(URLEncoder.encode("O_Brother,_Where_Art_Thou?", "UTF8"));
		System.out.println(URLDecoder.decode(URLEncoder.encode("O_Brother,_Where_Art_Thou?", "UTF8"),"UTF8"));

		System.out.println(URLDecoder.decode("O_Brother,_Where_Art_Thou%3F","UTF8"));

		String line = "http%3A%2F%2Ftvtropes.org%2Fpmwiki%2Fpmwiki.php%2FFilm%2FAlien";
		Matcher matcher = ExtractTropesFromTVTArticles.tvtPattern.matcher(line);
		
		try
		{
		if (matcher.find())
		{
			System.out.println("<"+matcher.group(0)+">");
			System.out.println("<"+matcher.group(1)+">");
			System.out.println("<"+matcher.group(2)+">");
		}
		else
			System.out.println("No match found...");
		}
		catch (Exception ex)
		{
			
		}
		
		
/*		String str1 = "http://tvtropes.org/pmwiki/pmwiki.php/Literature/TheLordOfTheRings";
		System.out.println(URLEncoder.encode(str1,"UTF-8"));
		String str2 = "http://tvtropes.org/pmwiki/pmwiki.php/TheLordOfTheRings/TropesAToC";
		System.out.println(URLEncoder.encode(str2,"UTF-8"));*/
		
		

		
		
	}

}
