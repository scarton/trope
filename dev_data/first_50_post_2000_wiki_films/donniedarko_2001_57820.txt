{{Infobox film
| name           = Donnie Darko
| image          = Donnie Darko poster.jpg
| image_size     =
| alt            = A collage of faces, in the shape of a head with rabbit ears.
| caption        = Theatrical release poster
| director       = [[Richard Kelly (director)|Richard Kelly]]
| producer       = Sean McKittrick<br />[[Nancy Juvonen]]<br />Adam Fields
| writer         = Richard Kelly
| starring       = [[Jake Gyllenhaal]]<br />[[Jena Malone]]<br />[[Drew Barrymore]]<br />[[Maggie Gyllenhaal]]<br />[[Mary McDonnell]]<br/>[[Katharine Ross]]<br />[[Patrick Swayze]]<br />[[Noah Wyle]]
| music          = [[Michael Andrews (musician)|Michael Andrews]]
| cinematography = Steven B. Poster
| editing        = Sam Bauer<br />Eric Strand
| studio         = [[Flower Films]]
| distributor    = Pandora Cinema<br />[[Newmarket Films]]
| released       = {{film date|2001|1|19|[[Sundance Film Festival|Sundance]]|2001|10|28|United States}}
| runtime        = 113 minutes<ref>[http://www.twcondemand.com/movies/hits/movies-a-z/d-f/donnie-darko Time Warner Cable program info for ''Donnie Darko'']</ref><br /> 133 minutes<br /> {{small|([[#Director's cut|Director's cut]])}}<ref>[http://www.movie-censorship.com/report.php?ID=1796 Detailed comparison of theatrical and director's cut versions, from Movie-Censorship.com]</ref>
| country        = United States
| language       = English
| budget         = $3.8 million<ref name="DCDVD">{{cite video | people=Richard Kelly (director) | year=2004 | title=Donnie Darko: The Director's Cut | medium=DVD}}</ref>
| gross          = $7.6 million<ref name="numbers">{{cite web |title=Donnie Darko |url= http://www.the-numbers.com/movies/2001/DARKO.php |publisher=The Numbers |accessdate=2013-06-23}}</ref>
}}
'''''Donnie Darko''''' is a 2001 American [[science fiction]] [[drama film]] written and directed by [[Richard Kelly (director)|Richard Kelly]] and starring [[Jake Gyllenhaal]], [[Drew Barrymore]], [[Patrick Swayze]], [[Maggie Gyllenhaal]], [[Noah Wyle]], [[Jena Malone]], and [[Mary McDonnell]]. The film depicts the adventures of the title character as he seeks the meaning and significance behind his troubling [[Doomsday event|Doomsday]]-related visions.

Budgeted with $4.5 million<ref name="DCDVD" /> and filmed over the course of 28 days, it grossed just under $7.7 million worldwide.<ref name="numbers" /> Since then, the film has received favorable reviews from critics and has developed a large [[cult film|cult following]],<ref>{{cite web |url= http://www.avclub.com/articles/the-new-cult-canon-donnie-darko,2179/|title= The New Cult Canon: Donnie Darko|author=Scott Tobias |date=2008-02-21 |publisher = The Onion | work = [[The A.V. Club]]}}</ref> resulting in the release of a [[director's cut]] on a two-disc special edition [[DVD]] in 2004.<ref name="rottentomatoes">{{Rotten Tomatoes|id=donnie_darko|title=Donnie Darko}}</ref>

==Plot==
On October 2, 1988, Donnie Darko ([[Jake Gyllenhaal]]), a troubled teenager living in [[Middlesex, Virginia]], is awakened and led outside by a figure in a monstrous rabbit costume, who introduces himself as "Frank" and tells him the world will end in 28 days, 6 hours, 42 minutes, and 12 seconds. At dawn, Donnie returns home to find a [[jet engine]] has crashed into his bedroom. His older sister, Elizabeth ([[Maggie Gyllenhaal]]), informs him the [[Federal Aviation Administration|FAA]] investigators do not know where it came from.

Donnie tells his [[psychotherapist]], Dr. Thurman ([[Katharine Ross]]), about his continuing visits from Frank. Acting under Frank's influence, he floods his school by damaging a water main. He also begins dating new student Gretchen Ross ([[Jena Malone]]), who has moved to town with her mother under a new identity to escape her violent stepfather. [[Conservatism in the United States|Conservative]] gym teacher Kitty Farmer ([[Beth Grant]]) blames the flooding on the influence of the short story "[[The Destructors]]", assigned by dedicated [[Modern liberalism in the United States|liberal]] English teacher Karen Pomeroy ([[Drew Barrymore]]), and begins teaching attitude lessons taken from [[motivational speaker]] Jim Cunningham ([[Patrick Swayze]]). Donnie rebels against these motivational lessons, leading to friction between Kitty and Donnie's mother Rose ([[Mary McDonnell]]).

Donnie asks his science teacher, Dr. Kenneth Monnitoff ([[Noah Wyle]]), about [[time travel]] after Frank brings up the topic, and is given the book ''The Philosophy of Time Travel'', written by Roberta Sparrow ([[Patience Cleveland]]), a former science teacher at the school who is now a seemingly senile old woman.  The book explains that time travel is possible when there is a disruption of the time continuum of the Primary Universe.  The disruption creates a fragile Tangent Universe, which can only exist for a few weeks before collapsing into a black hole which will utterly destroy the Primary Universe.  When a Tangent Universe is created, a metal Artifact will mysteriously appear there. It must be returned to the Primary Universe to re-establish the original time construct and dispel the danger of complete destruction.  The roles of the Living Receiver, who receives Fourth Dimension powers to ensure the Artifact is returned so that the Primary Universe continues, as well as the Manipulated Living and Dead are explained.  Those who die in the Tangent Universe (the Manipulated Dead) can time travel to appear in that universe before their deaths.  They must guide the Living Receiver and create an Ensurance Trap, which forces the Living Receiver to use Fourth Dimension powers and return the Artifact to the Primary Universe.  Once time is restored, people will probably have no memory of what happened in the Tangent Universe, though they may have disturbing dreams.  Donnie reads the book but doesn't understand how it applies to him. (The book is available on the DVD of the film and is essential to understanding what happens and why.)

Thurman tells Donnie's parents that he is detached from reality, and that his visions of Frank are "daylight hallucinations", symptomatic of paranoid [[schizophrenia]]. Donnie disrupts a speech being given by Jim Cunningham by insulting him in front of the student body, then burns down Cunningham's house on instructions from Frank. When police find evidence of a [[child pornography]] operation in the house's remains, Cunningham is arrested. During a [[hypnotherapy]] session, Donnie confesses his crimes to Dr. Thurman and says that Frank will soon kill someone.

Rose agrees to replace Kitty as chaperone for her daughter Samantha's ([[Daveigh Chase]]) dance troupe in Los Angeles, so Kitty can testify in Cunningham's defense; with her husband Eddie ([[Holmes Osborne]]) in New York on business, her older children are home alone.

Donnie and Elizabeth take the opportunity to throw a Halloween party to celebrate her acceptance to [[Harvard University|Harvard]]. Gretchen arrives, distraught that her mother has disappeared. Realizing that only hours remain before Frank's prophesied end of the world, Donnie takes Gretchen and two friends to seek Roberta Sparrow at her house. They are attacked by two school bullies ([[Alex Greenwald]] and [[Seth Rogen]]) who are attempting to rob Sparrow's house, and the fight spills into the street. An oncoming [[Pontiac Trans Am]] car swerves to avoid Sparrow who went for her daily walk to check her mailbox, but runs over Gretchen, killing her. The driver is Elizabeth's boyfriend Frank ([[James Duval]]), wearing the same rabbit costume as the Frank of Donnie's visions. Donnie shoots him in his eye with his father's gun, killing him.

As a [[vortex]] forms in dark clouds above his house, Donnie drives into the hills and watches as an airplane descends from above. The plane, carrying Rose and the dance troupe, is wrenched violently as one of its engines detaches and falls into the vortex. Events of the previous 28 days recapitulate in reverse order and action, propelled by Donnie's powers, until Donnie finds himself in bed in the early hours of October 2. The Ensurance Trap has worked. He sits laughing uncontrollably, knowing that his fate is to die - so that his mother, sister, Gretchen and Frank may live and the black hole will be averted.  Suddenly, the jet engine (Artifact) crashes through his room, killing him - as it should have done and now does in the Primary Universe. Others with whom Donnie had interacted in the 28 Tangent World days awaken, some looking disturbed. Gretchen rides by Donnie's house and learns of his death from a neighborhood boy ([[Scotty Leavenworth]]), but says she did not know him. Gretchen and Rose exchange a glance and wave as if they know one another, but cannot remember from where.

==Cast==
{{Div col}}
* [[Jake Gyllenhaal]] as Donnie Darko
* [[Jena Malone]] as Gretchen Ross
* [[Mary McDonnell]] as Rose Darko
* [[Holmes Osborne]] as Eddie Darko
* [[Katharine Ross]] as Dr. Lilian Thurman
* [[Maggie Gyllenhaal]] as Elizabeth Darko
* [[Daveigh Chase]] as Samantha Darko
* [[James Duval]] as Frank Anderson
* [[Drew Barrymore]] as Karen Pomeroy
* [[Patrick Swayze]] as Jim Cunningham
* [[Noah Wyle]] as Dr. Kenneth Monnitoff
* [[Beth Grant]] as Kitty Farmer
* [[Stuart Stone]] as Ronald Fisher
* [[Gary Lundy]] as Sean Smith
* [[Alex Greenwald]] as Seth Devlin
* [[Seth Rogen]] as Ricky Danforth
* [[Patience Cleveland]] as Roberta Sparrow / "Grandma Death"
* [[Jolene Purdy]] as Cherita Chen
* [[Ashley Tisdale]] as Kim
* [[Jerry Trainor]] as Lanky kid
* David St. James as Bob Garland
* [[Scotty Leavenworth]] as David
* [[Fran Kranz]] as Passenger
* [[Jack Salvatore Jr.]] as Larry Riesman
{{Div col end}}

==Production==

===Filming===
''Donnie Darko'' was filmed in 28 days on a budget of $4.5 million.<ref name="DCDVD" />
It almost went straight to home video release but was publicly released by Drew Barrymore's production company, [[Flower Films]].<ref>{{cite news |title = 'Darko' takes a long, strange trip |url= http://www.usatoday.com/life/movies/news/2005-02-14-dvd-donnie-darko_x.htm |publisher = [[USA Today]] |first1=Mike |last1=Snider |date=2005-02-14 |accessdate=2012-08-30}}</ref>

The film was shot in Bixby Knolls Virginia Country Club in Long Beach [[California]], with many of the school sequences shot at [[Loyola High School (Los Angeles, California)|Loyola High School]]. The "Carpathian ridge" scenes were shot on the [[Angeles Crest Highway]].<ref>{{cite video |people=Poster, Steven (Cinematographer) |title=Donnie Darko Production Diary |medium=DVD |publisher=[[20th Century Fox]] |date=2004}}</ref>

===Music===
{{Main|Donnie Darko (soundtrack)}}
In 2003, the piano-driven cover of [[Tears for Fears]]' "[[Mad World]]" featured in the film as part of the end sequence was a hit for composer [[Michael Andrews (musician)|Michael Andrews]] and singer [[Gary Jules]], topping the charts in the United Kingdom and Portugal.<ref name="indiewire">{{cite web|last=Brunett |first=Adam |date=2004-07-22 |website=[[Indie Wire]] |url=http://www.indiewire.com/article/donnie_darko_the_directors_cut_the_strange_afterlife_of_an_indie_cult_film |title=Donnie Darko: The Director's Cut: The Strange Afterlife of an Indie Cult Film |accessdate=2012-08-31}}</ref>

One continuous sequence involving an introduction of Donnie's high school prominently features the song "[[Head over Heels (Tears for Fears song)|Head over Heels]]" by [[Tears for Fears]], Samantha's dance group, "Sparkle Motion", performs with the song "[[Notorious (Duran Duran song)|Notorious]]" by [[Duran Duran]], and "[[Under the Milky Way]]" by [[The Church (band)|The Church]] is played after Donnie and Gretchen emerge from his room during the party. "[[Love Will Tear Us Apart]]" by [[Joy Division]] also appears in the film [[diegesis|diegetically]] during the party and shots of Donnie and Gretchen upstairs. The version included was released in 1995, although the film is set in 1988. The opening sequence is set to "[[The Killing Moon]]" by [[Echo & the Bunnymen]].<ref name="Day">{{cite web|url=http://film.thedigitalfix.com/content/id/12415/donnie-darko-directors-cut.html|title=''Donnie Darko'': Director's Cut|first=Matt|last=Day|date=10 August 2004|publisher=The Digital Fix}}</ref> In the theatrical cut, the song playing during the Halloween party is "Proud to be Loud" by [[Pantera]], a track released on their 1988 album, which would coincide with the time setting of the film. However, the band is credited as "The Dead Green Mummies".

In the re-released [[Director's cut]] version of the film, the music in the opening sequence is replaced by "[[Never Tear Us Apart]]" by [[INXS]]; "[[Under the Milky Way]]" is moved to the scene of Donnie and Eddie driving home from Donnie's meeting with his therapist; and "[[The Killing Moon]]" is played as Gretchen and Donnie return to the party from Donnie's parents' room.<ref name="Day"/>

==Release==
The film had a [[limited release]] in the month following the [[September 11 attacks]]. It was subsequently held back for almost a year for international release.

===Marketing===
''The Donnie Darko Book'', written by Richard Kelly, is a 2003 book about the film. It includes an introduction by Jake Gyllenhaal, the screenplay of the Donnie Darko Director's Cut, an in-depth interview with Kelly, facsimile pages from the ''Philosophy of Time Travel'', photos and drawings from the film, and artwork it inspired. [[National Entertainment Collectibles Association|NECA]] released first a six-inch (15&nbsp;cm) figure of Frank the Bunny and later a foot-tall (30&nbsp;cm) "talking version" of the same figure.

===Home media===
The film was originally released on [[VHS]] and [[DVD]] in March 2002. Strong DVD sales led [[Newmarket Films]] to release a "[[Director's cut|Director's Cut]]" on DVD in 2004. Bob Berney, President of Newmarket Films, described the film as "a runaway hit on DVD," citing United States sales of more than $10 million.

The film was released in the US on [[Blu-ray Disc|Blu-ray]] on February 10, 2009.

The film was released as a 2-disc Blu-ray special edition in the UK on July 19, 2010 by Metrodome Distribution and featuring both Original and Director's Cut. Also including commentaries from director Kelly and actor Gyllenhaal, Kelly and [[Kevin Smith]], and Cast and Crew including Drew Barrymore.

===Director's cut===
The ''Donnie Darko'' [[Director's cut]] was released on May 29, 2004, in [[Seattle, Washington]], at the [[Seattle International Film Festival]] and later in [[New York City]] and [[Los Angeles]] on July 23, 2004. This cut includes twenty minutes of extra footage and an altered soundtrack.

The director's cut DVD was released on February 15, 2005 in single- and double-disc versions, the latter being available in a standard DVD case or in a limited edition that also featured a [[Lenticular lens|lenticular]] slipcase, whose central image alternates between Donnie and Frank depending on the viewing angle. Most additional features are exclusive to the two-DVD set: the director's commentary assisted by [[Kevin Smith]],<ref>{{Cite book | last1 = Commentary with Kevin Smith | title = Donnie Darko Directors Cut | publisher = Faber and Faber | year = 2003 | isbn = 0-571-22124-6 | postscript = <!--None-->}}</ref> excerpts from the storyboard, a 52-minute production diary, "#1 fan video", a "cult following" video interviewing English fans, and the new director's cut trailer. The single-DVD edition was also released as a giveaway with copies of the British ''Sunday Times'' newspaper on February 19, 2006.

The DVD of the Director's Cut includes text of the in-universe fictional book, ''The Philosophy of Time Travel'', written by Roberta Sparrow, which Donnie is given and reads in the film.<ref name="POTT">[http://www.donniedarko.org.uk/philosphy-of-time-travel/ Text of ''The Philosophy of Time Travel'']</ref> The text expands on the philosophical and scientific concepts much of the film's plot revolves around, and has been seen as a way to understand the film better than from its theatrical release.<ref name="Everything">[http://www.salon.com/2004/07/23/darko/ Everything you were afraid to ask about "Donnie Darko", from Salon.com]</ref><ref>[http://www.cinemablend.com/reviews/Donnie-Darko-The-Director-s-Cut-631.html Film review from CinemaBlend.com]</ref><ref>[http://ca.ign.com/articles/2005/01/24/donnie-darko-the-directors-cut Film review from IGN]</ref> As outlined by ''[[Salon (website)|Salon]]''{{'}}s Dan Kois from the book's text, much of the film takes place in an unstable Tangent Universe that is connected to the Primary Universe and a duplicate of it, except for an extra metal vessel known as an Artifact - the plane engine. If the Artifact is not sent to the Primary Universe by the chosen Living Receiver (Donnie) within 28 days, the Primary Universe will be destroyed upon collapse of the Tangent. To aid in this task, the Living Receiver is given super-human abilities such as foresight, physical strength and elemental powers, but at the cost of troubling visions and paranoia, while the Manipulated Living (all who live around the Receiver) support him in unnatural ways, setting up a [[domino effect|domino-like chain of events]] encouraging him to return the Artifact. The Manipulated Dead (those who die within the Tangent Universe, like Frank and Gretchen) are more aware than the Living, having the power to travel through time, and will set an Ensurance Trap, a scenario which leaves the Receiver no choice but to save the Primary Universe.<ref>{{Cite web | url =http://www.salon.com/2004/07/23/darko/ | title = Everything you were afraid to ask about "Donnie Darko" | first = Dan | last = Kois | date = 2004-07-23 | accessdate = 2013-06-19 | publisher = [[Salon (website)|Salon]]}}</ref>

==Reception==

===Box office===
{{Anchor|Box office}}
''Donnie Darko'' had its first screening at the [[Sundance Film Festival]] on January 19, 2001, and debuted in United States theaters in October 2001 to a tepid response. Shown on only 58 screens nationwide, the film grossed $110,494 in its opening weekend.<ref name="BOM">{{cite web|url= http://boxofficemojo.com/movies/?id=donniedarko.htm|title=Donnie Darko (2001)|publisher= IMDB | work = [[Box Office Mojo]]|accessdate=2012-08-31}}</ref> This may have been the result of the movie being released shortly after the [[September 11 attacks]].<ref>{{cite web|url= http://www.cine-vue.com/2010/07/dvd-releases-donnie-darko-2-disc.html|title=Blu-ray Review: 'Donnie Darko: 2 Disc Ultimate Edition' (rerelease) |author=James Davies|publisher=cine-vue.com}}</ref> By the time the film closed in United States theaters on April 11, 2002, it had earned just $517,375.<ref name="numbers" /><ref name="BOM" /> It ultimately grossed $7.6 million worldwide, just enough to recoup its budget.<ref name="numbers" />

Despite its poor box office showing, the film began to attract a devoted fan base. It was originally released on VHS and DVD in March 2002. During this time, the Pioneer Theatre in [[New York City]]'s [[East Village, Manhattan|East Village]] began [[Midnight movie|midnight screenings]] of ''Donnie Darko'' that continued for 28 consecutive months.<ref name="indiewire" />

===Critical===
The film received very positive reviews. [[Rotten Tomatoes]] gives the theatrical version of the film an 85% rating and the director's cut a 91% rating.<ref name="rottentomatoes" /> [[Metacritic]] gives the theatrical version of the film a score of 71 out of 100, based on 21 reviews, which indicates "generally favorable reviews", whereas the director's cut received a much higher score of 88 out of 100, based on 15 reviews, which indicates "universal acclaim".<ref name="MC">{{cite web|work=Metacritic|title=Donnie Darko|url=http://www.metacritic.com/movie/donnie-darko-the-directors-cut}}</ref>

[[Andrew Johnston (critic)|Andrew Johnson]] cited the film in ''[[Us Weekly]]'' as one of the outstanding films at Sundance in 2001, describing it as "a heady blend of science fiction, spirituality, and teen angst."<ref>''Us Weekly'', 2/21/2001, p. 36.</ref> Jean Oppenheimer of ''New Times (LA)'' praised the film, saying, "Like gathering storm clouds, ''Donnie Darko'' creates an atmosphere of eerie calm and mounting menace—[and] stands as one of the most exceptional movies of 2001."<ref>{{cite web|url=http://www.indiewire.com/article/park_city_2001_review_donnie_darko_plays_with_the_time_of_our_lives |title=PARK CITY 2001 REVIEW: Donnie Darko Plays with the Time of Our Lives |author=Andy Bailey |date=2001-01-21 |publisher=[[Indie Wire]] |accessdate=2012-08-31}}</ref> Writing for ABC Australia, Megan Spencer called the movie, "menacing, dreamy, [and] exciting" and noted that "it could take you to a deeply emotional place lying dormant in your soul."<ref>{{cite web|url= http://www.abc.net.au/triplej/review/film/s702145.htm|title=Donnie Darko: triple j film reviews|author=Megan Spencer|date=2002-10-15|publisher=[[Australian Broadcasting Corporation]]}}</ref> [[Roger Ebert]] gave the theatrical version of the film a less than positive review, but later gave a positive review of the director's cut.<ref>{{cite news |title=Donnie Darko: The Director's Cut |url= http://www.rogerebert.com/reviews/donnie-darko-the-directors-cut-2004 |author=[[Roger Ebert]] |accessdate=2012-08-31 |work=Chicago Sun-Times}}</ref>

==Awards and nominations==
{{Anchor|Awards|Accolades}}
* 2001: [[Richard Kelly (director)|Richard Kelly]] won with ''Donnie Darko'' for "Best Screenplay" at the [[Catalonian International Film Festival|Sitges film festival]] and at the [[San Diego]] Film Critics Society. Donnie Darko also won the "Audience Award" for Best Feature at the [[Sweden]] Fantastic Film Festival. The film was nominated for "Best Film" at the Catalonian International Film Festival and for the "Grand Jury Prize" at the [[Sundance Film Festival]]. The film was nominated for three [[2001 Independent Spirit Awards|Independent Spirit Awards]] including Best First Feature, Best First Screenplay and Best Male Lead for Gyllenhaal.
* 2002: ''Donnie Darko'' won the "Special Award" at the [[Academy of Science Fiction, Fantasy and Horror Films]]'s [[28th Saturn Awards]]. The movie also won the "Silver Scream Award" at the [[Amsterdam]] Fantastic Film Festival. Kelly was nominated for "Best First Feature" and "Best First Screenplay" with ''Donnie Darko'', as well as Jake Gyllenhaal being nominated for "Best Male Lead," at the Independent Spirit Awards. The film was also nominated for the "Best Breakthrough Film" at the Online Film Critics Society Awards.
* 2005: ''Donnie Darko'' ranked in the top five on ''[[My Favourite Film]]'', an [[Australia]]n poll conducted by the [[Australian Broadcasting Corporation|ABC]].<ref>{{cite web |url= http://www.abc.net.au/myfavouritefilm/ |title=My Favourite Film |publisher=[[Australian Broadcasting Corporation]] |accessdate=2012-08-31}}</ref>
* 2006: ''Donnie Darko'' ranks #9 in FilmFour's ''[[50 Films to See Before You Die]]''.<ref>{{cite web| url= http://www.brandrepublic.com/article/567497/c4-relaunches-film4-50-films-die-countdown/ |title=C4 relaunches Film4 with '50 films to see before you die' countdown |author=Joanne Oatts |date= 2006-07-03 |publisher=[[Brand Republic]] |accessdate=2012-08-31}}</ref>

; Other awards
* #14 on ''[[Entertainment Weekly]]''{{'}}s list of the 50 Best High School Movies.<ref>{{cite web|url= http://www.listsofbests.com/list/14066-entertainment-weekly-s-50-best-high-school-movies|title=50 Best High School Movies|date=2006-09-15|publisher=[[Entertainment Weekly]]}}</ref>
* #2 in ''[[Empire (magazine)|Empire]]''{{'}}s "50 Greatest Independent Films of All Time" list.<ref>{{cite web|url= http://www.empireonline.com/features/50greatestindependent/2.asp|title=50 Greatest Independent Films of All Time|accessdate=2012-09-30}}</ref>
* #53 in Empire's 500 Greatest Movies of All Time 2008 poll.<ref>{{cite web|url= http://www.empireonline.com/500/88.asp|title=Empire's 500 Greatest Movies Of All Time|accessdate=2012-09-30}}</ref>

==Sequel==
A 2009 [[sequel]], ''[[S. Darko]]'', centers on Sam, Donnie's younger sister. Sam begins to have strange dreams that hint at a major catastrophe. ''Donnie Darko'' creator [[Richard Kelly (director)|Richard Kelly]] has stated that he has no involvement in this sequel, as he does not own the rights to the original.<ref>{{cite web |url= http://www.ign.com/articles/2008/05/13/arcade-fire-open-box|title=Arcade Fire Open Box: Richard Kelly on film score and Darko sequel |author=Chris Tilly |date=2008-05-13 |publisher=[[IGN]]|accessdate=2012-08-31}}</ref> Chase and producer Adam Fields are the only creative links between it and the original film. The sequel received extremely negative reviews.<ref name="rottentomatoes" /><ref>{{cite web |url= http://www.avclub.com/articles/s-darko,27924/|title=S. Darko |author=Josh Modell |date=2009-05-13 |publisher=[[A.V. Club]] |accessdate=2012-08-31}}</ref>

==Adaptations==
[[Marcus Stern (theatre director)|Marcus Stern]], associate director of the [[American Repertory Theater]], directed a stage adaptation of ''Donnie Darko'' at the Zero Arrow Theatre in [[Cambridge, Massachusetts|Cambridge]], [[Massachusetts]], in the fall of 2007. It ran from October 27 until November 18, 2007, with opening night scheduled near Halloween.

An article written by the production drama team stated that the director and production team planned to "embrace the challenge to make the fantastical elements come alive on stage."<ref>{{cite web| url= http://www.americanrepertorytheater.org/inside/articles/articles-vol-6-i1c-end-world| title = Bringing the End of the World to Life| author = Sarah Wallace| publisher = [[American Repertory Theatre]]| date = 2007-11-01}}</ref> In 2004, Stern adapted and directed Kelly's screenplay for a graduate student production at the American Repertory Theatre's [[Institute for Advanced Theater Training]] (I.A.T.T./M.X.A.T.).

==See also==
* [[List of films featuring time loops]]


{{Portal bar|Film|United States}}


==References==
{{Reflist|30em}}

==External links==
{{Wikiquote}}
* {{Official website|http://archive.hi-res.net/donniedarko/}}
* {{IMDb title|0246578|Donnie Darko}}
* {{Rotten-tomatoes|donnie_darko|Donnie Darko}}
* {{metacritic film|donnie-darko|Donnie Darko}}
* {{Rotten-tomatoes|donnie_darko_directors_cut|Donnie Darko: The Director's Cut}}
* {{metacritic film|donnie-darko-the-directors-cut|Donnie Darko: The Director's Cut}}
* {{Allmovie title|237115|Donnie Darko}}
* {{Mojo title|donniedarko|Donnie Darko}}
* {{cite web|url= http://www.salon.com/2004/07/23/darko/|title=Everything you were afraid to ask about "Donnie Darko"|author=Dan Kois|date=2004-07-23|publisher=[[Salon.com]]}}

{{Richard Kelly}}

{{Authority control}}
[[Category:2001 films]]
[[Category:2000s drama films]]
[[Category:2000s fantasy films]]
[[Category:2000s independent films]]
[[Category:2000s psychological thriller films]]
[[Category:American independent films]]
[[Category:American fantasy films]]
[[Category:American films]]
[[Category:Directorial debut films]]
[[Category:English-language films]]
[[Category:Fiction narrated by a dead person]]
[[Category:Films about suburbia]]
[[Category:Film scores by Michael Andrews]]
[[Category:Films directed by Richard Kelly]]
[[Category:Films set in 1988]]
[[Category:Films set in Virginia]]
[[Category:Films shot in California]]
[[Category:Flower Films films]]
[[Category:Time loop films]]
[[Category:Time travel films]]
[[Category:Wormholes in fiction]]
[[Category:Films about Halloween]]
[[Category:Films about death]]
      