{{pp-semi-sock|small=yes}}
{{Infobox film
| name = Max Keeble's Big Move
| image = MaxKeeble.jpg
| caption = Theatrical release poster
| director = [[Tim Hill (director)|Tim Hill]]
| producer = Mike Karz
| writer = David L. Watts<br />[[James Greer]]<br />Jonathan Bernstein<br />Mark Blackwell
| starring = [[Alex D. Linz]]
| music = Michael Wandmacher
| cinematography = [[Arthur Albert]]
| editing = [[Tony Lombardo]]<br>Peck Prior
| studio = [[Walt Disney Pictures]]<br>Karz Entertainment
| distributor = [[Walt Disney Studios Motion Pictures|Buena Vista Pictures]]
| released = {{filmdate|2001|10|5}}
| runtime = 86 minutes
| country = United States
| language = English
| budget = $25 million<ref>{{cite web | url = http://www.boxofficemojo.com/movies/?id=maxkeeblesbigmove.htm| title = Max Keeble's Big Move (2001) | publisher = Box Office Mojo, LLC.| accessdate = 2006-11-24}}</ref>
| gross = $18,634,654
}}

'''''Max Keeble's Big Move''''' is a 2001 American comedy film directed by [[Tim Hill (director)|Tim Hill]], written by David L. Watts, [[James Greer]], Jonathan Bernstein, and Mark Blackwell, and starring [[Alex D. Linz]] as the title character. The film was released in North America on October 5, 2001 by [[Walt Disney Pictures]].

==Plot==
Max Keeble ([[Alex D. Linz]]) is a seventh grade junior high school paperboy who has a huge crush on Jenna (Brooke Anne Smith), the daughter of one of his paper recipients. Max is antagonized by the corrupt megalomaniacal school principal, Elliot T. Jindrake ([[Larry Miller (comedian)|Larry Miller]]), resident bullies Troy McGinty and Dobbs ([[Noel Fisher (actor)|Noel Fisher]] and [[Orlando Brown (actor)|Orlando Brown]]), and the Evil Ice Cream Man ([[Jamie Kennedy]]). Max also learns that an animal shelter he visits is being closed down to build Jindrake's opulent football stadium.

When Max's father, Donald ([[Robert Carradine]]), reveals that he is moving to [[Chicago metropolitan area|Chicago]] for his boss, because he is unable to stand up for himself, Max realizes that he can do whatever he wants to Jindrake, Troy, Dobbs, and the Evil Ice Cream Man, facing no consequences because he will be gone by then. Enlisting his equally socially outcast friends, Robe and Megan ([[Josh Peck]] and [[Zena Grey]]), pranks include traumatizing Troy by playing the main theme song of the fictional children's television show, ''MacGoogle the Highlander Frog'', which frightened him as a child, trapping him in the gym with a MacGoogle suit wearer, instigating a fight between Dobbs and the Evil Ice Cream Man by stealing the coolant coil for the ice cream truck and Dobbs's handheld device, and ruining Jindrake's chances of becoming superintendent to replace the current superintendent, Crazy Legs ([[Clifton Davis]]), by planting animal pheromones within his breath spray, instigating a food fight in the cafeteria, and later by sabotaging his announcements by placing a cardboard cutout of Max pointing at him claiming that he was wearing a thong. 

After his missions are completed, Max ends up ditching Robe and Megan's going away party by accepting an invite to Jenna's milkshake party, causing a falling out. Taking Max's earlier advice to heart, Don announces that he quit his job and started his own business, meaning that Max is not moving after all. Max freaks out at this news, and learns that other students at his school are suffering because of his actions. Max states that no matter who you are, you can always stand up for yourself. Max confronts Jindrake, Troy, and Dobbs one final time, and with the help of other students at his school, Max eventually defeats Troy and Dobbs for good by throwing them into the dumpster and stops Jindrake from demolishing the animal shelter, which later gets him fired for fiddling with the school budget. The film ends when Max rides on his bicycle delivering newspapers around his neighborhood, and the Evil Ice Cream Man starts pursuing him once again.

==Cast==
*[[Alex D. Linz]] as Max Keeble
*[[Josh Peck]] as Robe
*[[Zena Grey]] as Megan
*[[Larry Miller (comedian)|Larry Miller]] as Principal Elliot T. Jindrake
*[[Jamie Kennedy]] as the Evil Ice Cream Man
*[[Noel Fisher (actor)|Noel Fisher]] as Troy McGinty
*[[Orlando Brown (actor)|Orlando Brown]] as Dobbs
*[[Robert Carradine]] as Donald Keeble
*[[Nora Dunn]] as Lily Keeble
*Brooke Anne Smith as Jenna
*[[Justin Berfield]] as Caption writer
*[[Tony Hawk]] as himself (cameo)
*[[Romeo Miller|Lil' Romeo]] as himself (cameo)
*[[Hopsin|Marcus Hopson]] as Pizza parlor guy (cameo)
*[[Clifton Davis]] as Superintendent Bobby "Crazy Legs" Knebworth
*[[Amy Hill]] as Ms. Phyllis Rangoon
*[[Amber Valletta]] as Ms. Dingman
*[[Dennis Haskins]] as Mr. Kohls
*[[Chely Wright]] as Mrs. Styles (Homeroom Teacher)

==Critical reception==
''Max Keeble's Big Move'' was a [[box office bomb]]. [[Rotten Tomatoes]] currently gives the film a 26% "rotten" rating on its site. The consensus states that the film is "fun for kids, but bland and unoriginal for adults."

==References==
{{Reflist}}<!--added above External links/Sources by script-assisted edit-->

==External links==
* {{official website|http://movies.disney.com/max-keebles-big-move}}
* {{imdb title|0273799|Max Keeble's Big Move}}
* {{amg movie|253783|Max Keeble's Big Move}}
* {{rotten-tomatoes|max_keebles_big_move|Max Keeble's Big Move}}
* {{mojo title|maxkeeblesbigmove|Max Keeble's Big Move}}

{{Tim Hill}}

[[Category:American comedy films]]
[[Category:American films]]
[[Category:Walt Disney Pictures films]]
[[Category:Films directed by Tim Hill]]
[[Category:Films set in 2001]]
[[Category:Films about bullying]]
      