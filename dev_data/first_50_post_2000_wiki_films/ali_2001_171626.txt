{{Infobox film
|name           =Ali
|image          =Ali_movie_poster.jpg
|image_size     =
|caption        =Theatrical release poster
|director       =[[Michael Mann (director)|Michael Mann]]
|producer       =Michael Mann<br />[[Jon Peters]]<br />[[James Lassiter]]<br />Paul Ardaji<br />A. Kitman Ho
|screenplay     =Michael Mann<br />[[Eric Roth]]<br />Stephen J. Rivele<br />[[Christopher Wilkinson]]
|story          =[[Gregory Allen Howard]]
|starring=[[Will Smith]]<br />[[Jamie Foxx]]<br />[[Jon Voight]]<br />[[Mario Van Peebles]]<br />[[Ron Silver]]<br />[[Jeffrey Wright (actor)|Jeffrey Wright]]<br />[[Mykelti Williamson]]<!--- per billing block --->
|music          =[[Pieter Bourke]]<br />[[Lisa Gerrard]]
|cinematography =[[Emmanuel Lubezki]]
|editing        =[[William Goldenberg]]<br />Lynzee Klingman<br />[[Stephen E. Rivkin]]<br />Stuart Waks
|distributor    =[[Columbia Pictures]]
|studio         =Peters Entertainment<br />Forward Pass<br />[[Overbrook Entertainment|Overbrook Films]]
|released       ={{Film date|2001|12|25}}
|runtime        =157 minutes
|country        =United States
|language       =English
|budget         =$107 million<ref>{{cite web|url=http://www.boxofficemojo.com/movies/?id=ali.htm|title=Ali (2001) - Box Office Mojo|work=boxofficemojo.com}}</ref>
|gross          =$87.7 million
}}
'''''Ali''''' is a 2001 American [[biographical film|biographical]] [[sports film|sports]] [[drama film]] directed by [[Michael Mann (director)|Michael Mann]]. The film tells the story of [[boxing|the boxer]] [[Muhammad Ali]], played by [[Will Smith]], from 1964–74 featuring his capture of the heavyweight title from [[Sonny Liston]], his conversion to [[Islam]], criticism of the [[Vietnam War]], banishment from boxing, his return to fight [[Joe Frazier]] in 1971, and, lastly, his reclaiming the title from [[George Foreman]]  in [[the Rumble in the Jungle]] fight of 1974. It also discusses the great social and political upheaval in the [[United States]] following the assassinations of [[Malcolm X]] and [[Martin Luther King, Jr.]] during the presidency of [[Lyndon B. Johnson]].

==Plot==
The film begins with Cassius Clay, Jr. ([[Will Smith]]) before his championship debut against then heavyweight champion Sonny Liston. In the pre-fight [[weigh-in]] Clay heavily taunts Liston (such as calling Liston a "big ugly bear"). In the fight Clay is able to dominate the early rounds of the match, but halfway through the fight Clay complains of a burning feeling in his eyes (implying that Liston has tried to cheat) and says he is unable to continue. However, his trainer/manager Angelo Dundee ([[Ron Silver]]) gets him to keep fighting. Once Clay is able to see again he easily dominates the fight and right before round seven Liston quits, therefore making Cassius Clay the second youngest heavyweight champion at the time after Floyd Patterson. Clay spends valued time with [[Malcolm X]] (Peebles) and the two decide to take a trip to Africa.

Clay is then invited to the home of [[Nation of Islam]] leader [[Elijah Muhammad]] where he is granted the name Muhammad Ali due to his status of World Heavyweight Champion. His father, Cassius Clay Sr ([[Giancarlo Esposito]]) disapproves of this. Ali marries Sonji Roi ([[Jada Pinkett Smith]]), an ex-Playboy Bunny, despite her not being Muslim. While at home with his wife and children, Malcolm X is called by the Nation of Islam and has been informed Ali will not go to Africa and Malcolm's suspension has been extended. Muhammad Ali takes the trip to Africa where he finds Malcolm X, but later refuses to speak to him, honoring the wishes of Elijah Muhammad. He is extremely distraught when Malcolm is later assassinated. 

Upon returning to America, Ali defeats Sonny Liston a second time. He and Sonji divorce after she continually objects to certain obligations Muslim women have, notably wearing a [[hijab]]

After being officially called to fight the Vietnam war in the US army, Ali refuses, and is subsequently stripped of his boxing license, passport and title, additionally facing a few years in prison. After a three-year hiatus, his conviction is later overturned.

He marries Belinda Ali ([[Nona Gaye]]), and attempts to regain the Heavyweight Championship against Joe Frazier. Dubbed the [[Fight of the Century]], Frazier wins, giving Ali the first loss of his career. When Frazier loses the championship to George Foreman, Ali makes a decision to fight George Foreman and become the first boxer to win his title a second time. 

Ali goes to Kinshasa, Zaire to face Foreman for the title. While there, Ali meets a woman named Veronica Porsche ([[Michael Michele]]), and has an affair with her.  After reading rumors of his infidelity through newspapers, his wife Belinda travels to Zaire to confront him about this. Ali says he is unsure as to whether he really loves Veronica or not, and just wants to focus on his upcoming title shot.

For a good portion of the fight against Foreman, Ali leans back against the ropes and covers up, letting Foreman wildly throw punches at him. During the fight Muhammad Ali realizes that he has to react sooner or else he will be knocked out or possibly die in the ring. As the rounds go on, Foreman tires himself out and Ali takes advantage. He quickly knocks out the tired Foreman, and the movie ends with Ali regaining the Heavyweight Championship of which he was previously stripped.

==Cast==
* [[Will Smith]] as [[Muhammad Ali|Cassius Clay, Jr. / Cassius X / Muhammad Ali]]
* [[Jamie Foxx]] as [[Drew Bundini Brown]]
* [[Jon Voight]] as [[Howard Cosell]]
* [[Mario Van Peebles]] as [[Malcolm X]]
* [[Ron Silver]] as [[Angelo Dundee]]
* [[Jeffrey Wright (actor)|Jeffrey Wright]] as [[Howard Bingham]]
* [[Mykelti Williamson]] as [[Don King (boxing promoter)|Don King]]
* [[Jada Pinkett Smith]] as Sonji Roi
* [[Nona Gaye]] as Belinda/Khalilah Ali
* [[Michael Michele]] as [[Veronica Porsche Ali|Veronica Porsche]]
* [[Joe Morton]] as Chauncey Eskridge
* [[Paul Rodriguez]] as Dr. [[Ferdie Pacheco]]
* [[Bruce McGill]] as Bradley
* [[Barry Shabaka Henley]] as [[Jabir Herbert Muhammad|Herbert Muhammad]]
* [[Giancarlo Esposito]] as [[Cassius Marcellus Clay, Sr.|Cassius Clay, Sr.]]
* [[Laurence Mason]] as Luis Sarria
* [[LeVar Burton]] as [[Martin Luther King, Jr.]]
* [[Albert Hall (actor)|Albert Hall]] as [[Elijah Muhammad]]
* [[David Cubitt]] as [[Robert Lipsyte]]
* [[Michael Bentt]] as [[Sonny Liston]]
* [[James Toney]] as [[Joe Frazier]]
* [[Charles Shufford]] as [[George Foreman]]
* Robert Sale as [[Jerry Quarry]]
* Candy Ann Brown as Odessa
* David Haines as [[Rahman Ali|Rudy Clay / Rahaman Ali]]
* [[Leon Robinson]] as Brother Joe
* [[Ted Levine]] as Joe Smiley
* David Elliott as [[Sam Cooke]]
* Shari Watson as Singer
* [[Malick Bowens]] as [[Mobutu Sese Seko|Joseph Mobutu]]
* [[Zaa Nkweta]] as Foreman Fight Announcer
* [[Brandon T. Jackson]] as Club Goer (uncredited)
* [[Victoria Dillard]] as Betty (wife of Malcolm X)
{{div col end}}

==Production==
Producer [[Jon Peters]] had developed the film as far back as 1993.<ref>{{cite news|author=Michael Fleming|url=http://variety.com/1993/voices/columns/peters-semel-team-rumored-116252/|title=Peters-Semel team rumored|work=Variety|date=1993-12-01|accessdate=2014-10-14}}</ref> [[Gregory Allen Howard]] wrote the initial draft of the script, which went under the working title ''Power and Grace''. Howard's draft focuses on Ali's life from 12 to 40 years old and his relationship with his father.<ref>{{cite news|author=Brad Schreiber|url=http://variety.com/2002/film/awards/fusing-fact-and-fiction-for-art-s-sake-1117861006/|title=Fusing fact and fiction for art's sake|work=Variety|date=2002-02-19|accessdate=2014-10-14}}</ref> He was replaced by writers Stephen J. Rivele and Chris Wilkinson and by 1998 the biopic was set up at [[Columbia Pictures]] with Will Smith attached to star with the possibility of [[Ron Howard]] directing.<ref>{{cite news|author=Staff|title=McConaughey sees 'Evel'; Horner tune$ in | url = http://variety.com/1998/voices/columns/mcconaughey-sees-evel-horner-tune-in-1117469150|work=Variety|date=1998-03-25|accessdate=2014-10-14}}</ref> During the filming of ''[[Wild Wild West]]'' Smith presented director [[Barry Sonnenfeld]] with the script. Columbia was hoping for filming to start towards the end of 1998,<ref>{{cite news|author=Michael Fleming|url=http://variety.com/1998/film/news/sonnenfeld-smith-might-team-again-on-ali-biopic-1117479505|title=Sonnenfeld, Smith might team again on Ali biopic|work =Variety| date= 1998-08-14|accessdate=2014-10-14}}</ref> but it was pushed back, and Sonnenfeld exited in November 1999. It was speculated the Columbia was hesitant to move forward with Sonnenfeld following the dismal box office performance of ''Wild Wild West''.<ref>{{cite news|author=Michael Fleming|title = Parkerperks 'Women'; Gere turns on Fawcett | url = http://variety.com/1999/voices/columns/parkerperks-women-gere-turns-on-fawcett-1117756626|work=Variety|date=1999-10-14|accessdate=2014-10-14}}</ref> In February 2000 it was announced the [[Michael Mann (director)|Michael Mann]] had taken over as director following his Academy Award nomination for ''[[The Insider (film)|The Insider]]''. With this commitment to ''Ali'', Mann turned down the opportunity to direct early versions of ''[[The Aviator (2004 film)|The Aviator]]'', ''[[Shooter (2007 film)|Shooter]]'' and ''[[Savages (2012 film)|Savages]]'',<ref>{{cite news| author=Michael Fleming|url=http://variety.com/2000/film/news/mann-handling-ali-pic-1117776671|title=Mann handling Ali pic|work=Variety|date=2000-02-22|accessdate=2014-10-14}}</ref> and brought [[Eric Roth]] to co-write the script.<ref>{{cite news|author=Michael Fleming |url=http://variety.com/2000/voices/columns/for-roth-it-s-potter-or-planet-1117783987| title=For Roth, it's 'Potter' or 'Planet'?|work=Variety|date=2000-07-20|accessdate=2014-10-14}}</ref> After years of being attached to the Ali biopic, Smith officially signed on in May 2000 with a $20 million salary.<ref>{{cite news|author=Michael Fleming|url=http://variety.com/2000/film/news/allen-may-be-in-big-trouble-smith-s-ali-1117781696|title=Allen may be in 'Big Trouble'; Smith's Ali|work=Variety|date=2000-05-16|accessdate=2014-10-14}}</ref>

Filming began in Los Angeles on January 11, 2001 on a $105 million budget. Shooting also took place in Chicago, Miami and Mozambique.<ref>{{cite news|author =Cathy Dunkley |url = http://variety.com/2001/film/news/ieg-punches-up-ali-deal-1117798841|title=IEG punches up 'Ali' deal|work=Variety|date=2001-05-08|accessdate=2014-10-14}}</ref>

Smith spent approximately one year learning all aspects of Ali's life. These included boxing training (up to seven hours a day), Islamic studies and dialect training. Smith has said that his portrayal of Ali is his proudest work to date.

One of the selling points of the film is the realism of the fight scenes. Smith worked alongside boxing promoter Guy Sharpe from SharpeShooter Entertainment and his lead fighter Ross Kent to get the majority of his boxing tips for the film. All of the boxers in the film are, in fact, former or current world heavyweight championship caliber boxers. It was quickly decided that 'Hollywood fighting'—passing the fist (or foot) between the camera and the face to create the illusion of a hit—would not be used in favor of actual boxing. The only limitation placed upon the fighters was for Charles Shufford (who plays [[George Foreman]]). He was permitted to hit Smith as hard as he could, so long as he did not actually knock the actor out.

Smith had to gain a significant amount of weight to look the part of Muhammad Ali.<ref>{{cite news| url=http://www.nytimes.com/2001/09/09/movies/the-new-season-film-michael-mann-and-will-smith-in-the-ring-with-ali.html?pagewanted=all | work=New York Times | title=THE NEW SEASON/FILM; Michael Mann and Will Smith in the Ring With Ali | date=2001-09-09 | accessdate=2012-03-10 | first1=Allen | last1=Barra}}</ref>

==Reception==
''Ali'' opened on December 25, 2001 and grossed a total of $14.7 million in 2,446 theaters on its opening weekend. The film went on to gross a total of $87.7 million worldwide. The film holds a 67% "fresh" rating at [[Rotten Tomatoes]]. In spite of generally favorable reviews, the film lost an estimated $63.1 million
The film had generally favorable reviews, with the performances of Smith and Voight being well received by critics in general. [[Roger Ebert]] derided the film with two stars in his review for the ''[[Chicago Sun-Times]]'', and mentioned, "it lacks much of the flash, fire and humor of Muhammad Ali and is shot more in the tone of a eulogy than a celebration".<ref name= "Ebert">{{cite news|last=Ebert|first=Roger|title=Ali|work=|publisher=[[Chicago Sun-Times]]|date=2001-12-25|url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20011225/REVIEWS/112250302/1023|accessdate=2008-01-15}}</ref> In ''[[Variety (magazine)|Variety]]'' magazine, Todd McCarthy wrote, "The director's visual and aural dapplings are strikingly effective at their best, but over the long haul don't represent a satisfactory alternative to in-depth dramatic scenes; one longs, for example, for even one sequence in which Ali and Dundee discuss boxing strategy or assess an opponent", but he did have praise for the performances: "The cast is outstanding, from Smith, who carries the picture with consummate skill, and Voight, who is unrecognizable under all the makeup but nails Cosell's distinctive vocal cadences".<ref name= "McCarthy">{{cite news|last=McCarthy|first=Todd|title=Ali|work=[[Variety (magazine)|Variety]]|date=2001-12-16|url=http://www.variety.com/review/VE1117916611.html?categoryid=31&cs=1&p=0|accessdate=2008-01-15}}</ref> ''[[USA Today]]'' gave the film two and half stars out of four and claimed that, "for many Ali fans, the movie may be good enough, but some perspective is in order. The documentaries ''[[a.k.a. Cassius Clay]]'' and the Oscar-winning ''[[When We Were Kings]]'' cover a lot of the same ground and are consistently more engaging".<ref name= "Clark">{{cite news|last=Clark|first=Mike|title=Despite hype, ''Ali'' isn't the greatest|work= [[USA Today]]|date=2001-12-31|url=http://www.usatoday.com/life/movies/2001-12-24-ali-review.htm|accessdate=2008-01-15}}</ref>

In ''[[The New York Times]]'', Elvis Mitchell proclaimed ''Ali'' to be a "breakthrough" film for Mann, that it was his "first movie with feeling" and that "his overwhelming love of its subject will turn audiences into exuberant, thrilled fight crowds".<ref name= "Mitchell">{{cite news|last=Mitchell|first=Elvis|title=Master of the Boast, King of the Ring, Vision of the Future|work=[[The New York Times]]|date=2001-12-25|url=http://query.nytimes.com/gst/fullpage.html?res=9806E0D71431F936A15751C1A9679C8B63|accessdate=2008-01-15}}</ref> [[J. Hoberman]], in his review for the ''[[Village Voice]]'', felt that the "first half percolates wonderfully — and the first half hour is even better than that. Mann opens with a thrilling montage that, spinning in and out of a nightclub performance by [[Sam Cooke]], contextualizes the hero in his times", concluded that, "Ali's astonishing personality is skillfully evoked but, in the end, remains a mystery".<ref name= "Hoberman">{{cite news | last = Hoberman | first = J | coauthors = | title = Fight Songmkmmmelkkjar;dfiik. fl;ud;ovids | work = | pages = | language = | publisher = [[Village Voice]] | date = December 26, 2001 | url = http://www.villagevoice.com/film/0152,hoberman,31031,20.html | accessdate = 2008-01-15 }}</ref>

===Awards===
{| class="collapsible collapsed" style="width:75%; border:1px solid #cedff2; background:#F5FAFF"
|-
! align="left" | List of awards and nominations
|-
| <!-- PLACE EXTRA AWARDS BELOW IN ALPHABETICAL ORDER-->

{| class="wikitable" border="1" align="center" style="font-size: 90%;"
|-
! Award
! Category
! Name
! Outcome
|-
| rowspan="2"|[[74th Academy Awards]]
| [[Academy Award for Best Actor|Best Actor in a Leading Role]]
| [[Will Smith]]
| {{nom}}
|-
| [[Academy Award for Best Supporting Actor|Best Actor in a Supporting Role]]
| [[Jon Voight]]
| {{nom}}
|-
| rowspan="8"|[[Black Reel Awards of 2001]]
| Best Supporting Actor
| [[Jamie Foxx]]
| {{won}}
|-
| Best Supporting Actress
| [[Nona Gaye]]
| {{won}}
|-
| Best Original Soundtrack
|
| {{won}}
|-
| Best Actor
| Will Smith
| {{won}}
|-
| Best Song From a Film
| [[R. Kelly]] - "[[The World's Greatest (R. Kelly song)|The World's Greatest]]"
| {{won}}
|-
| Best Screenplay, Adapted or Original
| Gregory Allen Howard
| {{won}}
|-
| Best Film
|
| {{won}}
|-
| Best Film Poster
|
| {{won}}
|-
| rowspan="3"|[[7th Critics' Choice Awards|Broadcast Film Critics Association Awards 2001]]
| Best Actor
| Will Smith
| {{nom}}
|-
| Best Supporting Actor
| Jon Voight
| {{nom}}
|-
| Best Picture
|
| {{nom}}
|-
| [[Chicago Film Critics Association Awards 2001]]
| Best Supporting Actor
| Jon Voight
| {{won}}
|-
| [[ESPY Awards]]
| [[Best Sports Movie ESPY Award]]
|
| {{won}}
|-
| rowspan="3"|[[59th Golden Globe Awards]]
| [[Golden Globe Award for Best Original Score|Best Original Score]]
| [[Lisa Gerrard]], [[Pieter Bourke]]
| {{nom}}
|-
| [[Golden Globe Award for Best Actor - Motion Picture Drama|Best Actor: Drama]]
| Will Smith
| {{nom}}
|-
| [[Golden Globe Award for Best Supporting Actor - Motion Picture|Best Supporting Actor]]
| Jon Voight
| {{nom}}
|-
| [[Motion Picture Sound Editors|Golden Reel Awards]]
| Best Sound Editing - Music
| Kenneth Karman, Lisa Jaime, Vicki Hiatt, Stephanie Lowry, Christine H. Luethje
| {{nom}}
|-
| [[Golden Trailer Awards]]
| Best Drama
|
| {{won}}
|-
| [[2002 MTV Movie Awards]]
| Best Male Performance
| Will Smith
| {{won}}
|-
| rowspan="5"|[[NAACP Image Awards]]
| [[NAACP Image Award for Outstanding Motion Picture|Outstanding Motion Picture]]
|
| {{won}}
|-
| rowspan="2"|[[NAACP Image Award for Outstanding Supporting Actor in a Motion Picture|Outstanding Supporting Actor]]
| Jamie Foxx
| {{won}}
|-
| [[Mario Van Peebles]]
| {{nom}}
|-
| [[NAACP Image Award for Outstanding Actor in a Motion Picture|Outstanding Actor]]
| Will Smith
| {{won}}
|-
| [[NAACP Image Award for Outstanding Supporting Actress in a Motion Picture|Outstanding Supporting Actress]]
| [[Jada Pinkett Smith]]
| {{nom}}
|-
| Phoenix Film Critics Society Awards
| Best Editing
| [[William Goldenberg]], Lynzee Klingman, [[Stephen E. Rivkin]], Stuart Waks
| {{won}}
|-
| [[Entertainment Industries Council#PRISM Awards|PRISM Awards]]
| Theatrical Feature Film
|
| {{won}}
|}
<!-- PLACE EXTRA AWARDS ABOVE -->
|}

==Home release==
After the theatrical version (157 min.) was released on [[DVD]], Mann revisited his film, creating a new cut that ran for 165 minutes. Approximately 4 minutes of theatrical footage was removed, while 14 minutes of previously unseen footage was placed back in by Mann.<ref>{{cite web|url=http://www.movie-censorship.com/report.php?ID=1740|title=Ali Comparison|publisher=MovieCensorship.com|accessdate=March 7, 2015}}</ref> The film was released on [[Blu-ray]] in 2012.

==References==
{{reflist|30em}}

==External links==
* {{IMDb title|0248667|Ali}}
* {{Amg movie|249601|Ali}}
* {{mojo title|ali|Ali}}
* {{rotten-tomatoes|ali|Ali}}
* [http://filmforce.ign.com/articles/316/316862p1.html Excerpts from production notes]
* [http://query.nytimes.com/gst/fullpage.html?res=9A01E2D71739F93AA3575AC0A9679C8B63 "Michael Mann and Will Smith in the Ring With Ali"] ''New York Times'' article
* [http://www.cameraguild.com/index.html?magazine/stoo1201a.htm~main_hp An interview] with the film's [[cinematography|cinematographer]] [[Emmanuel Lubezki]]

{{Michael Mann}}
{{Muhammad Ali}}
{{NAACP Image Award for Outstanding Motion Picture}}

{{Authority control}}
{{DEFAULTSORT:Ali}}
[[Category:2000s drama films]]
[[Category:2001 films]]
[[Category:American biographical films]]
[[Category:American drama films]]
[[Category:American films]]
[[Category:Biographical films about sportspeople]]
[[Category:Columbia Pictures films]]
[[Category:English-language films]]
[[Category:Films about Muhammad Ali]]
[[Category:Films about race and ethnicity]]
[[Category:Films directed by Michael Mann]]
[[Category:Films set in Miami, Florida]]
[[Category:Films set in New York City]]
[[Category:Films set in the 1960s]]
[[Category:Films set in the 1970s]]
[[Category:Films set in the Democratic Republic of the Congo]]
[[Category:Overbrook Entertainment films]]
[[Category:Screenplays by Eric Roth]]
[[Category:Sports films based on actual events]]
      