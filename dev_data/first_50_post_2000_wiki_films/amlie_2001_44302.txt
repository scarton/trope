{{redirect|Amelie|the given name|Amélie (given name)|other uses|Amélie (disambiguation)}}
{{Use dmy dates|date=March 2012}}
{{Infobox film
| name           = Amélie
| image          = Amelie poster.jpg
| alt            = 
| caption        = French theatrical poster
| director       = [[Jean-Pierre Jeunet]]
| producer       = {{Plainlist|
* Jean-Marc Deschamps
* Claudie Ossard
}}
| writer         = Guillaume Laurant
| story          = {{Plainlist|
* Guillaume Laurant
* Jean-Pierre Jeunet
}}
| starring       = {{Plainlist|
* [[Audrey Tautou]]
* [[Mathieu Kassovitz]]
}}
| narrator       = [[André Dussollier]]
| music          = [[Yann Tiersen]]
| cinematography = [[Bruno Delbonnel]]
| editing        = Hervé Schneid
| production companies = {{Plainlist|
* [[Canal+]]
* [[France 3 Cinéma]]
* [[UGC (cinema operator)|UGC]]
* [[UGC Fox Distribution]]
}}
| distributor    = UGC-Fox Distribution
| released       = {{Film date|df=y|2001|04|25|France|2001|08|16|Germany}}
| runtime        = 123 minutes<!--Theatrical runtime: 122:43--><ref>{{cite web|title=''AMELIE FROM MONTMARTRE (LE FABULEUX DESTIN D'AMELIE POULAIN)'' (15)|url=http://www.bbfc.co.uk/releases/amelie-montmartre-le-fabuleux-destin-damelie-poulain-2001|work=[[British Board of Film Classification]]|date=2001-07-17|accessdate=2013-04-13}}</ref>
| country        = {{Plainlist|
* France
* Germany
}}
| language       = French
| budget         = $10 million<ref name="mojo" />
| gross          = $173.9 million<ref name="mojo">{{cite web |url= http://www.boxofficemojo.com/movies/?id=amelie.htm |title=Amélie (2001) | work = [[Box Office Mojo]] |publisher= [[IMDb]]  |accessdate=2011-09-25}}</ref>
}}
'''''Amélie''''' ({{lang-fr|'''Le Fabuleux Destin d'Amélie Poulain'''}}; {{IPA-fr|lə.fa.by.lø.dɛs.tɛ̃.da.me.li.puˈlɛ̃}}); ''The Fabulous Destiny of Amélie Poulain'') is a 2001 [[romantic comedy film]] directed by [[Jean-Pierre Jeunet]]. Written by Jeunet with Guillaume Laurant, the film is a whimsical depiction of contemporary Parisian life, set in [[Montmartre]]. It tells the story of a shy waitress, played by [[Audrey Tautou]], who decides to change the lives of those around her for the better, while struggling with her own isolation. The film was a [[International co-production|co-production]] between companies in [[Cinema of France|France]] and [[Cinema of Germany|Germany]]. Grossing over $33 million in limited theatrical release, it is still the highest-grossing French-language film released in the United States.<ref>{{cite web|url=http://www.boxofficemojo.com/genres/chart/?id=foreign.htm&sort=gross&order=DESC&p=.htm |title=Foreign Language Movies at the Box Office |publisher=Box Office Mojo |date= |accessdate=2014-01-25}}</ref>

The film met with critical acclaim and was a major box-office success. ''Amélie'' won Best Film at the [[European Film Awards]]; it also won four [[César Award]]s (including [[César Award for Best Film|Best Film]] and [[César Award for Best Director|Best Director]]), two [[55th British Academy Film Awards|BAFTA Awards]] (including Best Original Screenplay), and was nominated for five [[Academy Award]]s. A [[Broadway theatre|Broadway]] [[Amélie (musical)|adaptation]] is in development.<ref name="broadway" />

==Plot==
[[File:Paris - Café des 2 Moulins - 2004.jpg|thumb|left|Amélie works at the [[Café des 2 Moulins]] in Montmartre]]
Amélie Poulain was raised by eccentric parents who — erroneously believing that she had a heart defect — prevented her from meeting other children. She was [[Homeschooling|home schooled]] by her mother. She developed an active imagination and fantasy life to cope with her loneliness. After her mother is killed in a freak accident, her father's withdrawal from society worsens. Amélie eventually decides to leave home and becomes a waitress at [[Café des 2 Moulins]] in [[Montmartre]], which is staffed and frequented by a collection of eccentrics. Spurning romantic relationships after a few disappointing efforts, she finds contentment in simple pleasures and letting her imagination roam free.

On 31 August 1997, Amélie is startled by the news of the [[death of Diana, Princess of Wales|death of Princess Diana]], causing her to drop a plastic perfume-stopper which in turn dislodges a loose bathroom tile. Behind the tile she finds an old metal box of childhood memorabilia hidden by a boy who lived in her apartment decades earlier. She resolves to track down the boy and return the box to him, and promises herself that if she finds him and it makes him happy, she will devote her life to bringing happiness to others and helping others as much as she can.

She asks Mrs. Wells, the concierge, about the boy. Wells redirects her to the abusive greengrocer, Mr. Collignon, who redirects Amélie to his mother. Mrs. Collignon remembers the name "Dominique Bredoteau", but Amélie has no success finding the owner of the box. Amélie meets her reclusive neighbour, Raymond Dufayel, a man whose bones are as fragile as glass and an artist who repaints ''[[Luncheon of the Boating Party]]'' by [[Pierre-Auguste Renoir]] every year. He remembers the boy also, but correctly recalls the name as "Bretodeau". Amélie quickly finds the man and surreptitiously passes him the box. Moved to tears by the discovery and the memories it holds, Bretodeau resolves to reconcile with his estranged daughter and the grandson he has never met. Amélie happily embarks on her new mission.

Amélie secretly executes complex schemes that affect the lives of those around her. She escorts a blind man to the Métro station, giving him a rich description of the street scenes he passes. She persuades her father to follow his dream of touring the world by stealing his [[garden gnome]] and having a flight attendant friend [[airmail]] pictures of it [[Travelling gnome prank|posing with landmarks from all over the world]]. She kindles a romance between a middle-aged co-worker and one of the customers in the bar. She convinces Mrs. Wells that the husband who abandoned her had sent her a final conciliatory love letter just before his accidental death years before. She avenges Lucien, Mr. Collignon's meek but good-natured assistant (who is the constant target of his abuse), by playing a number of practical jokes on Collignon, leaving him utterly exhausted and his ego deflated, while a delighted Lucien takes charge at the grocery stand.

While she is looking after others, Mr. Dufayel is observing her. He begins a conversation with her about his painting, a replica of ''Luncheon of the Boating Party''. Although he has copied the same painting 20 times, he has never quite captured the look of the girl drinking a glass of water. They discuss the meaning of this character, and over several conversations Amélie begins projecting her loneliness on to the image. Dufayel recognizes this, and uses the girl in the painting to push Amélie to examine her attraction to a quirky young man who collects the discarded photographs of strangers from passport photo booths. When Amélie bumps into the young man a second time, she realizes she is falling in love with him. He accidentally drops a photo album in the street. Amélie retrieves it. She discovers his name is Nino Quincampoix, and she plays a cat-and-mouse game with him around Paris before returning his treasured album anonymously. After orchestrating a proper meeting at the 2 Moulins, she is too shy to approach him and tries to deny her identity. Her co-worker, concerned for Amélie's well-being, screens Nino for her; a café patron's comment about this misleads Amélie to believe she has lost Nino to the co-worker. It takes Dufayel's insight to give her the courage to pursue Nino, resulting in a romantic night together and the beginning of a relationship.

==Cast==
{{div col|2}}
* [[Audrey Tautou]] as Amélie Poulain
** Flora Guiet as young Amélie
* [[Mathieu Kassovitz]] as Nino Quincampoix
** Amaury Babault as young Nino
* [[Rufus (actor)|Rufus]] as Raphaël Poulain, Amélie's father
* [[Serge Merlin]] as Raymond Dufayel, "The Glass Man"
* [[Lorella Cravotta]] as Amandine Poulain, Amélie's mother
* {{Ill|fr|Clotilde Mollet}} as Gina, a fellow waitress
* [[Claire Maurier]] as Suzanne, the owner of Café des deux moulins
* [[Isabelle Nanty]] as Georgette, the resident hypochondriac
* [[Dominique Pinon]] as Joseph
* [[Artus de Penguern]] as Hipolito, the writer
* [[Yolande Moreau]] as Madeleine Wallace (Wells, in English subtitled version)
* [[Urbain Cancelier]] as Collignon, the grocer
* [[Jamel Debbouze]] as Lucien, the grocer's assistant
* [[Maurice Bénichou]] as Dominique Bretodeau
** Kevin Fernandes as young Dominique
* [[Michel Robin]] as Mr. Collignon
* Andrée Damant as Mrs. Collignon
* [[Claude Perron]] as Eva, Nino's colleague
* [[Armelle]] as Philomène, air hostess
* [[Ticky Holgado]] as Man in a photo
* Franck-Olivier Bonnet, Alain Floret, Jean-Pol Brissart, and [[Frédéric Mitterrand]] as Additional voices
{{div col end}}

==Production==
[[File:20051018Épicerie d'Amélie Poulain 2.jpg|thumb|right|Au Marché de la Butte, Rue des Trois Frères, Paris, used as the location of Monsieur Collignon's shop]]
{{Expand section|date=June 2008}}
In his [[Audio commentary|DVD commentary]], Jeunet explains that he originally wrote the role of Amélie for the English actress [[Emily Watson]]; in the original draft, Amélie's father was an Englishman living in London. However, Watson's French was not strong, and when she became unavailable to shoot the film, owing to a conflict with the filming of ''[[Gosford Park]]'', Jeunet rewrote the screenplay for a French actress. [[Audrey Tautou]] was the first actress he auditioned having seen her on the poster for ''[[Venus Beauty Institute]]''.

The movie was filmed mainly in Paris. The [[Café des 2 Moulins]] (15 Rue Lepic, Montmartre, Paris) where Amélie works is a real place.<ref>{{Cite news|title=Amélie: filming locations |work=Movieloci.com |date=23 July 2012 |url=http://www.movieloci.com/281-Amelie-from-Montmartre?snapshot=2794}}</ref>

The filmmakers made use of [[computer-generated imagery]] and a [[digital intermediate]].<ref>[http://www.variety.com/ac2005_article/VR1117915901?nav=lenser&categoryid=1804 ] {{wayback|url=http://www.variety.com/ac2005_article/VR1117915901?nav=lenser&categoryid=1804 |date=20140226153927 |df=y }}</ref> The studio scenes were filmed in the Coloneum Studio in [[Cologne]] (Germany). The film shares many of the themes in the plot with second half of the 1994 film ''[[Chungking Express]]''.<ref>{{cite web|url=http://www.mediacircus.net/amelie.html |title=Amelie Movie Review by Anthony Leong from |publisher=MediaCircus.net |date= |accessdate=2014-01-25}}</ref><ref>{{cite web|url=http://www.michigandaily.com/content/audrey-tautou-and-french-film-amelie-are-pure-movie-magic |title=Audrey Tautou and French film `Amelie' are pure movie magic |publisher=The Michigan Daily |date= |accessdate=2014-01-25}}</ref>

==Release==
The film was released in France, [[Belgium]], and [[Romandy|French-speaking western Switzerland]] in April 2001, with subsequent screenings at various film festivals followed by releases around the world. It received [[limited release]]s in North America, the United Kingdom, and [[Australasia]] later in 2001.

[[Cannes Film Festival]] selector Gilles Jacob described ''Amélie'' as "uninteresting", and therefore it was not screened at the festival, although the version he viewed was an early cut without music. The absence of ''Amélie'' at the festival caused something of a controversy because of the warm welcome by the French media and audience in contrast with the reaction of the selector.<ref>{{cite web|last=Tobias |first=Scott |url=http://www.avclub.com/content/node/22708 |title=Jean-Pierre Jeunet |publisher=The A.V. Club |accessdate=2010-04-28}}</ref>

===Critical response===
The film received critical acclaim. According to [[Rotten Tomatoes]], 89% of critics liked the film, giving it an average grade of 8.1/10,<ref name="rottentomatoes.com">{{cite web|url=http://www.rottentomatoes.com/m/amelie/|title=Amlie|date=2 November 2001|work=rottentomatoes.com}}</ref> while its [[Metacritic]] score of 69 (indicating generally favorable reviews) has unusually high variance including five perfect scores of 100 and four scores of 40 or below.<ref>{{cite web|url=http://www.metacritic.com/movie/amelie |title=Reviews for 'Amélie' |publisher=Metacritic.com |date=2001}}</ref>

Alan Morrison from [[Empire (film magazine)|''Empire'' Online]] gave ''Amélie'' five stars and called it "one of the year’s best, with crossover potential along the lines of ''[[Cyrano de Bergerac (1990 film)|Cyrano De Bergerac]]'' and ''[[Il Postino]]''. Given its quirky heart, it might well surpass them all."<ref>{{cite web|url=http://www.empireonline.com/reviews/ReviewComplete.asp?FID=7284 |title=Empire's Amelie Movie Review |publisher=Empireonline.com |date= |accessdate=2014-01-25}}</ref>

Paul Tatara from CNN Reviewer praised ''Amélie''{{'}}s playful nature. In his review, he wrote, "Its whimsical, free-ranging nature is often enchanting; the first hour, in particular, is brimming with amiable, sardonic laughs."<ref>{{cite news| url=http://archives.cnn.com/2001/SHOWBIZ/Movies/11/07/review.amelie/index.html | work=CNN | title=Review: 'Amelie' is imaginative | date=7 November 2001}}</ref>

The film was attacked by critic Serge Kaganski of ''[[Les Inrockuptibles]]'' for an unrealistic and picturesque vision of a bygone French society with few [[Minority group|ethnic minorities]].<ref>{{cite web|url=http://www.filmlinc.com/film-comment/article/the-amelie-effect |title=The Amélie Effect |publisher=Filmlinc.com |accessdate=2011-09-21}}</ref> Jeunet dismissed the criticism by pointing out that the photo collection contains pictures of people from numerous ethnic backgrounds, and that [[Jamel Debbouze]], who plays Lucien, is of Moroccan descent.

==Awards and nominations== <!-- fix reference in lead section if this section is renamed -->
The film was a critical and box office success, gaining wide play internationally as well. It was nominated for five [[Academy Award]]s:<ref name="Oscars2002">{{cite web|url=http://www.oscars.org/awards/academyawards/legacy/ceremony/74th-winners.html |title=The 74th Academy Awards (2002) Nominees and Winners |accessdate=2011-11-19|work=oscars.org}}</ref>
* [[Academy Award for Best Production Design|Best Art Direction]] – [[Aline Bonetto]] (art director), [[Marie-Laure Valla]] (set decorator) (lost to ''[[Moulin Rouge!]]'')
* [[Academy Award for Best Cinematography|Best Cinematography]] – [[Bruno Delbonnel]] (lost to ''[[The Lord of the Rings: The Fellowship of the Ring]]'')
* [[Academy Award for Best Foreign Language Film|Best Foreign Language Film]] – France (lost to [[No Man's Land (2001 film)|''No Man's Land'']])
* [[Academy Award for Best Writing (Original Screenplay)|Best Original Screenplay]] – Guillaume Laurant, [[Jean-Pierre Jeunet]] (lost to ''[[Gosford Park]]'')
* [[Academy Award for Best Sound|Best Sound]] – [[Vincent Arnardi]], [[Guillaume Leriche]], [[Jean Umansky]] (lost to [[Black Hawk Down (film)|''Black Hawk Down'']])

In 2001, it won several awards at the [[European Film Awards]], including the Best Film award. It also won the [[Toronto International Film Festival#People's Choice Award|People's Choice Award]] at the [[Toronto International Film Festival]] and the [[Crystal Globe]] Award at the [[Karlovy Vary International Film Festival]]. In 2002, in France, it won the [[César Award]] for [[César Award for Best Film|Best Film]], [[César Award for Best Director|Best Director]], [[César Award for Best Music|Best Music]] and [[César Award for Best Production Design|Best Production Design]]. It was also awarded the [[French Syndicate of Cinema Critics]]'s Prix Mélies (Best French Film) in the same year.

The film was selected by ''[[The New York Times]]'' as one of "The Best 1,000 Movies Ever Made".<ref>{{cite news| url=http://www.nytimes.com/ref/movies/1000best.html | work=The New York Times | title=The Best 1,000 Movies Ever Made | date=29 April 2003 | accessdate=2010-04-23}}</ref> The film placed No. 2 in ''[[Empire (film magazine)|Empire]]'' magazine's "The 100 Best Films of World Cinema". ''[[Paste (magazine)|Paste]]'' magazine ranked it second on its list of the 50 Best Movies of the Decade (2000–2009).<ref>{{cite web|title=The 50 Best Movies of the Decade (2000–2009)|url=http://www.pastemagazine.com/blogs/lists/2009/11/50-best-movies-of-the-decade-2000-2009.html?p=5|work=[[Paste (magazine)|''Paste'' Magazine]]|accessdate=2011-12-14|date=3 November 2009}}</ref>

''[[Entertainment Weekly]]'' named the film poster one of the best on its list of the top 25 film posters in the past 25 years.<ref>{{cite news|url=http://www.ew.com/ew/article/0,,20207076_20207387_20207597,00.html |title=Movies: 25 New Classic Posters |work=Entertainment Weekly  |date=27 June 2008 |accessdate=2010-04-28}}</ref> It also named Amélie setting up a wild goose chase for her beloved Nino all through Paris as No. 9 on its list of top 25 Romantic Gestures.<ref>{{cite news|url=http://www.ew.com/ew/article/0,,20207076_20207387_20207406,00.html |title=New Classics: Romantic Gestures |work=Entertainment Weekly  |accessdate=2010-04-28}}</ref> In 2010, an online public poll by the ''[[American Cinematographer]]'' – the house journal of the [[American Society of Cinematographers]] – named ''Amélie'' the best shot film of the decade.<ref>{{cite web|url=http://www.movieline.com/2010/06/was-amelie-really-the-best-shot-film-of-the-last-decade.php |title=Was Amélie Really the Best-Shot Film of the Last Decade? |publisher=movieline.com |date= 29 June 2010 |accessdate=2010-06-05| archiveurl= http://web.archive.org/web/20100701041512/http://www.movieline.com/2010/06/was-amelie-really-the-best-shot-film-of-the-last-decade.php| archivedate= 2010-07-01 <!--DASHBot-->| deadurl= no}}</ref>

==Soundtrack==
{{main|Amélie (soundtrack)}}
The soundtrack to ''Amélie'' was composed by [[Yann Tiersen]].

==Musical adaptation==
On 23 August 2013, composer Dan Messe, one of the founders and members of the band [[Hem (band)|Hem]], confirmed speculation that he would be writing the score for a musical adaptation of Amelie to premiere on Broadway. He will be collaborating with [[Craig Lucas]] and [[Nathan Tysen]].<ref>{{cite news|url=http://www.bbc.co.uk/news/entertainment-arts-23808207 |title=BBC News - Amelie musical to be made for Broadway |publisher=Bbc.co.uk |date=2013-08-23 |accessdate=2014-01-25}}</ref><ref>{{cite news|author=23 August 2013, 9: 58 AM |url=http://www.cbsnews.com/8301-207_162-57599847/amelie-becoming-a-broadway-musical/ |title="Amelie" becoming a Broadway musical |publisher=CBS News |date=2013-08-23 |accessdate=2014-01-25}}</ref> Messe also confirmed he would be composing all original music for the show and not using the [[Yann Tiersen]] score.<ref name="broadway">{{cite news| url= http://broadwaytour.net/amelie-set-to-be-adapted-for-broadway | work=Broadway Tour | title='Amelie' Set to be Adapted for Broadway | date=26 August 2013}}</ref>

Jeunet distanced himself from the musical, saying he only sold the rights to raise funds for children's charity "{{Ill|fr|Mécénat Chirurgie Cardiaque}}" (Cardiac Surgery Patronage).<ref>{{cite news|url=http://www.hollywoodreporter.com/news/amelie-director-jean-pierre-jeunet-616611 |title='Amelie' Director Jean-Pierre Jeunet 'Disgusted' by Musical |publisher=The Hollywood Reporter |date=2013-08-28 |accessdate=2014-01-25 |first=Rhonda |last=Richford}}</ref><!-- Article based on an interview France’s RTL {{citation needed}} for original french language source too ideally -->

==Home media==
{{Anchor|Home media}}
The film has no overall worldwide distributor, but [[Blu-ray Disc]]s have been released in Canada and Australia. The first release occurred in Canada in September 2008 by TVA Films. This version did not contain any English subtitles and received criticisms regarding picture quality.<ref>{{cite web|url=http://www.blu-ray.com/movies/Amelie-Blu-ray/1273/ |title=Amelie Blu-ray (Le Fabuleux destin d'Amélie Poulain) (2001) |publisher=Blu-ray.com |accessdate=2010-04-28}}</ref> In November 2009, an Australian release occurred. This time the version contained English subtitles and features no region coding.<ref>{{cite web|url=http://www.blu-ray.com/movies/Amelie-Blu-ray/7813/ |title=Amelie Blu-ray (Le Fabuleux destin d'Amélie Poulain) (2001) |publisher=Blu-ray.com |accessdate=2010-04-28| archiveurl= http://web.archive.org/web/20100414070910/http://www.blu-ray.com/movies/Amelie-Blu-ray/7813/| archivedate= 2010-04-14 <!--DASHBot-->| deadurl= no}}</ref> Momentum Pictures released a Blu-ray in the UK on 17 October 2011. The film is also available in HD on [[iTunes Store|iTunes]] and other digital download services. It can also be found on Netflix {{as of|2013|lc=y}}.

==Legacy==
For the 2007 television show ''[[Pushing Daisies]]'', a "quirky fairy tale", [[American Broadcasting Company]] (ABC) sought an ''Amélie'' feel, with the same chords of "whimsy and spirit and magic". ''Pushing Daisies'' creator [[Bryan Fuller]] said ''Amélie'' is his favorite film. "All the things I love are represented in that movie", he said. "It's a movie that will make me cry based on kindness as opposed to sadness". ''[[The New York Times]]''{{'}} review of ''Pushing Daisies'' reported "the ''Amélie'' influence on ''Pushing Daisies'' is everywhere".<ref>{{cite news |author=Bill Carter|title=A Touching Romance, if They Just Don't Touch|work=The New York Times |date=5 July 2007|url=http://www.nytimes.com/2007/07/05/arts/television/05dais.html}}</ref>

A species of frog was named ''[[Cochranella]] amelie''. The scientist who named it said: "[T]his new species of [[glass frog]] is for Amélie, protagonist of the extraordinary movie ''Le Fabuleux Destin d'Amélie Poulain''; a film where little details play an important role in the achievement of ''[[joie de vivre]]''; like the important role that glass frogs and all amphibians and reptiles play in the health of our planet".<ref>{{cite web|url=http://www.mapress.com/zootaxa/2007f/zt01572p082.pdf |title=zt01572p082.pdf |format=PDF |accessdate=2010-04-28}}</ref> The species was described in the scientific journal ''Zootaxa''<ref>{{cite web|author=mp |url=http://www.mapress.com/zootaxa/ |title=Zootaxa; a mega-journal for zoological taxonomists |publisher=Mapress.com |date= |accessdate=2014-01-25}}</ref> in an article entitled "An enigmatic new species of Glassfrog (Amphibia: Anura: Centrolenidae) from the Amazonian Andean slopes of Ecuador".<ref>http://www.mapress.com/zootaxa/2007f/z01485p041f.pdf</ref>

==See also==
* [[Cinema of France]]
* [[List of French language films]]

==References==
{{Reflist|30em}}

==External links==
{{wikiquote}}
* {{Official website|http://www.miramax.com/movie/amelie}}
* {{IMDb title|0211915|Amélie}}
* {{mojo title|amelie|Amélie}}
* {{rotten-tomatoes|amelie|Amélie}}
* {{metacritic film|amelie|Amélie}}
* [http://www.hollywoodgothique.com/jeunet.html Jean-Pierre Jeunet discusses the film]
* [http://www.movieloci.com/281-Amelie Filming locations at the Movieloci.com]

{{Navboxes
|list={{Jean-Pierre Jeunet}}
{{French submission for Academy Awards}}
{{César Award for Best Film}}
{{Broadcast Film Critics Association Award for Best Foreign Language Film}}
{{Crystal Globe}}
{{European Film Award for Best Film}}
{{European Film Award – People's Choice Award for Best European Film}}
{{London Film Critics Circle Award for Foreign Language Film of the Year}}
{{Lumières Award for Best Film}}
{{Yann Tiersen}}
}}

{{Portal bar|Film|France|Paris}}

{{DEFAULTSORT:Amelie}}
[[Category:2001 films]]
[[Category:French films]]
[[Category:German films]]
[[Category:French-language films]]
[[Category:2000s romantic comedy films]]
[[Category:French romantic comedy films]]
[[Category:German romantic comedy films]]
[[Category:Best Film César Award winners]]
[[Category:Best Film Lumières Award winners]]
[[Category:César Award winners]]
[[Category:Crystal Globe winners]]
[[Category:Fantasy-comedy films]]
[[Category:Films about women]]
[[Category:Films directed by Jean-Pierre Jeunet]]
[[Category:Films featuring a Best Actress Lumières Award winning performance]]
[[Category:Films set in 1997]]
[[Category:Films set in France]]
[[Category:Films set in Paris]]
[[Category:Films shot in Paris]]
[[Category:Films whose writer won the Best Original Screenplay BAFTA Award]]
[[Category:Independent Spirit Award for Best Foreign Film winners]]
[[Category:Magic realism films]]
[[Category:Montmartre]]
[[Category:Miramax films]]
      